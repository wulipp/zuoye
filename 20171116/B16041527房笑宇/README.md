## 11.16 作业 Windows程序框架和俄罗斯方块框架

### 思维导图

1. Windows程序框架

![windows程序框架](https://gitee.com/uploads/images/2017/1209/114217_a43e4ed3_1578283.png "Windows程序框架.png")

2. Tetris框架

![Tetris框架](https://gitee.com/uploads/images/2017/1209/114311_543761cc_1578283.png "Tetris.png")

3. Tetris功能框架

![Tetris功能](https://gitee.com/uploads/images/2017/1220/102343_543d2cdb_1578283.png "Tetris功能.png")

### 心得

* 学会使用思维导图
* 掌握了Windows程序和俄罗斯方块的基本框架，对具体实现方式稍有了解

### 说明

* 由于XMind未激活，不能使用导出为pdf功能，所以提交的是.xmind文件
