﻿俄罗斯方块
------
* 采用C++语言开发，实现了简单的方块摆放、旋转和计分功能。

**思维导图**   

![image](http://wx3.sinaimg.cn/mw690/00677rG9gy1flz3uhjft4j310o0m6q5s.jpg)

**运行结果**  

![image](http://wx3.sinaimg.cn/mw690/00677rG9gy1flz3uhjft4j310o0m6q5s.jpghttp://wx2.sinaimg.cn/mw690/00677rG9gy1flz4fql6kcg30de0iomyr.gif)





## 部分代码（刷新方块函数）  
* `tetrisX、tetrisY`为方块左上角坐标，作为参考点；
* `now_tetirs[4][4]`存放了当前方块坐标数组；
* `cur_tetirs[GAME_WIDTH][GAME_HEIGHT]`是整个网格数组，数组下标为网格x,y坐标，为1表示有方块，为0则表示空。
```c++
//刷新方块  (当调用此方法时，方块已经下落到底部)
/*
  1、将方块记录在 网格数组中
  2、循环整个网格，从底向上，查找满行
  3、重新排列新网格（去掉满行）
*/
VOID RefreshTetris(int tetrisX, int tetrisY, BOOL now_tetirs[4][4], BOOL cur_tetirs[GAME_WIDTH][GAME_HEIGHT])
{
	int x, y;
	for ( x = 0; x < 4; x++)
	{
		for ( y = 0; y < 4; y++)
		{
			if (now_tetirs[x][y])
			{
				cur_tetirs[tetrisX + y][tetrisY + x] = TRUE;   //把方块记录网格数组中
			}
		}
	}
	//循环整个网格
	
	int fullLine = 0;
	int newY;    //记录新坐标，当满行，用上一行代替这一满行
	int i, j;
	for ( i = GAME_HEIGHT,newY = GAME_HEIGHT; i >= 0; i--)
	{
		BOOL flag = FALSE;
		for ( j = 0; j < GAME_WIDTH; j++)
		{
			cur_tetirs[j][newY] = cur_tetirs[j][i];    //y是递减的，如果有满行，newY不递减，就用上一行代替这满行
			if (!cur_tetirs[j][i])   //不满行
			{
				flag = TRUE;  
			}
		}
		if (flag)   //不满行，newY和y一样递减
		{
			newY--;   
		}
		else      //满行，newY不递减，y递减，用上一行代替这满行
		{
			fullLine++;  //满行数 +1
		}
	}
	if (fullLine > 0)
	{
		GameScore += fullLine * 1;  //分数增加
	}
	memcpy(NOW_TETRIS,NEXT_TETRIS,sizeof(NOW_TETRIS));   //把下一个方格拷贝给当前方格
	memcpy(NEXT_TETRIS,BASIC_TETRIS[GetRandNum(0,TETRIS_CNT)],sizeof(NEXT_TETRIS));  //生成下一方格

	TetrisX = (GAME_WIDTH - 4) / 2;  //居中
	TetrisY = 0;
}
```   
**说明**   

&nbsp;游戏难度（速度）未实现，自动摆放（AI）算法后期加上。
