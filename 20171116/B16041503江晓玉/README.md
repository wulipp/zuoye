# 一、思维导图
## 1、Windows思维导图
![Windows思维导图](https://gitee.com/AYuKKNi/images/raw/master/Windows%E6%80%9D%E7%BB%B4%E5%AF%BC%E5%9B%BE.png)
## 2、Tetris思维导图
![Tetris思维导图](https://gitee.com/AYuKKNi/images/raw/master/Tetris%E6%80%9D%E7%BB%B4%E5%AF%BC%E5%9B%BE.png)
# 二、收获
## 1、学会制作思维导图。
## 2、对于windows和Tetris的基本框架比较了解，进一步掌握其中的元素。