// Win32Project1.cpp : 定义应用程序的入口点。
//

#include "stdafx.h"
#include "Win32Project1.h"

#define MAX_LOADSTRING 100

// 全局变量: 
HINSTANCE hInst;                                // 当前实例
WCHAR szTitle[MAX_LOADSTRING];                  // 标题栏文本
WCHAR szWindowClass[MAX_LOADSTRING];            // 主窗口类名

// 此代码模块中包含的函数的前向声明: 
ATOM /*WORD 类型， unsigned short */               MyRegisterClass(HINSTANCE hInstance /*指针*/); 
BOOL /* int 类型 */               InitInstance(HINSTANCE, int);
LRESULT /* long 类型 */ CALLBACK  /* __stdcall */  WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY /* __stdcall */ wWinMain(_In_ HINSTANCE hInstance, // 当前实例句柄
                     _In_opt_ HINSTANCE hPrevInstance, // 上一次实例句柄
                     _In_ LPWSTR    lpCmdLine, // 命令行参数
                     _In_ int       nCmdShow) // 窗口显示方式
{
    UNREFERENCED_PARAMETER(hPrevInstance); // 取消警告信息
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: 在此放置代码。

    // 初始化全局字符串
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING); // 从资源文件中读取字符串
    LoadStringW(hInstance, IDC_WIN32PROJECT1, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance); // 注册窗口类

    // 执行应用程序初始化: 
    if (!InitInstance (hInstance, nCmdShow)) // 初始化程序
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_WIN32PROJECT1)); // 加速器

    MSG msg;

	/*
	typedef struct tagMSG {
		HWND        hwnd;
		UINT        message;
		WPARAM      wParam;
		LPARAM      lParam;
		DWORD       time;
		POINT       pt;
	#ifdef _MAC
		DWORD       lPrivate;
	#endif
	} MSG, *PMSG, NEAR *NPMSG, FAR *LPMSG;
	*/

    // 主消息循环: 
    while (GetMessage(&msg, nullptr, 0, 0)) // 取得消息
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) // 处理加速键的信息
        {
            TranslateMessage(&msg); // 转换消息
            DispatchMessage(&msg); // 分发消息
        }
    }

    return (int) msg.wParam;
}



//
//  函数: MyRegisterClass()
//
//  目的: 注册窗口类。
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex; // 窗口类信息

	/*
	typedef struct tagWNDCLASSEXW {
		UINT        cbSize;
    
		UINT        style;
		WNDPROC     lpfnWndProc;
		int         cbClsExtra;
		int         cbWndExtra;
		HINSTANCE   hInstance;
		HICON       hIcon;
		HCURSOR     hCursor;
		HBRUSH      hbrBackground;
		LPCWSTR     lpszMenuName;
		LPCWSTR     lpszClassName;
		HICON       hIconSm;
	} WNDCLASSEXW, *PWNDCLASSEXW, NEAR *NPWNDCLASSEXW, FAR *LPWNDCLASSEXW; 
*/

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW; // 水平重绘和垂直重绘
    wcex.lpfnWndProc    = WndProc; // 回掉函数的函数指针
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_WIN32PROJECT1)); // 图标，从资源文件中获得
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW); // 从资源文件中获得光标
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1); // 背景
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_WIN32PROJECT1); // 从资源文件中获得菜单
    wcex.lpszClassName  = szWindowClass; // 窗口类名称
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL)); // 小图标

    return RegisterClassExW(&wcex); // 注册窗口类
}

//
//   函数: InitInstance(HINSTANCE, int)
//
//   目的: 保存实例句柄并创建主窗口
//
//   注释: 
//
//        在此函数中，我们在全局变量中保存实例句柄并
//        创建和显示主程序窗口。
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // 将实例句柄存储在全局变量中

   HWND hWnd = CreateWindowW(szWindowClass, // 类名称
	   szTitle, // 标题
	   WS_OVERLAPPEDWINDOW, // 窗口的样式
      CW_USEDEFAULT, //默认位置
	   0,
	   CW_USEDEFAULT, // 默认位置
	   0, 
	   nullptr, // 父窗口
	   nullptr, // 菜单栏
	   hInstance,
	   nullptr); // 附加信息

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow); // 显示窗口
   UpdateWindow(hWnd); // 更新从窗口

   return TRUE;
}

//
//  函数: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  目的:    处理主窗口的消息。
//
//  WM_COMMAND  - 处理应用程序菜单
//  WM_PAINT    - 绘制主窗口
//  WM_DESTROY  - 发送退出消息并返回
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_COMMAND: // 命令消息
        {
            int wmId = LOWORD(wParam);
            // 分析菜单选择: 
            switch (wmId)
            {
            case IDM_ABOUT: // 关于的消息
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About); // 显示窗口，根据资源文件显示窗口
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd); // 销毁窗口
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam); // 默认的消息处理函数
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
			/*
			typedef struct tagPAINTSTRUCT {
				HDC         hdc;
				BOOL        fErase;
				RECT        rcPaint;
				BOOL        fRestore;
				BOOL        fIncUpdate;
				BYTE        rgbReserved[32];
			} PAINTSTRUCT, *PPAINTSTRUCT, *NPPAINTSTRUCT, *LPPAINTSTRUCT;
			*/
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: 在此处添加使用 hdc 的任何绘图代码...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0); // 终止程序
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// “关于”框的消息处理程序。
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG: // 初始化对话框完成
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam)); // 清除对话框
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
