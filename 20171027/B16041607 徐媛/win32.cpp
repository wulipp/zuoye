// win32.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "resource.h"

#define MAX_LOADSTRING 100//宏定义，MAX_LOADSTRING值为100

// Global Variables:
HINSTANCE hInst;//定义结构体变量								// current instance
TCHAR szTitle[MAX_LOADSTRING];								// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];							// The title bar text

// Foward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);//注册窗口类的函数声明
BOOL				InitInstance(HINSTANCE, int); // 初始化实例的函数声明
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);// 主窗口的回调函数
LRESULT CALLBACK	About(HWND, UINT, WPARAM, LPARAM);// "关于"对话框的回调函数

int APIENTRY WinMain// win32程序入口函数
                    (HINSTANCE hInstance,//程序当前实例的句柄，以后随时可以用GetModuleHandle(0)来获得
                     HINSTANCE hPrevInstance, //这个参数在Win32环境下总是0
                     LPSTR     lpCmdLine,
                     int       nCmdShow)//指明应该以什么方式显示主窗口
{
 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_WIN32, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDC_WIN32);

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage is only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)//注册窗口类的函数
{
	WNDCLASSEX wcex;//定义了一个WNDCLASSEX 结构
    wcex.cbSize = sizeof(WNDCLASSEX);//WNDCLASSEX 的大小
    wcex.style			= CS_HREDRAW | CS_VREDRAW;//从这个窗口类派生的窗口具有的风格
	wcex.lpfnWndProc	= (WNDPROC)WndProc;//窗口处理函数的指针
	wcex.cbClsExtra		= 0;//指定紧跟在窗口类结构后的附加字节数
    wcex.cbWndExtra		= 0;//指定紧跟在窗口事例后的附加字节数
    wcex.hInstance		= hInstance;//本模块的事例句柄
	wcex.hIcon			= LoadIcon(hInstance, (LPCTSTR)IDI_WIN32);//图标的句柄
    wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);//光标的句柄
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);//背景画刷的句柄
	wcex.lpszMenuName	= (LPCSTR)IDC_WIN32;//指向菜单的指针
    wcex.lpszClassName	= szWindowClass;//指向类名称的指针
	wcex.hIconSm		= LoadIcon(wcex.hInstance, (LPCTSTR)IDI_SMALL);//和窗口类关联的小图标

	return RegisterClassEx(&wcex);//注册窗口类
}

//
//   FUNCTION: InitInstance(HANDLE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)// 初始化实例函数,此函数功能是创建并显示窗口,实际使用的api 是CreateWindow,ShowWindow
{
   HWND hWnd;

   hInst = hInstance; // 将实例句柄存储在全局变量中

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, unsigned, WORD, LONG)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	TCHAR szHello[MAX_LOADSTRING];
	LoadString(hInst, IDS_HELLO, szHello, MAX_LOADSTRING);

	switch (message)
	{
		case WM_COMMAND:
			wmId    = LOWORD(wParam);
			wmEvent = HIWORD(wParam);
			// Parse the menu selections:
			switch (wmId)
			{
				case IDM_ABOUT:
				   DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);
				   break;
				case IDM_EXIT:
				   DestroyWindow(hWnd);
				   break;
				default:
				   return DefWindowProc(hWnd, message, wParam, lParam);
			}
			break;
		case WM_PAINT:
			hdc = BeginPaint(hWnd, &ps);
			// TODO: Add any drawing code here...
			RECT rt;
			GetClientRect(hWnd, &rt);
			DrawText(hdc, szHello, strlen(szHello), &rt, DT_CENTER);
			EndPaint(hWnd, &ps);
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
   }
   return 0;
}

// Mesage handler for about box.
LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_INITDIALOG:
				return TRUE;

		case WM_COMMAND:
			if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
			{
				EndDialog(hDlg, LOWORD(wParam));
				return TRUE;
			}
			break;
	}
    return FALSE;
}
