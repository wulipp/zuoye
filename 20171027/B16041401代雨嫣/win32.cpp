// win32.cpp : Defines the entry point for the application.
//

#include "stdafx.h"               //头文件预编译，把一个工程(Project)中使用的一些MFC标准头文件(如Windows.H、Afxwin.H)预先编译，以后该工程编译时，不再编译这部分头文件，仅仅使用预编译的结果。这样可以加快编译速度，节省时间
#include "resource.h"

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;		//HINSTANCE:无符号的长整形，32位的，用于标示（记录）一个程序的实例						// current instance
TCHAR szTitle[MAX_LOADSTRING];						//TCHAR是通过define定义的字符串宏		// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];								// The title bar text

// Foward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
 	// TODO: Place code here.
	MSG msg;//Windows程序中的结构体。在Windows程序中，消息是由MSG结构体来表示的
	HACCEL hAccelTable;//加速键句柄变量

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);             //LoadString从 资源 里加载字符串资源到CString对象里
	LoadString(hInstance, IDC_WIN32, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow)) //InitInstance是CWinThread的一个虚函数，InitInstance就是“初始化实例”的意思，它是在实例创建时首先被调用的。
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDC_WIN32);

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0)) 
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) 
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage is only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;//WNDCLASSEX属于一个窗台类

	wcex.cbSize = sizeof(WNDCLASSEX);    //WNDCLASSEX 的大小

	wcex.style			= CS_HREDRAW | CS_VREDRAW;   //设置从这个窗口类派生的窗口具有的风格。 
	wcex.lpfnWndProc	= (WNDPROC)WndProc;          //窗口处理函数的指针。 
	wcex.cbClsExtra		= 0;                        //指定紧跟在窗口类结构后的附加字节数。 
	wcex.cbWndExtra		= 0;                        //指定紧跟在窗口实例的附加字节数 
	wcex.hInstance		= hInstance;				//本模块的实例句柄 
	wcex.hIcon			= LoadIcon(hInstance, (LPCTSTR)IDI_WIN32);		//图标的句柄。 
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);					//光标的句柄。 
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);						//背景画刷的句柄。 
	wcex.lpszMenuName	= (LPCSTR)IDC_WIN32;							//指向菜单的指针。 
	wcex.lpszClassName	= szWindowClass;								//指向类名称的指针。 
	wcex.hIconSm		= LoadIcon(wcex.hInstance, (LPCTSTR)IDI_SMALL);//和窗口类关联的小图标。如果该值为NULL。则把hIcon中的图标转换成大小合适的小图标。 

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HANDLE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, unsigned, WORD, LONG)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)             //Wndproc是Windows操作系统向应用程序发送一系列消息之一，每个窗口会有一个窗口过程的回调函数，分别是窗口句柄、消息ID、WPARAM、LPARAM 
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;                       //PAINTSTRUCT结构体包含了某应用程序用来绘制它所拥有的窗口客户区所需要的信息。
	HDC hdc;                           //HDC设备上下文是一种包含有关某个设备（如显示器或打印机）的绘制属性信息的 Windows 数据结构
	TCHAR szHello[MAX_LOADSTRING];
	LoadString(hInst, IDS_HELLO, szHello, MAX_LOADSTRING);

	switch (message) 
	{
		case WM_COMMAND:
			wmId    = LOWORD(wParam); 
			wmEvent = HIWORD(wParam); 
			// Parse the menu selections:
			switch (wmId)
			{
				case IDM_ABOUT:
				   DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);
				   break;
				case IDM_EXIT:
				   DestroyWindow(hWnd);
				   break;
				default:
				   return DefWindowProc(hWnd, message, wParam, lParam);
			}
			break;
		case WM_PAINT:
			hdc = BeginPaint(hWnd, &ps);
			// TODO: Add any drawing code here...
			RECT rt;
			GetClientRect(hWnd, &rt);
			DrawText(hdc, szHello, strlen(szHello), &rt, DT_CENTER);
			EndPaint(hWnd, &ps);
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
   }
   return 0;
}

// Mesage handler for about box.
LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_INITDIALOG:
				return TRUE;

		case WM_COMMAND:
			if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)          //LOWORD是:此宏从指定的 32 位值，检索低顺序单词。
			{
				EndDialog(hDlg, LOWORD(wParam));                               //清除一个模态对话框,并使系统中止对对话框的任何处理的函数。 
				return TRUE;
			}
			break;
	}
    return FALSE;
}
