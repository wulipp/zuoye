#include<stdio.h>
#include<stdlib.h>
#include<string.h>

char * mystrcat(const char * str1, const char * str2)
{
	int i = 0, j = 0, k = 0;
    if(str1 != NULL)
    {
    	while(str1[i] != '\0')
    	{
    		i++;
    		k++;
		}
	}
	if(str2 != NULL)
	{
		while(str2[j] != '\0')
		{
			j++;
			k++;
		}
	}
	char * t = (char *)malloc(sizeof(char)*(k+1));
	for(k = 0; k < i; k++)
	{
		t[k] = str1[k];
	}
	for(k = 0; k < j; k++)
	{
		t[k+i] = str2[k];
	}
	return t;
}

int main()
{
	const char *s1="abc";
	const char *s2="def";
	char *s;
	s = mystrcat(s1,s2);
	int i=0;
	while (s[i]!='\0')
	{
		printf("%c",s[i]);
		i++;
	}
	printf("\n");
	return 0;
}

TEST(FooTest, HandleNoneZeroInput)
{
	 EXPECT_STREQ("abcdef", mystrcat("abc","def"));
	 EXPECT_STREQ("abc", mystrcat("abc", ""));
	 EXPECT_STREQ("abc", mystrcat("abc", NULL));
	 EXPECT_STREQ("abc", mystrcat(NULL, "abc"));
	 EXPECT_STREQ(NULL, mystrcat(NULL,NULL));
}
int _tmain(int argc, _TCHAR * argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

