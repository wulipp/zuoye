// B16041515杜跃通.cpp : 定义应用程序的入口点。
//

#include "stdafx.h"
#include "B16041515杜跃通.h"

#define MAX_LOADSTRING 100  //宏定义

// 全局变量: 
HINSTANCE hInst;                                // 当前实例
WCHAR szTitle[MAX_LOADSTRING];    //类型定义 char类型              // 标题栏文本
WCHAR szWindowClass[MAX_LOADSTRING];    //类型定义 char类型  //       // 主窗口类名

// 此代码模块中包含的函数的前向声明: 
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

/* LRESULT 宏定义 LONG_PTR 类型定义 long类型
CALLBACK 宏定义 __stdcall
UINT 类型定义 unsigned int类型
WPARAM 字参数 宏定义 UINT_PTR 类型定义 unsigned int类型
LPARAM 长字参数 宏定义 LONG_PTR 类型定义 long类型 */
int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
	/* APIENTRY 宏定义 WINAPI 类型定义 __stdcall
	LPSTR 类型定义 char*类型
	WinMain(hInstance 当前实例句柄,hPrevInstance 前一个
	实例句柄,lpCmdLine 命令行,nCmdShow 控制窗口的显示方
	式)Windows函数 win32应用程序入口 */
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: 在此放置代码。

    // 初始化全局字符串
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_B16041515, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);
	/* LoadString(hInstance 当前实例句柄,uID 要加载的字符串,
	lpBuffer 接收的字符串,nBufferMax 缓冲区的大小) Windows函
	数 从与指定模块相关联的可执行文件中加载字符串资源，将字符
	串复制到缓冲区，并附加终止空字符IDS_APP_TITLE 宏定义 103 */
    // 执行应用程序初始化: 
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_B16041515));
	// LoadAccelerators(hInstance 当前实例句柄，lpTableName 要加载的加速器表的名称)
    MSG msg;

    // 主消息循环: 
    while (GetMessage(&msg, nullptr, 0, 0))
    {
		/* GetMessage(lpMSG 消息结构的指针,hWnd 窗口句柄,wMsgFilterMin 要
		检索的最低消息值的整数值,wMsgFilterMax 要检索的最高消息值的整数值) 
		Windows函数 从调用线程的消息队列中检索消息LPMSG 类型定义 tagMSG的
		结构指针 */
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  函数: MyRegisterClass()
//
//  目的: 注册窗口类。
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
	/* 窗口类的样式
	CS_HREDRAW 宏定义 0x0002 如果窗口的位置或宽度改变,将重绘窗口
	CS_VREDRAW 宏定义 0x0001 如果窗口的位置或高度改变,将重绘窗口 */
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_B16041515));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_B16041515);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   函数: InitInstance(HINSTANCE, int)
//
//   目的: 保存实例句柄并创建主窗口
//
//   注释: 
//
//        在此函数中，我们在全局变量中保存实例句柄并
//        创建和显示主程序窗口。
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // 将实例句柄存储在全局变量中

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);
   /* CreateWindow(lpClassName 窗口类名称,lpWindowName 窗口名称,
   dwStyle 窗口样式,x 窗口初始水平位置,y 窗口初始垂直位置,
   nWidth 窗口初始宽度,nHeight 窗口初始高度,
   hWndParent 父窗口或所有者窗口句柄,hMemu 菜单句柄,
   hInstance 实例句柄,lpParam 消息参数指针) Windows函数 创建窗口
   WS_OVERLAPPEDWINDOW 宏定义 (WS_OVERLAPPED  宏定义 0x00000000L|\
   WS_CAPTION	   宏定义 0x00C00000L|\
   WS_SYSMENU	   宏定义 0x00080000L|\
   WS_THICKFRAME  宏定义 0x00040000L|\
   WS_MINIMIZEBOX 宏定义 0x00020000L|\
   WS_MAXIMIZEBOX 宏定义 0x00010000L) 窗口样式类型
   CW_USEDEFAULT 宏定义 ((int)0x80000000) */
   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  函数: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  目的:    处理主窗口的消息。
//
//  WM_COMMAND  - 处理应用程序菜单
//  WM_PAINT    - 绘制主窗口
//  WM_DESTROY  - 发送退出消息并返回
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // 分析菜单选择: 
            switch (wmId)
            {
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: 在此处添加使用 hdc 的任何绘图代码...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// “关于”框的消息处理程序。
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
