## 10.27作业 注释 win32 application 程序 "Hello World" 代码

### 作业内容

一、对于向导生成的win32 application程序“Hello World”代码添加注解

1. 非c语言自带的类型定义
2. 宏定义
3. windows函数（查询msdn.microsoft.com获得准确解释）

二、对于生成的代码运行通过

1. 熟悉单步执行
2. 断点、条件断点
3. 熟悉各调试窗口

### 作业心得

* MSDN文库十分好用，查Windows函数的时候非常方便，对以后的学习也十分有好处，有不懂的就可以查官方文档

* 调试对于一个程序员来说十分重要，要好好掌握Debug的方法，熟悉步骤，方便排查程序错误

* 学习了Markdown语法，排版清晰

### 运行结果截图

见images文件夹