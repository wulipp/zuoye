//WindowsProject1.cpp: 定义应用程序的入口点。
//

#include "stdafx.h"
#include "WindowsProject1.h"

#define MAX_LOADSTRING 100       //用100替换MAX_LOADSTRING

// 全局变量: 
HINSTANCE hInst;                                // 当前实例    HINSTANCE:实例的句柄
WCHAR szTitle[MAX_LOADSTRING];                  // 标题栏文本  WCHAR:宽字符类型
WCHAR szWindowClass[MAX_LOADSTRING];            // 主窗口类名

// 此代码模块中包含的函数的前向声明: 
ATOM                MyRegisterClass(HINSTANCE hInstance); //ATOM：WORD型，无符号的短整型
BOOL                InitInstance(HINSTANCE, int); 
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);//LRESULT：long型，从窗口程序或者回调函数返回的32位值
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);//CALLBACK：对应函数为回调函数，消息的次序

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,//APIENTRY：即__stdcall,表明此函数是应用程序的入口点
                     _In_opt_ HINSTANCE hPrevInstance,//_In_opt_:宏，返回后不会改变其值
                     _In_ LPWSTR    lpCmdLine,//LPSTR：即 char *
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: 在此放置代码。

    // 初始化全局字符串
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);//LoadStringW：Unicode中用于把String Table中Caption里面的字符串读出来到CString对象里
    LoadStringW(hInstance, IDC_WINDOWSPROJECT1, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);//MyRegisterClass：注册窗口类函数

    // 执行应用程序初始化: 
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_WINDOWSPROJECT1));//HACCEL：加速键句柄, LoadAccelerators：函数调入指定的加速键表

    MSG msg;
	/*typedef struct tagMSG{HWND        hwnd;
	UINT        message;
	WPARAM      wParam;
	LPARAM      lParam;
	DWORD       time;
	POINT       pt;
	#ifdef _MAC
	DWORD       lPrivate;
	#endif
	} MSG;
 主消息循环:*/ 
    while (GetMessage(&msg, nullptr, 0, 0))//GetMessage：从调用线程的消息队列里取得一个消息并将其放于指定的结构；返回值：如果函数取得WM_QUIT之外的其他消息，返回非零值。如果函数取得WM_QUIT消息，返回值是零。如果出现了错误，返回值是-1。
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
			TranslateMessage(&msg);//TranslateMessage:将虚拟键消息转换为字符消息。字符消息被寄送到调用线程的消息队列里，当下一次线程调用函数GetMessage或PeekMessage时被读出;返回值：如果消息被转换（即，字符消息被寄送到调用线程的消息队列里），返回非零值
            DispatchMessage(&msg);// DispatchMessage:分发一个消息给窗口程序,将消息传递给操作系统，然后操作系统去调用回调函数；返回值：返回值是窗口程序返回的值
        }
    }

    return (int) msg.wParam;
}



//
//  函数: MyRegisterClass()
//
//  目的: 注册窗口类。
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;//CS_HREDRAW：一旦移动或尺寸调整使客户区的宽度发生变化，就重新绘制窗口；CS_VREDRAW：一旦移动或尺寸调整使客户区的高度发生变化，就重新绘制窗口
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_WINDOWSPROJECT1));//LoadIcon函数从与hInstance模块相关联的可执行文件中装入lpIconName指定的图标资源，仅当图标资源还没有被装入时该函数才执行装入操作，否则只获取装入的资源句柄
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);//LoadCursor：从一个与应用事例相关的可执行文件（EXE文件）中载入指定的光标资源；IDC_ARROW 标准的箭头
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_WINDOWSPROJECT1);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);//RegisterClassExW：函数为随后在调用Createwindow函数和CreatewindowEx函数中使用的窗口注册一个窗口类
}

//
//   函数: InitInstance(HINSTANCE, int)
//
//   目的: 保存实例句柄并创建主窗口
//
//   注释: 
//
//        在此函数中，我们在全局变量中保存实例句柄并
//        创建和显示主程序窗口。
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // 将实例句柄存储在全局变量中

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  函数: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  目的:    处理主窗口的消息。
//
//  WM_COMMAND  - 处理应用程序菜单
//  WM_PAINT    - 绘制主窗口
//  WM_DESTROY  - 发送退出消息并返回
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)//WndProc：回调函数，WndProc的第一个参数hWnd就是当前接收消息的窗口句柄，第二个参数就是被传送过来的消息，第三、第四个参数都是附加在消息上的数据
{//UINT：消息名称；WPARAM：WORD类型的32位整型变量
	switch (message)
	{
	case WM_COMMAND://消息，点击菜单、按钮、下拉列表框等控件时候，会触发
	{
		int wmId = LOWORD(wParam);//LOWORD：从指定的 32 位值，检索低顺序单词，返回值是指定值的低位单字
								  // 分析菜单选择: 
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);//DialogBox：Windows API函数。它的作用是从一个对话框资源中创建一个模态对话框。该函数直到指定的回调函数通过调用EndDialog函数中止模态的对话框才能返回控制;About:“关于”框的消息处理程序
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);//函数功能：销毁指定的窗口
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);//DefWindowProc函数调用缺省的窗口过程来为应用程序没有处理的任何窗口消息提供缺省的处理
		}
	}
	break;
	case WM_PAINT://当窗口显示区域的一部分显示内容或者全部变为“无效”，以致于必须“更新画面”时，将由这个消息通知程序
	{
		PAINTSTRUCT ps;// PAINTSTRUCT：结构体，包含了应用程序用来绘制它所拥有的窗口客户区所需要的信息
		HDC hdc = BeginPaint(hWnd, &ps);//HDC：MFC中的设备上下文句柄; BeginPaint:为指定窗口进行绘图工作的准备，并用将和绘图有关的信息填充到一个PAINTSTRUCT结构中
										// TODO: 在此处添加使用 hdc 的任何绘图代码...
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY://窗口销毁后(调用DestroyWindow()后)，消息队列得到的消息
		PostQuitMessage(0);//PostQuitMessage:该函数向系统表明有个线程有终止请求
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// “关于”框的消息处理程序。
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)//INT_PTR:和指针长度相等的INT
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:// WM_INITDIALOG:消息,对话框才能收到的消息,表明对话框及其所有子控件都创建完毕
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)//IDOK:确定按钮,IDCANCEL:取消按钮
		{
			EndDialog(hDlg, LOWORD(wParam));//该函数清除一个模态对话框,并使系统中止对对话框的任何处理
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

