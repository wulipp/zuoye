// win32.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "resource.h"

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];								// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];								// The title bar text

// Foward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);//窗口函数：处理消息
LRESULT CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

 int APIENTRY _tWinMain(HINSTANCE hInstance,//当前实例句柄  
                         HINSTANCE hPrevInstance,//NULL，为了兼容  
                         LPTSTR    lpCmdLine,//该字符串包含传递给应用程序的命令行参数  
                         int       nCmdShow)//指定程序的窗口应该如何显示  
{
 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);//将szTitle赋值为字符串IDS_APP_TITLE
	LoadString(hInstance, IDC_WIN32, szWindowClass, MAX_LOADSTRING); //将szWindowClass赋值为字符串IDC_WIN32PROJECT  
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow)) 
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDC_WIN32);

	// Main message loop:  
	while (GetMessage(&msg, NULL, 0, 0))  //编写消息循环代码
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) 
		{
			 TranslateMessage(&msg); //请求Windows为那些与键盘有关的消息做一些转换工作  
               		 DispatchMessage(&msg);  //请求Windows分派消息到窗口过程，由窗口过程函数对消息进行处理  
		}
	}

	return msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage is only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance) //设计窗口类  
{
	WNDCLASSEX wcex;//创建WNDCLASSEX类型的对象  

	wcex.cbSize = sizeof(WNDCLASSEX); 

	wcex.style          = CS_HREDRAW | CS_VREDRAW;  //设置窗口的样式  
        wcex.lpfnWndProc    = WndProc;                  //回调函数  
        wcex.cbClsExtra     = 0;                        //可以请求额外空间，一般不需要  
        wcex.cbWndExtra     = 0;  
        wcex.hInstance      = hInstance;                //指定当前应用程序的实例句柄  
        wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_WIN32PROJECT));//指定窗口类的图标句柄  
        wcex.hCursor        = LoadCursor(NULL, IDC_ARROW);  //指定窗口类的光标句柄  
        wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);     //指定窗口类的背景画刷句柄  
        //wcex.hbrBackground = static_cast<HBRUSH>(GetStockObject(BLACK_BRUSH));  //BLACK_BRUSH,DKGRAY_BRUSH, GRAY_BRUSH  
        wcex.lpszMenuName   = MAKEINTRESOURCE(IDC_WIN32PROJECT);//lpszMenuName是一个以空终止的字符串，指定菜单资源的名字。  
                                                                //如果该窗口没有菜单，则应该将其设为0.  
        wcex.lpszClassName  = szWindowClass;                    //指定窗口类的名字  
        wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL)); //标识某个窗口最小化时显示的图标  

        return RegisterClassEx(&wcex);//调用RegisterClassEx()函数向系统注册窗口类 
}

//
//   FUNCTION: InitInstance(HANDLE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable
   //创建窗口  
   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);  //初始化窗口  
   UpdateWindow(hWnd);  //通知Windows应用程序重绘客户区

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, unsigned, WORD, LONG)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
//窗口过程函数，处理消息
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)//参数:窗口句柄，消息，消息参数，消息参数
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	TCHAR szHello[MAX_LOADSTRING];
	LoadString(hInst, IDS_HELLO, szHello, MAX_LOADSTRING);
//处理感兴趣的消息
	switch (message) 
	{
		case WM_COMMAND:
			wmId    = LOWORD(wParam); 
			wmEvent = HIWORD(wParam); 
			// Parse the menu selections:
			switch (wmId)
			{
				case IDM_ABOUT:
				   DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);
				   break;
				case IDM_EXIT:
				   DestroyWindow(hWnd);
				   break;
				default:
				   return DefWindowProc(hWnd, message, wParam, lParam);
			}
			break;
		case WM_PAINT:
			hdc = BeginPaint(hWnd, &ps);
			// TODO: Add any drawing code here...
			RECT rt;
			GetClientRect(hWnd, &rt);
			DrawText(hdc, szHello, strlen(szHello), &rt, DT_CENTER);
			EndPaint(hWnd, &ps);
			break;
		case WM_DESTROY://当用户关闭窗口，窗口销毁，程序需结束，发退出消息，以退出消息循环
			PostQuitMessage(0);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);//其他消息交给由系统提供的缺省处理函数
   }
   return 0;
}

// Mesage handler for about box.
LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)//？？是About对话框的对话框处理函数 处理About对话框的各种消息
{
	switch (message)
	{
		case WM_INITDIALOG:
				return TRUE;

		case WM_COMMAND:
			if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) 
			{
				EndDialog(hDlg, LOWORD(wParam));
				return TRUE;
			}
			break;
	}
    return FALSE;
}
