// win32.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "resource.h"

#define MAX_LOADSTRING 100

// Global Variables:
//#define DECLARE_HANDLE(n) typedef struct n##__{int i;}*n
//DECLARE_HANDLE(HINSTANCE);
HINSTANCE hInst;								// current instance
//typedef	wchar_t	TCHAR;
TCHAR szTitle[MAX_LOADSTRING];								// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];								// The title bar text

//用于设置程序所需的主窗口类的值																	// Foward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);//typedef unsigned short WORD;//typedef WORD ATOM;
//在生成的一个新的实例的时候，完成一些初始化的工作
BOOL				InitInstance(HINSTANCE, int);//typedef int BOOL
//窗口过程函数，主要用于处理发送给窗口的信息
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);//typedef long LRESULT//DECLARE_HANDLE(HWND)//#define CALLBACK __stdcall//typedef unsigned int UINT//typedef UINT_PTR WPARAM//typedef LONG_PTR LPARAM
//
LRESULT CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
//#define APIENTRY __stdcall
//主函数
int APIENTRY WinMain(HINSTANCE hInstance,//DECLARE_HANDLE(HINSTANCE)
	HINSTANCE hPrevInstance,
	LPSTR     lpCmdLine,//typedef char LPSTR
	int       nCmdShow)
{
	// TODO: Place code here.
	MSG msg;//typedef struct MSG
	HACCEL hAccelTable;//DECLARE_HANDLE(HACCEL)

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);//资源里加载字符串到CString对象
	LoadString(hInstance, IDC_WIN32, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;//#define FALSE 0
	}

	hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDC_WIN32);//调入加速键表，该函数调入指定的加速键表

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))//是从调用线程的消息队列里取得一个消息并将其放于指定的结构
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))//翻译加速键表
		{
			TranslateMessage(&msg);//用于将虚拟键消息转换为字符消息
			DispatchMessage(&msg);//该函数分发一个消息给窗口程序
		}
	}

	return msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage is only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex; //typedef struct WNDCLASSEX

	wcex.cbSize = sizeof(WNDCLASSEX);//unsigned int

	wcex.style = CS_HREDRAW | CS_VREDRAW;//unsigned int
	wcex.lpfnWndProc = (WNDPROC)WndProc;//long
	wcex.cbClsExtra = 0;//int
	wcex.cbWndExtra = 0;//int
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, (LPCTSTR)IDI_WIN32);//LoadIcon函数从与hInstance模块相关联的可执行文件中装入lpIconName指定的图标资源，仅当图标资源还没有被装入时该函数才执行装入操作，否则只获取装入的资源句柄。

	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);//该函数从一个与应用事例相关的可执行文件（EXE文件）中载入指定的光标资源
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);//#define COLOR_WINDOW 5
	wcex.lpszMenuName = (LPCSTR)IDC_WIN32;
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, (LPCTSTR)IDI_SMALL);
	//为随后在调用Createwindow函数和CreatewindowEx函数中使用的窗口注册一个窗口类
	return RegisterClassEx(&wcex);//#define RegisterClassEx RegisterClassExW
}

//
//   FUNCTION: InitInstance(HANDLE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND hWnd;

	hInst = hInstance; // Store instance handle in our global variable
	//在WinUser.h中根据是否已定义Unicode被分别定义为CreateWindowW和CreateWindowA，然后两者又被分别定义为对CreateWindowExW和CreateWindowExA函数的调用
	hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);//该函数设置指定窗口的显示状态
	UpdateWindow(hWnd);//更新指定窗口的客户区

	return TRUE;//#define TRUE 1
}

//
//  FUNCTION: WndProc(HWND, unsigned, WORD, LONG)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;//struct
	HDC hdc;
	TCHAR szHello[MAX_LOADSTRING];
	LoadString(hInst, IDS_HELLO, szHello, MAX_LOADSTRING);

	switch (message)
	{
	case WM_COMMAND:
		wmId = LOWORD(wParam);//#define LOWORD(l)	((WORD)((DWORD)(l)))
		wmEvent = HIWORD(wParam);//#define HIWORD(l)	((WORD)(((DWORD)(l)>>16)&0xFFFF))
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);//从一个对话框资源中创建一个模态对话框
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);//销毁指定的窗口
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);//调用缺省的窗口过程来为应用程序没有处理的任何窗口消息提供缺省的处理
		}
		break;
	case WM_PAINT://15
		hdc = BeginPaint(hWnd, &ps);//为指定窗口进行绘图工作的准备，并用将和绘图有关的信息填充到一个PAINTSTRUCT结构中
		// TODO: Add any drawing code here...
		RECT rt;//struct
		GetClientRect(hWnd, &rt);//获取窗口客户区的大小
		DrawText(hdc, szHello, strlen(szHello), &rt, DT_CENTER);//该函数在指定的矩形里写入格式化的正文，根据指定的方法对正文格式化
		EndPaint(hWnd, &ps);//为指定窗口进行绘图的开始EndPaint是绘图的结束，释放绘图区
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Mesage handler for about box.
LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_INITDIALOG:
		return TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));//是清除一个模态对话框,并使系统中止对对话框的任何处理的函数
			return TRUE;//1
		}
		break;
	}
	return FALSE;
}
