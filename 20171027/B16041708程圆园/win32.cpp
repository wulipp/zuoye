// win32.cpp : Defines the entry point for the application.
//
#include "stdafx.h"
#include "resource.h"
#define MAX_LOADSTRING 100
// Global Variables:
HINSTANCE hInst;//HINSTANCE 是“句柄型”数据类型								// current instance
TCHAR szTitle[MAX_LOADSTRING];//	TCHAR是通过define定义的字符串宏,TCHAR等价于CHAR							// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];								// The title bar text
																	// Foward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);//ATOM编辑器 
BOOL				InitInstance(HINSTANCE, int);//BOOL表示布尔型变量，也就是逻辑型变量的定义符，BOOL长度视实际环境来定，一般可认为是4个字节
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);//LRESULT是一个数据类型，指的是从窗口程序或者回调函数返回的32位值。 
LRESULT CALLBACK	About(HWND, UINT, WPARAM, LPARAM);//CALLBACK是由用户设计却由windows系统呼叫的函数，统称为callback函数。某些API函数要求以callback作为你参数之一。
													  //APIENTRY表示是windows的mian函数的入口 
int APIENTRY WinMain(HINSTANCE hInstance,//程序当前实例的句柄，以后随时可以用GetModuleHandle(0)来获得
	HINSTANCE hPrevInstance,//这个参数在Win32环境下总是0，已经废弃不用了
	LPSTR     lpCmdLine,//指向以/0结尾的命令行，不包括EXE本身的文件名，
						//以后随时可以用GetCommandLine()来获取完整的命令行
	int       nCmdShow)//指明应该以什么方式显示主窗口
{
	// TODO: Place code here.
	MSG msg;//消息数据类型
	HACCEL hAccelTable;//HACCEL是加速键句柄变量
					   // Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);// //将IDS_APP_TITLE里的内容载入字符串szTitle
	LoadString(hInstance, IDC_WIN32, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);// 调用注册窗口函数
							   // 执行应用程序初始化: 
							   // Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))//调用初始化实例函数 
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDC_WIN32);//载入快捷键表，该表能在资源VC的视图中看到
																  // Main message loop:
	while (GetMessage(&msg, NULL, 0, 0)) // 从消息队列中取消息
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);// 传递消息
			DispatchMessage(&msg);?//发送消息给窗口过程
		}
	}
	return msg.wParam;
}
//  FUNCTION: MyRegisterClass()
//  PURPOSE: Registers the window class.
//  COMMENTS:
//    This function and its usage is only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
ATOM MyRegisterClass(HINSTANCE hInstance)// 注册窗口函数,实际调用的api 是  RegisterClassEx
{
	WNDCLASSEX wcex;//WNDCLASSEX属于一个窗台类,WNDCLASSEX 结构用于注册窗口类
	wcex.cbSize = sizeof(WNDCLASSEX); //WNDCLASSEX结构体大小
	wcex.style = CS_HREDRAW | CS_VREDRAW;//位置改变时重绘
	wcex.lpfnWndProc = (WNDPROC)WndProc;//消息处理函数
	wcex.cbClsExtra = 0;//附加字节，一般为0
	wcex.cbWndExtra = 0;//附加字节，一般为0
	wcex.hInstance = hInstance;//背景色
	wcex.hIcon = LoadIcon(hInstance, (LPCTSTR)IDI_WIN32);//图标
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);//光标
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);?//窗口画刷颜色
	wcex.lpszMenuName = (LPCSTR)IDC_WIN32;//菜单名称
	wcex.lpszClassName = szWindowClass;//参窗口类名
	wcex.hIconSm = LoadIcon(wcex.hInstance, (LPCTSTR)IDI_SMALL);//最小化图标

	return RegisterClassEx(&wcex);//注册
}
//   FUNCTION: InitInstance(HANDLE, int)
//   PURPOSE: Saves instance handle and creates main window
//   COMMENTS:
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow) // 初始化实例函数,此函数功能是创建并显示窗口,实际使用的api 是CreateWindow,,ShowWindow
{
	HWND hWnd;//定义窗口句柄 

	hInst = hInstance;//将实例句柄存储在全局变量中 // Store instance handle in our global variable

					  //创建窗口
	hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);//显示窗口 
	UpdateWindow(hWnd);//刷新窗口 

	return TRUE;
}
//  FUNCTION: WndProc(HWND, unsigned, WORD, LONG)
//  PURPOSE:  Processes messages for the main window.
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//消息处理函数，参数:窗口句柄，消息，消息参数，消息参数
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)// 主窗口的回调函数
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;//保存了窗口绘制信息定义 
	HDC hdc;//HDC是Windows的设备描述表句柄 
	TCHAR szHello[MAX_LOADSTRING];//定义双字节字符串
	LoadString(hInst, IDS_HELLO, szHello, MAX_LOADSTRING);//将资源表里的字符给szhello变量 
	switch (message) //选择消息 
	{
	case WM_COMMAND://菜单中选择一个命令时得到的消息 
		wmId = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);//跳出芯图形窗口 
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);//销毁窗口 
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		RECT rt;//定义矩形变量 
		GetClientRect(hWnd, &rt);//将窗口尺寸赋给矩形变量
		DrawText(hdc, szHello, strlen(szHello), &rt, DT_CENTER);//在窗口上打印szHello里的内容
		EndPaint(hWnd, &ps);//结束绘制 
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}
// Mesage handler for about box.
LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)//菜单中点击关于弹出的小窗口
{
	switch (message)//选择消息 
	{
	case WM_INITDIALOG://此处添加初始化时进行的内容，默认没有添加
		return TRUE;
	case WM_COMMAND://选择了其中的按钮
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) //如果点击“确定”或者“取消” 
		{
			EndDialog(hDlg, LOWORD(wParam));//结束 
			return TRUE;
		}
		break;
	}
	return FALSE;
}
