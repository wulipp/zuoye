// win32.cpp: 定义应用程序的入口点。
//

#include "stdafx.h"
#include "WindowsProject1.h"

#define MAX_LOADSTRING 100//宏定义,MAX_LOADSTRING=100

// 全局变量: 
HINSTANCE hInst;//定义结构体struct HINSTANCE__ { int unused; }；          //当前实例
WCHAR szTitle[MAX_LOADSTRING];//WCHAR是wchar_t的别名                  // 标题栏文本
WCHAR szWindowClass[MAX_LOADSTRING];                                // 主窗口类名

																	// 此代码模块中包含的函数的前向声明: 

																	//ATOM是unsigned short的别名

ATOM    MyRegisterClass(HINSTANCE hInstance);//注册窗口函数的声明
											 //BOOL是int的别名
											 //InitInstance(HINSTANCE, int)保存实例句柄并创建主窗口
BOOL    InitInstance(HINSTANCE, int);// 初始化实例的函数声明
									 //LRESULT是long型_W64类型的别名
									 //WndProc(HWND, UINT, WPARAM, LPARAM)处理主窗口的消息
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);//// 主窗口的回调函数 
														//About(HWND, UINT, WPARAM, LPARAM)“关于”框的消息处理程序
														//INT_PTR是int型_W64类型的别名
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM); // "关于"对话框的回调函数 
													   //APIENTRY是__stdcall的别名
													   // win32程序入口函数
int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	//UNREFERENCED_PARAMETER告诉编译器，已经使用了该变量，不必检测警告！
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: 在此放置代码。

	// 初始化全局字符串
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);// 导入字符串资源
	LoadStringW(hInstance, IDC_WINDOWSPROJECT1, szWindowClass, MAX_LOADSTRING);// 导入字符串资源
	MyRegisterClass(hInstance);// 调用注册窗口函数

							   // 执行应用程序初始化: 
	if (!InitInstance(hInstance, nCmdShow)) //调用初始化实例函数
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_WINDOWSPROJECT1));// 导入
																						   // 主消息循环: 

	MSG msg;

	// 主消息循环: 
	while (GetMessage(&msg, nullptr, 0, 0))//从调用线程的消息队列里取得一个消息并将其放于指定的结构
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))//处理菜单命令中的加速键
		{
			TranslateMessage(&msg);// 传递消息，将虚拟键消息转换为字符消息
			DispatchMessage(&msg);//把虚拟键消息转换为字符消息，并不会修改原有的消息，它只是产生新的消息并投递到消息队列中。
		}
	}

	return (int)msg.wParam;
}



//
//  函数: MyRegisterClass()
//
//  目的: 注册窗口类。
//
ATOM MyRegisterClass(HINSTANCE hInstance) // 注册窗口函数,实际调用的api 是  RegisterClassEx
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	//LoadIcon（）从与hInstance模块相关联的可执行文件中装入lpIconName指定的图标资源
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_WINDOWSPROJECT1));
	//LoadCursor（）从一个与应用事例相关的可执行文件（EXE文件）中载入指定的光标资源
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	//MAKEINTRESOURCEW是一个资源名转换的宏，这个宏是把一个数字类型转换成指针类型的宏
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_WINDOWSPROJECT1);
	wcex.lpszClassName = szWindowClass;
	//LoadIcon（）从与hInstance模块相关联的可执行文件中装入lpIconName指定的图标资源
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

//
//   函数: InitInstance(HINSTANCE, int)
//
//   目的: 保存实例句柄并创建主窗口
//
//   注释: 
//
//        在此函数中，我们在全局变量中保存实例句柄并
//        创建和显示主程序窗口。
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)// 初始化实例函数,此函数功能是创建并显示窗口,实际使用的api 是CreateWindow,,ShowWindow
{
	hInst = hInstance; // 将实例句柄存储在全局变量中

	HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);//发送一个WM_PAINT消息来更新指定窗口

	return TRUE;
}

//
//  函数: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  目的:    处理主窗口的消息。
//
//  WM_COMMAND  - 处理应用程序菜单
//  WM_PAINT    - 绘制主窗口
//  WM_DESTROY  - 发送退出消息并返回
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_COMMAND://WM_COMMAND：0x0111
	{
		int wmId = LOWORD(wParam);
		// 分析菜单选择: 
		switch (wmId)
		{
		case IDM_ABOUT: //IDM_ABOUT=104
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);//从一个对话框资源中创建一个模态对话框。
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);//销毁指定的窗口
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);//调用缺省的窗口过程来为应用程序没有处理的任何窗口消息提供缺省的处理
		}
	}
	break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: 在此处添加使用 hdc 的任何绘图代码...
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);//调用缺省的窗口过程来为应用程序没有处理的任何窗口消息提供缺省的处理
	}
	return 0;
}

// “关于”框的消息处理程序。
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);// 避免编译器关于未引用参数的警告
	switch (message)
	{
	case WM_INITDIALOG://WM_INITDIALOG：0x0110
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));//清除一个模态对话框,并使系统中止对对话框的任何处理的函数
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
