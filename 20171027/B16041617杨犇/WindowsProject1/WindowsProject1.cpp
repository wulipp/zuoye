// WINDOWSPROJECT1.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "resource.h"

#define MAX_LOADSTRING 100					//define global constant

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];								// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];								// The title bar text

// Foward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);   
BOOL				InitInstance(HINSTANCE, int);           
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
 	// TODO: Place code here.
	MSG msg;                         //Define MSG structure variables
	HACCEL hAccelTable;              //Call up the accelerator key

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);                         //Load the string into the cstring object
	LoadString(hInstance, IDC_WINDOWSPROJECT1, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow)) 
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDC_WINDOWSPROJECT1);               //Call up the accelerator key

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))                                              //do when there is a message
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))                       //Shortcuts to respond to messages
		{
			TranslateMessage(&msg);                                                   //Converts a virtual key message to a character message
			DispatchMessage(&msg);                                                    //Schedule the message obtained from GetMessage to the window program
		}
	}

	return msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage is only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;                                                              //Declare a window class wcex

	wcex.cbSize = sizeof(WNDCLASSEX);                                             //The size of the window

	wcex.style			= CS_HREDRAW | CS_VREDRAW;                                //Window style
	wcex.lpfnWndProc	= (WNDPROC)WndProc;                                       //Message handling function, window handling pointer
	wcex.cbClsExtra		= 0;                                                      //The number of additional bytes after the window class structure
	wcex.cbWndExtra		= 0;                                                      //The number of additional words after the window instance
	wcex.hInstance		= hInstance;                                              //Instance handle
	wcex.hIcon			= LoadIcon(hInstance, (LPCTSTR)IDI_WINDOWSPROJECT1);      //The handle of the icon
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);                            //The handle of the cursor
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);                               //Background brush handle
	wcex.lpszMenuName	= (LPCWSTR)IDC_WINDOWSPROJECT1;                           //Pointer to the menu
	wcex.lpszClassName	= szWindowClass;                                          //Class name pointer
	wcex.hIconSm		= LoadIcon(wcex.hInstance, (LPCTSTR)IDI_SMALL);           //A small icon associated with a window class

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HANDLE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,                              //transfer CreateWindow
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);                                                                  //Sets the display state of the specified window
   UpdateWindow(hWnd);                                                                          //Update the client area of the specified window

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, unsigned, WORD, LONG)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;                                                                             //Contains information for drawing window client areas
	HDC hdc;                                                                                    //HDC is the Windows device description table handle
	TCHAR szHello[MAX_LOADSTRING]=TEXT("hello world");                                                //Character set compatible
	LoadString(hInst, IDS_HELLO, szHello, MAX_LOADSTRING);
	
	switch (message) 
	{
		case WM_COMMAND:                                                                        //do if the program code is used in the window menu program
			wmId    = LOWORD(wParam); 
			wmEvent = HIWORD(wParam); 
			// Parse the menu selections:
			switch (wmId)
			{
				case IDM_ABOUT:
				   DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);               //Create a modal dialog box from a dialog resource
				   break;
				case IDM_EXIT:
				   DestroyWindow(hWnd);
				   break;
				default:
				   return DefWindowProc(hWnd, message, wParam, lParam);                         //Call the default window procedure to provide default processing for any window messages that the application has not processed
			}
			break;
		case WM_PAINT:                                                                          //do if a part of the window display area shows the contents or all becomes invalid                                                  
			hdc = BeginPaint(hWnd, &ps);
			// TODO: Add any drawing code here...
			RECT rt;
			GetClientRect(hWnd, &rt);                                                           //Gets the size of the window client area
			DrawText(hdc, szHello, 6, &rt, DT_CENTER);                                          //print function
			EndPaint(hWnd, &ps);                                                                //Start drawing for the specified window EndPaint is the end of the drawing, releasing the drawing area
			break;
		case WM_DESTROY:                                                                        //If the window is destroyed
			PostQuitMessage(0);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
   }
   return 0;
}

// Mesage handler for about box.
LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_INITDIALOG:
				return TRUE;

		case WM_COMMAND:
			if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)                             //LOWORD:Retrieves the low-order word from the specified value.
			{
				EndDialog(hDlg, LOWORD(wParam));                                                  //Clear a modal dialog box
				return TRUE;
			}
			break;
	}
    return FALSE;
}
