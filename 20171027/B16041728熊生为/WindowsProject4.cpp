//WindowsProject4.cpp: 定义应用程序的入口点。
//

#include "stdafx.h"
#include "WindowsProject4.h"

#define MAX_LOADSTRING 100

// 全局变量: 
HINSTANCE hInst;                                // 当前实例
WCHAR szTitle[MAX_LOADSTRING];                  // 标题栏文本
WCHAR szWindowClass[MAX_LOADSTRING];            // 主窗口类名

// 此代码模块中包含的函数的前向声明: 
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int); //在全局变量中保存实例句柄并创建和显示主窗口
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM); //主窗口处理函数的声明
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM); //关于框的处理程序

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: 在此放置代码。

    // 初始化全局字符串
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_WINDOWSPROJECT4, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // 执行应用程序初始化: 
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_WINDOWSPROJECT4));

    MSG msg;

    // 主消息循环: 
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);  //翻译消息
            DispatchMessage(&msg);    //传送消息
        }
    }

    return (int) msg.wParam;
}



//
//  函数: MyRegisterClass()
//
//  目的: 注册窗口类。
//
ATOM MyRegisterClass(HINSTANCE hInstance)  //注册窗口类
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);  //窗口类的大小

    wcex.style          = CS_HREDRAW | CS_VREDRAW;  //窗口类型为默认类型
    wcex.lpfnWndProc    = WndProc;          //窗口处理函数为WinProc
    wcex.cbClsExtra     = 0;            //窗口类无拓展
    wcex.cbWndExtra     = 0;            //窗口实例无拓展
    wcex.hInstance      = hInstance;    //当前实例句柄
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_WINDOWSPROJECT4)); //窗口的图标为默认图标
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW); //窗口采用箭头光标
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);        //窗口背景为白色
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_WINDOWSPROJECT4);  //窗口采用默认菜单
    wcex.lpszClassName  = szWindowClass;                    //设置窗口类名
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL)); //窗口的小图标为默认图标

    return RegisterClassExW(&wcex);  //返回注册的窗口
}

//
//   函数: InitInstance(HINSTANCE, int)
//
//   目的: 保存实例句柄并创建主窗口
//
//   注释: 
//
//        在此函数中，我们在全局变量中保存实例句柄并
//        创建和显示主程序窗口。
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)  //保存实例句柄并创建主窗口
{
   hInst = hInstance; // 将实例句柄存储在全局变量中

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr); //创建窗口

   if (!hWnd)         //如果全局变量为空指针，则返回false   
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);      //显示窗口
   UpdateWindow(hWnd);              //绘制用户区

   return TRUE;               //创建成功
}

//
//  函数: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  目的:    处理主窗口的消息。
//
//  WM_COMMAND  - 处理应用程序菜单
//  WM_PAINT    - 绘制主窗口
//  WM_DESTROY  - 发送退出消息并返回
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)// 窗口处理函数
{
    switch (message)
    {
    case WM_COMMAND:  //当用户从菜单选中一个命令项目、当一个控件发送通知消息给去父窗口或者按下一个快捷键将发送 WM_COMMAND 消息
        {
            int wmId = LOWORD(wParam);
            // 分析菜单选择: 
            switch (wmId)
            {
            case IDM_ABOUT:  //关于框消息的处理
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:  //当消息来自于退出菜单时
                DestroyWindow(hWnd); //消除窗口
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam); //默认时采用系统消息默认处理函数
            }
        }
        break;
    case WM_PAINT:  //绘制用户区
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: 在此处添加使用 hdc 的任何绘图代码...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:  //消除窗口
        PostQuitMessage(0); //调用PostQuitMessage发出WM_QIUT消息
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);//采用系统消息默认处理函数
    }
    return 0;
}

// “关于”框的消息处理程序。
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam)); //绘制文本区
            return (INT_PTR)TRUE; //处理成功
        }
        break;
    }
    return (INT_PTR)FALSE;  //框消息处理失败
}
