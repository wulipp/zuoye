// win32.cpp: 定义应用程序的入口点。
//

#include "stdafx.h"
#include "WindowsProject1.h"

#define MAX_LOADSTRING 100

// 全局变量: 
HINSTANCE hInst;                                                    //当前实例
WCHAR szTitle[MAX_LOADSTRING];						                // 标题栏文本
WCHAR szWindowClass[MAX_LOADSTRING];                                // 主窗口类名

// 此代码模块中包含的函数的前向声明:																	 

//ATOM是WORD的别名，WORD是unsigned short的别名

ATOM    MyRegisterClass(HINSTANCE hInstance);//注册窗口函数的声明											 
//BOOL是int的别名   InitInstance(HINSTANCE, int)保存实例句柄并创建主窗口
BOOL    InitInstance(HINSTANCE, int);// 初始化实例的函数声明	LRESULT是long型_W64类型的别名
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);//主窗口的回调函数 
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM); //INT_PTR是int型_W64类型的别名   
 // win32程序入口函数	APIENTRY是__stdcall的别名
int APIENTRY wWinMain(_In_ HINSTANCE hInstance,//应用程序当前实例句柄
	_In_opt_ HINSTANCE hPrevInstance,//应用程序其他实例句柄
	_In_ LPWSTR    lpCmdLine, //指向程序命令行参数的指针   LPWSTR是WHAR类型的指针
	_In_ int       nCmdShow)//应用程序开始执行时窗口显示方式的整数值标识
{
	//UNREFERENCED_PARAMETER告诉编译器，已经使用了该变量，不必检测警告！
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: 在此放置代码。

	// 初始化全局字符串
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);//将编号为IDS_APP_TITLE的字符串资源赋值给窗口的标题szTitle
	LoadStringW(hInstance, IDC_WINDOWSPROJECT1, szWindowClass, MAX_LOADSTRING);//将编号为IDC_WINDOWSPROJECT1的字符串资源赋值给窗口类名szWindowClass
	MyRegisterClass(hInstance);// 调用注册窗口类
	 // 执行应用程序初始化: 
	if (!InitInstance(hInstance, nCmdShow)) 
	{
		return FALSE;
	}
	//HACCEL加速键表句柄	MAKEINTRESOURCE：把一个数字类型转换成指针类型的宏
	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_WINDOWSPROJECT1));// 导入加速键表
	
	MSG msg;

	// 主消息循环: 
	while (GetMessage(&msg, nullptr, 0, 0))//从调用线程的消息队列里取得一个消息并将其放于指定的结构
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))//处理菜单命令中的加速键
		{
			TranslateMessage(&msg);// 传递消息，将虚拟键消息转换为字符消息
			DispatchMessage(&msg);//把虚拟键消息转换为字符消息，并不会修改原有的消息，它只是产生新的消息并投递到消息队列中。
		}
	}

	return (int)msg.wParam;
}



//
//  函数: MyRegisterClass()
//
//  目的: 注册窗口类。
//
ATOM MyRegisterClass(HINSTANCE hInstance) // 注册窗口函数,实际调用的api 是  RegisterClassEx
{
	WNDCLASSEXW wcex;//窗口类
	//大小
	wcex.cbSize = sizeof(WNDCLASSEX);//风格
	//CS_HREDRAW：一旦移动或尺寸调整使客户区的宽度发生变化，就重新绘制窗口
	//CS_VREDRAW：一旦移动或尺寸调整使客户区的高度发生变化，就重新绘制窗口
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;//窗口处理函数
	wcex.cbClsExtra = 0;//窗口类无扩展
	wcex.cbWndExtra = 0;//窗口类无扩展
	wcex.hInstance = hInstance;//当前实例
	//LoadIcon（）从与hInstance模块相关联的可执行文件中装入lpIconName指定的图标资源
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_WINDOWSPROJECT1));
	//LoadCursor（）从一个与应用事例相关的可执行文件（EXE文件）中载入指定的光标资源
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);//设置窗口背景颜色
	//MAKEINTRESOURCEW是一个资源名转换的宏，这个宏是把一个数字类型转换成指针类型的宏
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_WINDOWSPROJECT1);//设置菜单
	wcex.lpszClassName = szWindowClass;//设置窗口类名
	//LoadIcon（）从与hInstance模块相关联的可执行文件中装入lpIconName指定的图标资源
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

//
//   函数: InitInstance(HINSTANCE, int)
//
//   目的: 保存实例句柄并创建主窗口
//
//   注释: 
//
//        在此函数中，我们在全局变量中保存实例句柄并
//        创建和显示主程序窗口。
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)// 初始化实例函数,此函数功能是创建并显示窗口,实际使用的api 是CreateWindow,,ShowWindow
{
	hInst = hInstance; // 将实例句柄存储在全局变量中
	//创建窗口   HWND：窗口句柄   WS_OVERLAPPEDWINDOW：创建一个拥有各种窗口风格的窗体，包括标题，系统菜单，边框，最小化和最大化按钮等
	HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);
	//CW_USEDEFAULT：表示窗口的位置和大小由系统默认设置
	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);//显示窗口
	UpdateWindow(hWnd);//绘制窗口

	return TRUE;
}

//
//  函数: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  目的:    处理主窗口的消息。
//
//  WM_COMMAND  - 处理应用程序菜单
//  WM_PAINT    - 绘制主窗口
//  WM_DESTROY  - 发送退出消息并返回
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_COMMAND://WM_COMMAND：处理材料
	{
		int wmId = LOWORD(wParam);//检索低顺序单词
		// 分析菜单选择: 
		switch (wmId)
		{
		case IDM_ABOUT: //“关于”框
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);//从一个对话框资源中创建一个模态对话框。
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);//销毁指定的窗口
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);//调用缺省的窗口过程来为应用程序没有处理的任何窗口消息提供缺省的处理
		}
	}
	break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;//包含了某应用程序用来绘制它所拥有的窗口客户区所需要的信息
		HDC hdc = BeginPaint(hWnd, &ps);//开始绘制
		// TODO: 在此处添加使用 hdc 的任何绘图代码...
		EndPaint(hWnd, &ps);//结束绘制
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);//该函数向系统表明有个线程有终止请求
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);//调用缺省的窗口过程来为应用程序没有处理的任何窗口消息提供缺省的处理
	}
	return 0;
}

// “关于”框的消息处理程序。
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);// 避免编译器关于未引用参数的警告
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND://IDOK：“确定”按钮	IDCANCEL：“取消”按钮
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));//清除一个模态对话框,并使系统中止对对话框的任何处理的函数
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
