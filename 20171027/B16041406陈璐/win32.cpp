// win32.cpp : Defines the entry point for the application.
//

//#include "stdafx.h"
//#include "resource.h"

#define MAX_LOADSTRING 100          //宏定义 MAX_LOADSTRING的大小为100  

// 全局变量的:          
HINSTANCE hInst;								// 当前实例。一个实例的句柄。这是模块在内存中的基址。 
TCHAR szTitle[MAX_LOADSTRING];								// The title bar text  存储通过条件编译（通过_UNICODE和UNICODE宏）控制实际使用的字符集，当没有定义_UNICODE宏时，TCHAR = char，_tcslen =strlen，当定义了_UNICODE宏时，TCHAR = wchar_t ， _tcslen = wcslen 
TCHAR szWindowClass[MAX_LOADSTRING];								// The title bar text

// Foward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);//这是一个注册窗口类函数 一个ATOM表是存储字符串和相应的标识符的系统定义的表。应用程序在一个原子表中放置一个字符串，并接收一个16位整数，可用于访问该字符串。
BOOL				InitInstance(HINSTANCE, int);//MFC的CWinApp类的成员函数 
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);//处理发送到窗口的消息的应用程序定义的函数。所述WNDPROC类型定义一个指向这个回调函数。 
LRESULT CALLBACK	About(HWND, UINT, WPARAM, LPARAM);//About对话框的对话框处理函数,处理About对话框的各种消息 

int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)//每个Windows程序都包含一个名为WinMain或wWinMain的入口函数。这是wWinMain的签名。该函数返回一个int值。操作系统不使用返回值，但是可以使用返回值将状态码传递给您编写的其他程序。
{
 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);//从与指定模块关联的可执行文件加载字符串资源，将该字符串复制到缓冲区中，并附加一个终止空字符。
	LoadString(hInstance, IDC_WIN32, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);//注册一个窗口类，以便随后用于调用CreateWindow或CreateWindowEx函数。 

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow)) //Windows允许同一个程序的多个副本同时运行。
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDC_WIN32);//加载指定的加速器表。

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0)) //从调用线程的消息队列中检索消息。该函数调度传入的已发送消息，直到发布的消息可用于检索。
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) //处理菜单命令的加速键。该函数将WM_KEYDOWN或WM_SYSKEYDOWN消息转换为WM_COMMAND或WM_SYSCOMMAND消息（如果在指定的加速器表中存在键的条目），然后将WM_COMMAND或WM_SYSCOMMAND消息直接发送到指定的窗口过程。TranslateAccelerator在窗口过程处理消息之前不会返回。
		{
			TranslateMessage(&msg);//将虚拟键消息转换为字符消息。字符消息被发送到调用线程的消息队列，在下一次线程调用GetMessage或PeekMessage函数时读取。
			DispatchMessage(&msg);//将消息分派给窗口过程。它通常用于分派由GetMessage函数检索的消息。
		}
	}

	return msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage is only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;//定义结构。包含窗口类信息。它与RegisterClassEx和GetClassInfoEx   函数一起使用。

	wcex.cbSize = sizeof(WNDCLASSEX); // //结构的大小 

	wcex.style			= CS_HREDRAW | CS_VREDRAW;// //从这个窗口类派生的窗口具有的风格
	wcex.lpfnWndProc	= (WNDPROC)WndProc;//窗口处理函数的指针 
	wcex.cbClsExtra		= 0;//指定紧跟在窗口类结构后的附加字节数
	wcex.cbWndExtra		= 0;//指定紧跟在窗口事例后的附加字节数
	wcex.hInstance		= hInstance;;//本模块的事例句柄
	wcex.hIcon			= LoadIcon(hInstance, (LPCTSTR)IDI_WIN32);//图标的句柄
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);//光标的句柄
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);//背景画刷的句柄
	wcex.lpszMenuName	= (LPCSTR)IDC_WIN32;//指向菜单的指针
	wcex.lpszClassName	= szWindowClass;//指向类名称的指针
	wcex.hIconSm		= LoadIcon(wcex.hInstance, (LPCTSTR)IDI_SMALL);//和窗口类关联的小图标

	return RegisterClassEx(&wcex);//注册窗口类
}

//
//   FUNCTION: InitInstance(HANDLE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;//HWND结构，允许托管应用程序编程接口（API）访问Microsoft DirectX API的非托管部分。

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);//显示窗口 
   UpdateWindow(hWnd);//关闭窗口 

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, unsigned, WORD, LONG)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;//该PAINTSTRUCT结构包含应用程序的信息。这些信息可以用来绘制该应用程序拥有的窗口的客户区域。
	HDC hdc;//HCD结构，允许托管应用程序编程接口（API）访问Microsoft DirectX API的非托管部分。
	TCHAR szHello[MAX_LOADSTRING];
	LoadString(hInst, IDS_HELLO, szHello, MAX_LOADSTRING);

	switch (message) 
	{
		case WM_COMMAND:
			wmId    = LOWORD(wParam); 
			wmEvent = HIWORD(wParam); 
			// Parse the menu selections:
			switch (wmId)
			{
				case IDM_ABOUT:
				   DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);//从对话框模板资源中创建一个模式对话框。在指定的回调函数通过调用EndDialog函数终止模式对话框之前，DialogBox不会返回控件。DialogBox实现为对DialogBoxParam函数的调用
				   break;
				case IDM_EXIT:
				   DestroyWindow(hWnd);//销毁指定的窗口。该函数将WM_DESTROY和WM_NCDESTROY消息发送到该窗口来停用它，并从中移除键盘焦点。该函数还销毁窗口的菜单，刷新线程消息队列，破坏定时器，删除剪贴板所有权，并打破剪贴板查看器链（如果窗口位于查看器链的顶部）。
				   break;
				default:
				   return DefWindowProc(hWnd, message, wParam, lParam);
			}
			break;
		case WM_PAINT:
			hdc = BeginPaint(hWnd, &ps);//该调用BeginPaint函数准备指定的窗口进行绘画和填充一个PAINTSTRUCT结构有关绘画的信息。
			// TODO: Add any drawing code here...
			RECT rt;//RECT结构限定的矩形的左上角和右下角的坐标。 
			GetClientRect(hWnd, &rt);//检索窗口客户区域的坐标。客户端坐标指定客户区的左上角和右下角。由于客户端坐标相对于窗口客户区的左上角，所以左上角的坐标是（0,0）。
			DrawText(hdc, szHello, strlen(szHello), &rt, DT_CENTER);//该DrawText的函数绘制格式化指定的矩形文本。它根据指定的方法格式化文本（扩展选项卡，对齐字符，断行等等）。
			EndPaint(hWnd, &ps);//该调用EndPaint函数标记画在指定的窗口结束。每次调用BeginPaint函数都需要该函数，但是只有在绘制完成后才需要
			break;
		case WM_DESTROY:
			PostQuitMessage(0);//向系统表示一个线程已经提出终止请求（退出）。它通常用于响应WM_DESTROY消息。
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);//调用默认窗口过程为应用程序不处理的任何窗口消息提供默认处理。这个函数确保每个消息都被处理。调用DefWindowProc时，窗口过程接收到相同的参数。
   }
   return 0;
}

// Mesage handler for about box.
LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_INITDIALOG:
				return TRUE;

		case WM_COMMAND:
			if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) 
			{
				EndDialog(hDlg, LOWORD(wParam));//销毁一个模式对话框，导致系统结束对话框的任何处理。
				return TRUE;
			}
			break;
	}
    return FALSE;
}
