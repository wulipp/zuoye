﻿作业在本文件夹中放在自己的 “学号姓名/README.md”文件中，比如 B120416张巧/README.md

一、参照如下图用AXCURE画俄罗斯方块的原型图

![原型图示例](https://gitee.com/YangYuHao1/zuoye/blob/master/20171123/B16041420%E6%9D%A8%E5%AE%87%E8%B1%AA/Tetris.pdf "Tetris.pdf")


二、调试20171114目录下能运行的俄罗斯方块程序，将程序的各函数的流程图用思维导图画出来，将思维导图中的流程图放到自己的“学号姓名/README.md”文件中