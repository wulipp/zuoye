## 11.23 作业 Tetris原型图

### 原型图

![Tetris原型图](https://gitee.com/uploads/images/2017/1220/103202_2d9652ab_1578283.png "TetrisIndex.png")

### 函数流程图

![Tetris函数](https://gitee.com/uploads/images/2017/1221/171843_70e37318_1578283.png "Tetris函数.png")
