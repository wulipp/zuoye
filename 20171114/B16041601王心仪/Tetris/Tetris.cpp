// Tetris.cpp : 定义应用程序的入口点。
//

/*
记录于2016-5-29 星期日    由于闪屏过于显眼，采用双缓冲

*/

#include "stdafx.h"
#include "Tetris.h"
#include <windows.h>
#include <mmsystem.h>
#pragma comment(lib, "WINMM.LIB")
#define MAX_LOADSTRING 100

// 此代码模块中包含的函数的前向声明:
ATOM                MyRegisterClass(HINSTANCE hInstance);//unsigned short
BOOL                InitInstance(HINSTANCE, int);//int
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);//long
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);//int

// 全局变量:
HINSTANCE hInst;                                // 当前实例
TCHAR szTitle[MAX_LOADSTRING];                  // 标题栏文本
TCHAR szWindowClass[MAX_LOADSTRING];            // 主窗口类名

int APIENTRY _tWinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPTSTR    lpCmdLine,
	int       nCmdShow)
{
	init_game();//初始化
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: 在此放置代码。
	MSG msg;
	HACCEL hAccelTable;

	// 初始化全局字符串
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_TETRIS, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// 执行应用程序初始化:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;//0
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_TETRIS));

	// 主消息循环:
	while (1)
	{
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			if (msg.message == WM_QUIT)
			{
				break;
			}
		}
		else
		{
			if ((GAME_STATE & 2) != 0)
			{
				tCur = GetTickCount();////返回（retrieve）从操作系统启动所经过（elapsed）的毫秒数
				if (tCur - tPre>g_speed)////超过反应时间时
				{

					int flag = CheckValide(curPosX, curPosY + 1, bCurTeris);////判断下一行的情况，返回给flag
					if (flag == 1)//正常下降一行
					{
						curPosY++;
						tPre = tCur;
						HWND hWnd = GetActiveWindow();
						InvalidateRect(hWnd, &rc_left, FALSE);
						InvalidateRect(hWnd, &rc_right_top, FALSE);
					}
					else if (flag == -2)
					{
						g_speed = t_speed;
						fillBlock();
						checkComplite();
						setRandomT();
						curPosX = (NUM_X - 4) >> 1;
						curPosY = 0;
						HWND hWnd = GetActiveWindow();
						InvalidateRect(hWnd, &rc_main, FALSE);
					}
					else if (flag == -3)
					{
						HWND hWnd = GetActiveWindow();
						if (MessageBox(hWnd, L"胜败乃兵家常事，菜鸡请重新来过", L":时光机", MB_YESNO) == IDYES)
						{
							init_game();
						}
						else
						{
							break;
						}
					}
				}
			}
		}
	}

	return (int)msg.wParam;
}


//
//  函数: MyRegisterClass()
//
//  目的: 注册窗口类。
//
//  注释:
//
//    仅当希望
//    此代码与添加到 Windows 95 中的“RegisterClassEx”
//    函数之前的 Win32 系统兼容时，才需要此函数及其用法。调用此函数十分重要，
//    这样应用程序就可以获得关联的
//    “格式正确的”小图标。
//
ATOM MyRegisterClass(HINSTANCE hInstance)//unsigned short
{
	WNDCLASSEX wcex;//窗口类

	wcex.cbSize = sizeof(WNDCLASSEX);//窗口类的结构的大小

	wcex.style = CS_HREDRAW | CS_VREDRAW;//窗口类的样式
	wcex.lpfnWndProc = WndProc;//指向窗口的函数的指针
	wcex.cbClsExtra = 0;//分配在窗口类结构后的字节数
	wcex.cbWndExtra = 0;//分配在窗口类实例后的字节数
	wcex.hInstance = hInstance;//定义窗口类的应用程序的实例句柄
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_TETRIS));//窗口类的图标  LoadIcon:从与hInstance模块相关联的可执行文件中装入lpIconName指定的图标资源，仅当图标资源还没有被装入时该函数才执行装入操作，否则只获取装入的资源句柄
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);//窗口类的光标 LoadCursor:该函数从一个与应用事例相关的可执行文件（EXE文件）中载入指定的光标资源
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);//窗口类的背景刷 5+1
	wcex.lpszMenuName = MAKEINTRESOURCE(IDC_TETRIS);//窗口类菜单资源名 109 MAKEINTRESOURCE:把一个数字类型转换成指针类型的宏
	wcex.lpszClassName = szWindowClass;//窗口类名
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));//窗口类的小图标

	return RegisterClassEx(&wcex);//为随后在调用Createwindow函数和CreatewindowEx函数中使用的窗口注册一个窗口类
}

//
//   函数: InitInstance(HINSTANCE, int)
//
//   目的: 保存实例句柄并创建主窗口
//
//   注释:
//
//        在此函数中，我们在全局变量中保存实例句柄并
//        创建和显示主程序窗口。
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND hWnd;//标识窗口句柄

	hInst = hInstance; // 将实例句柄存储在全局变量中

	hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);
	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);//更新指定窗口的客户区

	return TRUE;
}

//
//  函数: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  目的: 处理主窗口的消息。
//
//  WM_COMMAND  - 处理应用程序菜单
//  WM_PAINT    - 绘制主窗口
//  WM_DESTROY  - 发送退出消息并返回
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)//long
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;//包含了某应用程序用来绘制它所拥有的窗口客户区所需要的信息
	HDC hdc;
	int nWinx, nWiny, nClientX, nClientY;
	int posX, posY;
	RECT rect;
	HMENU hSysmenu;
	switch (message)
	{
	case WM_CREATE:


		GetWindowRect(hWnd, &rect);//返回指定窗口的边框矩形的尺寸
		nWinx = 530;
		nWiny = 680;
		posX = GetSystemMetrics(SM_CXSCREEN);//获取系统分辨率、窗体显示区域的宽度和高度、滚动条的宽度和高度   0
		posY = GetSystemMetrics(SM_CYSCREEN);//1
		posX = (posX - nWinx) >> 1;
		posY = (posY - nWiny) >> 1;
		GetClientRect(hWnd, &rect);//获取窗口客户区的大小
		nClientX = rect.right - rect.left;
		nClientY = rect.bottom - rect.top;

		MoveWindow(hWnd, posX, posY, 530, 680, TRUE);//改变指定窗口的位置和大小
		hSysmenu = GetSystemMenu(hWnd, false);//允许应用程序为复制或修改而访问窗口菜单（系统菜单或控制菜单）
		AppendMenu(hSysmenu, MF_SEPARATOR, 0, NULL);//在指定的菜单条、下拉式菜单、子菜单或快捷菜单的末尾追加一个新菜单项
		AppendMenu(hSysmenu, 0, IDM_DIFF, L"难度选择");
		break;
	case WM_COMMAND:
		wmId = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// 分析菜单选择:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);//关于弹窗
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);//关闭程序
			break;
		case ID_dif1:
			selectDiffculty(hWnd, 1);
			break;
		case ID_dif2:
			selectDiffculty(hWnd, 2);
			break;
		case ID_dif3:
			selectDiffculty(hWnd, 3);
			break;
		case ID_LAYOUT1:
			selectLayOut(hWnd, 1);
			break;
		case ID_LAYOUT2:
			selectLayOut(hWnd, 2);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);//调用缺省的窗口过程来为应用程序没有处理的任何窗口消息提供缺省的处理
		}
		break;
	case WM_KEYDOWN://0x0100
		hdc = GetDC(hWnd);//能使应用程序更多地控制在客户区域内如何或是否发生剪切
		InvalidateRect(hWnd, NULL, false);//向指定的窗体更新区域添加一个矩形，然后窗口客户区域的这一部分将被重新绘制
		switch (wParam)
		{
		case VK_LEFT:// 0x25
			curPosX--;
			if (CheckValide(curPosX, curPosY, bCurTeris) != 1)
			{
				curPosX++;
			}
			break;
		case VK_RIGHT://0x27
			curPosX++;
			if (CheckValide(curPosX, curPosY, bCurTeris) != 1)
			{
				curPosX--;
			}
			break;
		case VK_UP://0x26
			RotateTeris(bCurTeris);
			break;
		case VK_DOWN://0x28
			if (g_speed == t_speed)
				g_speed = 10;
			else
				g_speed = t_speed;
			//outPutBoxInt(g_speed);
			break;
		case 'W':
			RotateTeris(bCurTeris);
			break;
		case 'A':
			curPosX--;
			if (CheckValide(curPosX, curPosY, bCurTeris) != 1)
			{
				curPosX++;
			}
			break;
		case 'D':
			curPosX++;
			if (CheckValide(curPosX, curPosY, bCurTeris) != 1)
			{
				curPosX--;
			}
			break;

		case 'S':
			if (g_speed == t_speed)
				g_speed = 10;
			else
				g_speed = t_speed;
			//outPutBoxInt(g_speed);
			break;
		default:
			break;
		}

	case WM_PAINT://0x000F
		hdc = BeginPaint(hWnd, &ps);//为指定窗口进行绘图工作的准备，并用将和绘图有关的信息填充到一个PAINTSTRUCT结构中
		// TODO: 在此添加任意绘图代码...
		DrawBackGround(hdc);
		EndPaint(hWnd, &ps);//函数为指定窗口进行绘图的开始EndPaint是绘图的结束，释放绘图区
		break;
	case WM_DESTROY://0x0002
		PostQuitMessage(0);//该函数向系统表明有个线程有终止请求
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);//调用缺省的窗口过程来为应用程序没有处理的任何窗口消息提供缺省的处理
	}
	return 0;
}

// “关于”框的消息处理程序。
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG://0x0110
		return (INT_PTR)TRUE;

	case WM_COMMAND://0x0111
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));//清除一个模态对话框,并使系统中止对对话框的任何处理的函数
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}


void drawBlocked(HDC mdc)
{
	int i, j;

	HBRUSH hBrush = (HBRUSH)CreateSolidBrush(RGB(255, 255, 0));//该函数创建一个具有指定颜色的逻辑刷子

	SelectObject(mdc, hBrush);//计算机编程语言函数，该函数选择一对象到指定的设备上下文环境中，该新对象替换先前的相同类型的对象

	for (i = 0; i<NUM_Y; i++)//20
	{
		for (j = 0; j<NUM_X; j++)//10
		{
			if (g_hasBlocked[i][j])
			{
				Rectangle(mdc, BORDER_X + j*BLOCK_SIZE, BORDER_Y + i*BLOCK_SIZE,//画一个矩形，可以用当前的画笔画矩形轮廓，用当前画刷进行填充
					BORDER_X + (j + 1)*BLOCK_SIZE, BORDER_Y + (i + 1)*BLOCK_SIZE
				);
			}
		}
	}
	DeleteObject(hBrush);//删除一个逻辑笔、画笔、字体、位图、区域或者调色板，释放所有与该对象有关的系统资源，在对象被删除之后，指定的句柄也就失效了
}

int CheckValide(int startX, int startY, BOOL bTemp[4][4])
{
	int i, j;
	for (i = 3; i >= 0; i--)
	{
		for (j = 3; j >= 0; j--)
		{
			if (bTemp[i][j])
			{
				if (j + startX<0 || j + startX >= NUM_X)
				{
					return -1;
				}
				if (i + startY >= NUM_Y)
				{
					return -2;
				}
				if (g_hasBlocked[i + startY][j + startX])
				{
					//outPutBoxInt(j+startY);
					if (curPosY == 0)
					{
						return -3;
					}
					return -2;
				}
			}
		}
	}
	//MessageBox(NULL,L"这里",L"as",MB_OK);
	//outPutBoxInt(curPosY);
	return 1;
}

void checkComplite()
{
	int i, j, k, count = 0;
	for (i = 0; i<NUM_Y; i++)
	{
		bool flag = true;
		for (j = 0; j<NUM_X; j++)
		{
			if (!g_hasBlocked[i][j])
			{
				flag = false;
			}
		}
		if (flag)
		{
			count++;
			for (j = i; j >= 1; j--)
			{
				for (k = 0; k<NUM_X; k++)
				{
					g_hasBlocked[j][k] = g_hasBlocked[j - 1][k];
				}

			}
			drawCompleteParticle(i);
			Sleep(300);

			PlaySound(_T("coin.wav"), NULL, SND_FILENAME | SND_ASYNC);//用于播放音乐
		}
	}
	GAME_SCORE += int(count*1.5);
}



VOID outPutBoxInt(int num)
{
	TCHAR szBuf[1024];
	LPCTSTR str = TEXT("%d");
	wsprintf(szBuf, str, num);//将一系列的字符和数值输入到缓冲区
	MessageBox(NULL, szBuf, L"aasa", MB_OK);
}

VOID outPutBoxString(TCHAR str[1024])
{
	TCHAR szBuf[1024];
	LPCTSTR cstr = TEXT("%s");
	wsprintf(szBuf, cstr, str);
	MessageBox(NULL, szBuf, L"aasa", MB_OK); //显示一个模态对话框，其中包含一个系统图标、 一组按钮和一个简短的特定于应用程序消息，如状态或错误的信息
}




void setRandomT()
{
	int rd_start = RandomInt(0, sizeof(state_teris) / sizeof(state_teris[0]));
	int rd_next = RandomInt(0, sizeof(state_teris) / sizeof(state_teris[0]));
	//outPutBoxInt(rd_start);
	//outPutBoxInt(rd_next);
	//outPutBoxInt(rd_start);
	if (GAME_STATE == 0)
	{
		GAME_STATE = GAME_STATE | 0x0001;
		//outPutBoxInt(GAME_STATE);
		memcpy(bCurTeris, state_teris[rd_start], sizeof(state_teris[rd_start]));
		memcpy(bNextCurTeris, state_teris[rd_next], sizeof(state_teris[rd_next]));
	}
	else
	{
		memcpy(bCurTeris, bNextCurTeris, sizeof(bNextCurTeris));
		memcpy(bNextCurTeris, state_teris[rd_next], sizeof(state_teris[rd_next]));
	}

}
void init_game()//初始化游戏
{
	GAME_SCORE = 0;
	setRandomT();
	curPosX = (NUM_X - 4) >> 1;
	curPosY = 0;


	memset(g_hasBlocked, 0, sizeof(g_hasBlocked));
	rc_left.left = 0;
	rc_left.right = SCREEN_LEFT_X;
	rc_left.top = 0;
	rc_left.bottom = SCREEN_Y;

	rc_right.left = rc_left.right + BORDER_X;
	rc_right.right = 180 + rc_right.left;
	rc_right.top = 0;
	rc_right.bottom = SCREEN_Y;

	rc_main.left = 0;
	rc_main.right = SCREEN_X;
	rc_main.top = 0;
	rc_main.bottom = SCREEN_Y;

	rc_right_top.left = rc_right.left;
	rc_right_top.top = rc_right.top;
	rc_right_top.right = rc_right.right;
	rc_right_top.bottom = (rc_right.bottom) / 2;

	rc_right_bottom.left = rc_right.left;
	rc_right_bottom.top = rc_right_top.bottom + BORDER_Y;
	rc_right_bottom.right = rc_right.right;
	rc_right_bottom.bottom = rc_right.bottom;

	g_speed = t_speed = 1000 - GAME_DIFF * 280;
}

void fillBlock()
{
	int i, j;
	for (i = 0; i<4; i++)
	{
		for (j = 0; j<4; j++)
		{
			if (bCurTeris[i][j])
			{
				g_hasBlocked[curPosY + i][curPosX + j] = 1;
			}
		}
	}
}

void RotateTeris(BOOL bTeris[4][4])
{
	BOOL bNewTeris[4][4];
	int x, y;
	for (x = 0; x<4; x++)
	{
		for (y = 0; y<4; y++)
		{
			bNewTeris[x][y] = bTeris[3 - y][x];
			//逆时针：
			//bNewTeris[x][y] = bTeris[y][3-x];
		}
	}
	if (CheckValide(curPosX, curPosY, bNewTeris) == 1)
	{
		memcpy(bTeris, bNewTeris, sizeof(bNewTeris));//从源src所指的内存地址的起始位置开始拷贝n个字节到目标dest所指的内存地址的起始位置中
	}

}

int RandomInt(int _min, int _max)
{
	srand((rd_seed++) % 65532 + GetTickCount());
	return _min + rand() % (_max - _min);
}

VOID DrawTeris(HDC mdc)
{

	int i, j;
	HPEN hPen = (HPEN)GetStockObject(BLACK_PEN);//检索预定义的备用笔、刷子、字体或者调色板的句柄
	HBRUSH hBrush = (HBRUSH)GetStockObject(WHITE_BRUSH);
	SelectObject(mdc, hPen);//选择一对象到指定的设备上下文环境中，该新对象替换先前的相同类型的对象
	SelectObject(mdc, hBrush);
	for (i = 0; i<4; i++)
	{
		for (j = 0; j<4; j++)
		{
			if (bCurTeris[i][j])
			{
				//使用该函数画一个矩形，可以用当前的画笔画矩形轮廓，用当前画刷进行填充
				Rectangle(mdc, (j + curPosX)*BLOCK_SIZE + BORDER_X, (i + curPosY)*BLOCK_SIZE + BORDER_Y, (j + 1 + curPosX)*BLOCK_SIZE + BORDER_X, (i + 1 + curPosY)*BLOCK_SIZE + BORDER_Y);
			}
		}
	}
	drawBlocked(mdc);
	DeleteObject(hPen);//删除一个逻辑笔、画笔、字体、位图、区域或者调色板，释放所有与该对象有关的系统资源
	DeleteObject(hBrush);
}

VOID DrawBackGround(HDC hdc)
{

	HBRUSH hBrush = (HBRUSH)GetStockObject(GRAY_BRUSH);//检索预定义的备用笔、刷子、字体或者调色板的句柄
	HDC mdc = CreateCompatibleDC(hdc);//创建一个与指定设备兼容的内存设备上下文环境
	HBITMAP hBitmap = CreateCompatibleBitmap(hdc, SCREEN_X, SCREEN_Y);//该函数创建与指定的设备环境相关的设备兼容的位图

	SelectObject(mdc, hBrush);//函数选择一对象到指定的设备上下文环境中，该新对象替换先前的相同类型的对象
	SelectObject(mdc, hBitmap);

	HBRUSH hBrush2 = (HBRUSH)GetStockObject(WHITE_BRUSH);
	FillRect(mdc, &rc_main, hBrush2);//用指定的画刷填充矩形
	Rectangle(mdc, rc_left.left + BORDER_X, rc_left.top + BORDER_Y, rc_left.right, rc_left.bottom);
	Rectangle(mdc, rc_right.left + BORDER_X, rc_right.top + BORDER_Y, rc_right.right, rc_right.bottom);
	DrawTeris(mdc);
	drawNext(mdc);
	drawScore(mdc);
	::BitBlt(hdc, 0, 0, SCREEN_X, SCREEN_Y, mdc, 0, 0, SRCCOPY);//对指定的源设备环境区域中的像素进行位块（bit_block）转换
	DeleteObject(hBrush);
	DeleteDC(mdc);
	DeleteObject(hBitmap);
	DeleteObject(hBrush2);


	//  int x,y;
	//  HPEN hPen = (HPEN)GetStockObject(NULL_PEN);
	//  HBRUSH hBrush = (HBRUSH)GetStockObject(GRAY_BRUSH);
	//  SelectObject(hdc,hPen);
	//  SelectObject(hdc,hBrush);
	//  for (x = 0;x<NUM_X;x++)
	//  {
	//      for(y=0;y<NUM_Y;y++)
	//      {
	//          Rectangle(hdc,BORDER_X+x*BLOCK_SIZE,BORDER_Y+y*BLOCK_SIZE,
	//              BORDER_X+(x+1)*BLOCK_SIZE,
	//              BORDER_Y+(y+1)*BLOCK_SIZE);
	//      }
	//  }
}

void drawNext(HDC hdc)
{
	int i, j;
	HBRUSH hBrush = (HBRUSH)CreateSolidBrush(RGB(0, 188, 0));
	SelectObject(hdc, hBrush);
	for (i = 0; i<4; i++)
	{
		for (j = 0; j<4; j++)
		{
			if (bNextCurTeris[i][j])
			{
				Rectangle(hdc, rc_right_top.left + BLOCK_SIZE*(j + 1), rc_right_top.top + BLOCK_SIZE*(i + 1), rc_right_top.left + BLOCK_SIZE*(j + 2), rc_right_top.top + BLOCK_SIZE*(i + 2));
			}
		}
	}
	HFONT hFont = CreateFont(30, 0, 0, 0, FW_THIN, 0, 0, 0, UNICODE, 0, 0, 0, 0, L"微软雅黑");//创建一种有特殊性的逻辑字体
	SelectObject(hdc, hFont);//择一对象到指定的设备上下文环境中，该新对象替换先前的相同类型的对象
	SetBkMode(hdc, TRANSPARENT);//设置指定DC的背景混合模式，背景混合模式用于与文本，填充画刷和当画笔不是实线时
	SetBkColor(hdc, RGB(255, 255, 0));//用指定的颜色值来设置当前的背景色
	RECT rect;
	rect.left = rc_right_top.left + 40;
	rect.top = rc_right_top.bottom - 150;
	rect.right = rc_right_top.right;
	rect.bottom = rc_right_top.bottom;
	DrawTextW(hdc, TEXT("下一个"), _tcslen(TEXT("下一个")), &rect, 0);
	DeleteObject(hFont);
	DeleteObject(hBrush);
}

void drawScore(HDC hdc)
{
	HFONT hFont = CreateFont(30, 0, 0, 0, FW_THIN, 0, 0, 0, UNICODE, 0, 0, 0, 0, L"微软雅黑");
	SelectObject(hdc, hFont);
	SetBkMode(hdc, TRANSPARENT);
	SetBkColor(hdc, RGB(255, 255, 0));
	RECT rect;
	rect.left = rc_right_bottom.left;
	rect.top = rc_right_bottom.top;
	rect.right = rc_right_bottom.right;
	rect.bottom = rc_right_bottom.bottom;
	TCHAR szBuf[30];
	LPCTSTR cstr = TEXT("当前难度：%d");
	wsprintf(szBuf, cstr, GAME_DIFF);
	DrawTextW(hdc, szBuf, _tcslen(szBuf), &rect, DT_CENTER | DT_VCENTER);

	RECT rect2;
	rect2.left = rc_right_bottom.left;
	rect2.top = rc_right_bottom.bottom / 2 + 100;
	rect2.right = rc_right_bottom.right;
	rect2.bottom = rc_right_bottom.bottom;
	TCHAR szBuf2[30];
	LPCTSTR cstr2 = TEXT("得分：%d");
	wsprintf(szBuf2, cstr2, GAME_SCORE);
	//outPutBoxInt(sizeof(szBuf));
	DrawTextW(hdc, szBuf2, _tcslen(szBuf2), &rect2, DT_CENTER | DT_VCENTER);

	DeleteObject(hFont);
}

int selectDiffculty(HWND hWnd, int diff)//选择难度
{
	TCHAR szBuf2[30];
	LPCTSTR cstr2 = TEXT("确认选择难度 %d 吗？");
	wsprintf(szBuf2, cstr2, diff);
	if (MessageBox(hWnd, szBuf2, L"难度选择", MB_YESNO) == IDYES)
	{
		GAME_DIFF = diff;
		InvalidateRect(hWnd, &rc_right_bottom, false);//向指定的窗体更新区域添加一个矩形，然后窗口客户区域的这一部分将被重新绘制
		GAME_STATE |= 2;
		init_game();
		return GAME_DIFF;
	}
	return -1;
}

int selectLayOut(HWND hWnd, int layout)
{
	NUM_X = 10 * layout;
	NUM_Y = 20 * layout;
	BLOCK_SIZE = 30 / layout;
	GAME_STATE |= 2;
	InvalidateRect(hWnd, &rc_right_bottom, false);//向指定的窗体更新区域添加一个矩形，然后窗口客户区域的这一部分将被重新绘制
	init_game();
	return layout;
}

void drawCompleteParticle(int line)
{
	HWND hWnd = GetActiveWindow();//该函数可以获得与调用线程的消息队列相关的活动窗口的窗口句柄
	HDC hdc = GetDC(hWnd);//使应用程序更多地控制在客户区域内如何或是否发生剪切
	HBRUSH hBrush = (HBRUSH)GetStockObject(GRAY_BRUSH);
	HPEN hPen = (HPEN)CreatePen(PS_DOT, 1, RGB(255, 255, 0));//指定的样式、宽度和颜色创建画笔
	SelectObject(hdc, hBrush);
	SelectObject(hdc, hPen);
	Rectangle(hdc, BORDER_X,
		BORDER_Y + line*BLOCK_SIZE,
		BORDER_X + NUM_X*BLOCK_SIZE,
		BORDER_Y + BLOCK_SIZE*(1 + line));
	DeleteObject(hBrush);
	DeleteObject(hPen);
}