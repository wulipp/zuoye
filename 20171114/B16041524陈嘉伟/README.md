﻿# 作业要求
* 将分配到的精通Windows.API-函数
接口、编程实例中的代码添加注解

1. 非c语言自带的类型定义

2. 宏定义

3. windows函数（查询msdn.microsoft.com获得准确解释）

* 对于生成的代码运行通过

1. 熟悉单步执行 
2. 断点、条件断点 
3. 熟悉各调试窗口

# 作业结果截图
![输入图片说明](https://gitee.com/JiaWeiOuBaORZ/zuoye/raw/master/20171114/B16041524陈嘉伟/布局选择.png)
![输入图片说明](https://gitee.com/JiaWeiOuBaORZ/zuoye/raw/master/20171114/B16041524陈嘉伟/难度选择.png)
![输入图片说明](https://gitee.com/JiaWeiOuBaORZ/zuoye/raw/master/20171114/B16041524陈嘉伟/布局一.png)
![输入图片说明](https://gitee.com/JiaWeiOuBaORZ/zuoye/raw/master/20171114/B16041524陈嘉伟/布局2.png)
