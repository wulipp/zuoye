# 作业要求
* 将代码在集成环境中运行通过

1. 熟悉单步执行 
2. 断点、条件断点 
3. 熟悉各调试窗口
4. 添加注解

# 作业结果截图
* 起始界面 

* 难度选择

	* 难度1![输入图片说明](https://gitee.com/uploads/images/2017/1219/214425_a2fdd84c_1581486.png "难度1.png")
	* 难度2![输入图片说明](https://gitee.com/uploads/images/2017/1219/214433_f2a95240_1581486.png "难度2.png")
    * 难度3![输入图片说明](https://gitee.com/uploads/images/2017/1219/214443_37f2d1f8_1581486.png "难度3.png")

* 布局选择

	* 布局1![输入图片说明](https://gitee.com/uploads/images/2017/1219/214451_df71d022_1581486.png "布局1.png")
	* 布局2![输入图片说明](https://gitee.com/uploads/images/2017/1219/214501_f7e2ec82_1581486.png "布局2.png")


