// Tetris.cpp : 定义应用程序的入口点。
//

/*
记录于2016-5-29 星期日    由于闪屏过于显眼，采用双缓冲

*/

#include "stdafx.h"
#include "Tetris.h"
#include "windows.h"
#include <mmsystem.h> //多媒体api
#pragma comment(lib, "WINMM.LIB") //windows多媒体程序相关接口
//#progma comment() 表示将一个库链接到工程
#define MAX_LOADSTRING 100 //设置最大缓冲大小为100

// 此代码模块中包含的函数的前向声明:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

// 全局变量:
HINSTANCE hInst;                                                   // 当前实例
TCHAR szTitle[MAX_LOADSTRING];                        // 标题栏文本
TCHAR szWindowClass[MAX_LOADSTRING];         // 主窗口类名

int APIENTRY _tWinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPTSTR    lpCmdLine,
	int       nCmdShow)
{
	init_game(); //初始化游戏
	/*UNREFERENCED_PARAMETER函数告诉编译器，已经使用了该变量，不必检测警告*/
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: 在此放置代码。
	MSG msg;
	HACCEL hAccelTable;

	// 初始化全局字符串
	/*LoadString函数：从资源里加载字符串到CString对象里*/
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);//设置标题名
	LoadString(hInstance, IDC_TETRIS, szWindowClass, MAX_LOADSTRING);//设置类名
	MyRegisterClass(hInstance);//初始化窗口类

	// 执行应用程序初始化:
	if (!InitInstance(hInstance, nCmdShow)) //判断窗口是否被成功建立
	{
		return FALSE;
	}
	/*LoadAccelerators函数：调入加速键表，MAKEINTRESOURCE用于创建标识符*/
	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_TETRIS));

	// 主消息循环:
	while (1)
	{
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))//读取消息
		{
			TranslateMessage(&msg);//翻译消息
			DispatchMessage(&msg);//发送消息
			if (msg.message == WM_QUIT) //若接收到窗口关闭的消息
			{
				break;
			}
		}
		else
		{
			if ((GAME_STATE & 2) != 0) //游戏开始
			{
				tCur = GetTickCount(); //获取开机以来经过的时间
				if (tCur - tPre>g_speed) //超过反应时间
				{

					int flag = CheckValide(curPosX, curPosY + 1, bCurTeris); //检查方块运行到下一行的情况
					if (flag == 1) //如果一切正常
					{
						curPosY++; //方块下移一格
						tPre = tCur; //重置反应时间
						HWND hWnd = GetActiveWindow();
						//获得与调用线程的消息队列相关的活动窗口的窗口句柄
						InvalidateRect(hWnd, &rc_left, FALSE);
						//向指定的窗体更新区域添加一个矩形，然后窗口客户区域的这一部分将被重新绘制
						InvalidateRect(hWnd, &rc_right_top, FALSE);
					}
					else if (flag == -2) //方块到底
					{
						g_speed = t_speed; //速度置零
						fillBlock(); //填充底部被填充的的方块区域
						checkComplite(); //查看一行能否消去
						setRandomT();  //随机生成一个方块用于下次掉落
						curPosX = (NUM_X - 4) >> 1; //设置方块的坐标
						curPosY = 0;
						HWND hWnd = GetActiveWindow(); //获得与调用线程的消息队列相关的活动窗口的窗口句柄
						InvalidateRect(hWnd, &rc_main, FALSE); //刷新矩形区域
					}
					else if (flag == -3) //你输啦菜鸡
					{
						HWND hWnd = GetActiveWindow();
						//弹出消息提示框，提示你已经输了
						if (MessageBox(hWnd, L"胜败乃兵家常事，菜鸡请重新来过", L":时光机", MB_YESNO) == IDYES)
						{
							init_game(); //游戏重新来过
						}
						else
						{
							break;
						}
					}
				}
			}
		}
	}

	return (int)msg.wParam;
}


//
//  函数: MyRegisterClass()
//
//  目的: 注册窗口类。
//
//  注释:
//
//    仅当希望
//    此代码与添加到 Windows 95 中的“RegisterClassEx”
//    函数之前的 Win32 系统兼容时，才需要此函数及其用法。调用此函数十分重要，
//    这样应用程序就可以获得关联的
//    “格式正确的”小图标。
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex; //定义窗口类的对象

	wcex.cbSize = sizeof(WNDCLASSEX); //窗口大小

	wcex.style = CS_HREDRAW | CS_VREDRAW; //窗口类型为默认类型
	wcex.lpfnWndProc = WndProc; //消息过程处理函数为Winproc
	wcex.cbClsExtra = 0; //窗口类无拓展
	wcex.cbWndExtra = 0; //窗口类实例无拓展
	wcex.hInstance = hInstance; //当前实例句柄
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_TETRIS)); //设置窗口的图标
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW); //窗口采用箭头光标
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1); //设置背景颜色（白）
	wcex.lpszMenuName = MAKEINTRESOURCE(IDC_TETRIS); //设置窗口菜单
	wcex.lpszClassName = szWindowClass; //设置窗口类名
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL)); //自定义窗口的小图标

	return RegisterClassEx(&wcex); //返回注册窗口
}

//
//   函数: InitInstance(HINSTANCE, int)
//
//   目的: 保存实例句柄并创建主窗口
//
//   注释:
//
//        在此函数中，我们在全局变量中保存实例句柄并
//        创建和显示主程序窗口。
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND hWnd;

	hInst = hInstance; // 将实例句柄存储在全局变量中

	hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);
	//WS_OVERLAPPEDWINDOW：多种窗口类型的组合
	//CW_USEDEFAULT：窗口的大小位置等为默认值
	if (!hWnd) //判断是否创建成功
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow); //显示窗口
	UpdateWindow(hWnd); //更新窗口，绘制用户区

	return TRUE;
}

//
//  函数: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  目的: 处理主窗口的消息。
//
//  WM_COMMAND  - 处理应用程序菜单
//  WM_PAINT    - 绘制主窗口
//  WM_DESTROY  - 发送退出消息并返回
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	int nWinx, nWiny, nClientX, nClientY;
	int posX, posY;
	RECT rect;
	HMENU hSysmenu;
	switch (message) //处理消息
	{
	case WM_CREATE: //当一个应用程序请求创建窗口时发送此消息


		GetWindowRect(hWnd, &rect);
		//定义窗口大小
		nWinx = 530;
		nWiny = 680;
		//定义窗口位置
		posX = GetSystemMetrics(SM_CXSCREEN);
		posY = GetSystemMetrics(SM_CYSCREEN);
		posX = (posX - nWinx) >> 1;
		posY = (posY - nWiny) >> 1;
		//绘制用户区大小
		GetClientRect(hWnd, &rect);
		nClientX = rect.right - rect.left;
		nClientY = rect.bottom - rect.top;

		MoveWindow(hWnd, posX, posY, 530, 680, TRUE); //改变窗口的位置和大小
		hSysmenu = GetSystemMenu(hWnd, false); //允许应用程序为复制或修改而访问窗口菜单
		AppendMenu(hSysmenu, MF_SEPARATOR, 0, NULL); //在窗口中添加一个菜单
		AppendMenu(hSysmenu, 0, IDM_DIFF, L"难度选择"); //在菜单中新增菜单项
		break;
	case WM_COMMAND: //点击菜单、按钮、下拉列表框等控件时触发
		wmId = LOWORD(wParam); //获取地位字节
		wmEvent = HIWORD(wParam); //获取高位字节
		// 分析菜单选择:
		switch (wmId)
		{
		case IDM_ABOUT: //关于菜单项
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT: //退出菜单项
			DestroyWindow(hWnd);
			break;
		case ID_dif1: //选择难度一
			selectDiffculty(hWnd, 1);
			break;
		case ID_dif2: //选择难度二
			selectDiffculty(hWnd, 2);
			break;
		case ID_dif3: //选择难度三
			selectDiffculty(hWnd, 3);
			break;
		case ID_LAYOUT1: //选择大视图
			selectLayOut(hWnd, 1);
			break;
		case ID_LAYOUT2: //选择小视图
			selectLayOut(hWnd, 2);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam); //缺省处理
		}
		break;
	case WM_KEYDOWN: //键盘消息触发
		hdc = GetDC(hWnd);
		InvalidateRect(hWnd, NULL, false);
		switch (wParam)
		{
		case VK_LEFT: //按方向键←的时候，方块左移
			curPosX--;
			if (CheckValide(curPosX, curPosY, bCurTeris) != 1) //如果移出左边框外，则方块右移修正
			{
				curPosX++;
			}
			break;
		case VK_RIGHT: //按方向键→的时候，方块右移
			curPosX++;
			if (CheckValide(curPosX, curPosY, bCurTeris) != 1) //如果移出右边框外，则方块右左移修正
			{
				curPosX--;
			}
			break;
		case VK_UP: //按方向键↑的时候，方块旋转
			RotateTeris(bCurTeris); //方块旋转
			break;
		case VK_DOWN: //按方向键↓的时候，方块加速
			if (g_speed == t_speed)
				g_speed = 10; 
			else //再按一次则恢复原来的速度
				g_speed = t_speed;
			//outPutBoxInt(g_speed);
			break;
			/*WASD实现与方向键相同的效果*/
		case 'W':
			RotateTeris(bCurTeris);
			break;
		case 'A':
			curPosX--;
			if (CheckValide(curPosX, curPosY, bCurTeris) != 1)
			{
				curPosX++;
			}
			break;
		case 'D':
			curPosX++;
			if (CheckValide(curPosX, curPosY, bCurTeris) != 1)
			{
				curPosX--;
			}
			break;

		case 'S':
			if (g_speed == t_speed)
				g_speed = 10;
			else
				g_speed = t_speed;
			//outPutBoxInt(g_speed);
			break;
		default:
			break;
		}

	case WM_PAINT: //绘制用户区
		hdc = BeginPaint(hWnd, &ps);
		// TODO: 在此添加任意绘图代码...
		DrawBackGround(hdc); //绘制背景
		EndPaint(hWnd, &ps); //停止绘制
		break;
	case WM_DESTROY: //关闭窗口
		PostQuitMessage(0); //退出
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam); //缺省处理
	}
	return 0;
}

// “关于”框的消息处理程序。
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam); //避免编译器警告
	switch (message)
	{
	case WM_INITDIALOG: //对话框消息
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam)); //清除一个模态对话框, 并使系统中止对对话框的任何处理的函数
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

//绘制当前已经存在砖块的区域
void drawBlocked(HDC mdc)
{
	int i, j;

	HBRUSH hBrush = (HBRUSH)CreateSolidBrush(RGB(255, 255, 0)); //新建黄色的画刷

	SelectObject(mdc, hBrush); //赋值新的画刷属性

	for (i = 0; i<NUM_Y; i++)
	{
		for (j = 0; j<NUM_X; j++)
		{
			if (g_hasBlocked[i][j])
			{
				Rectangle(mdc, BORDER_X + j*BLOCK_SIZE, BORDER_Y + i*BLOCK_SIZE,
					BORDER_X + (j + 1)*BLOCK_SIZE, BORDER_Y + (i + 1)*BLOCK_SIZE
				);//用黄色的画刷对底部落下的方块进行填充
			}
		}
	}
	DeleteObject(hBrush); //释放画笔资源对象
}
//检查有效性
int CheckValide(int startX, int startY, BOOL bTemp[4][4])
{
	int i, j;
	for (i = 3; i >= 0; i--)
	{
		for (j = 3; j >= 0; j--)
		{
			if (bTemp[i][j])
			{
				if (j + startX<0 || j + startX >= NUM_X) //如果方块移动到了左右边框的外面
				{
					return -1;
				}
				if (i + startY >= NUM_Y) //如果方块移动到了底部
				{
					return -2;
				}
				if (g_hasBlocked[i + startY][j + startX]) //方块有堆积
				{
					//outPutBoxInt(j+startY);
					if (curPosY == 0) //方块触及边框上部
					{
						return -3;
					}
					return -2;
				}
			}
		}
	}
	//MessageBox(NULL,L"这里",L"as",MB_OK);
	//outPutBoxInt(curPosY);
	//一切正常
	return 1;
}
//查看一行是否能消去  采用从上往下的消法，消去一行后把上面的每行都往下移
void checkComplite()
{
	int i, j, k, count = 0;
	for (i = 0; i<NUM_Y; i++)
	{
		bool flag = true;
		for (j = 0; j<NUM_X; j++)
		{
			if (!g_hasBlocked[i][j]) //没有完全填满一行
			{
				flag = false;
			}
		}
		if (flag)
		{
			count++;
			for (j = i; j >= 1; j--)
			{
				for (k = 0; k<NUM_X; k++)
				{
					g_hasBlocked[j][k] = g_hasBlocked[j - 1][k]; //上面的方块下移填充下面消去的方块
				}

			}
			drawCompleteParticle(i);
			Sleep(300); //休息一下

			PlaySound(_T("coin.wav"), NULL, SND_FILENAME | SND_ASYNC); //发出提示音
		}
	}
	GAME_SCORE += int(count*1.5);
}


//自定义的弹窗函数  用于调试
VOID outPutBoxInt(int num)
{
	TCHAR szBuf[1024];
	LPCTSTR str = TEXT("%d");
	wsprintf(szBuf, str, num); // 将一系列的字符和数值输入到缓冲区。输出缓冲区里的的值取决于格式说明符（即"%"）
	MessageBox(NULL, szBuf, L"aasa", MB_OK); //弹出无意义消息框
}

VOID outPutBoxString(TCHAR str[1024])
{
	TCHAR szBuf[1024];
	LPCTSTR cstr = TEXT("%s");
	wsprintf(szBuf, cstr, str); // 将一系列的字符和数值输入到缓冲区。输出缓冲区里的的值取决于格式说明符（即"%"）
	MessageBox(NULL, szBuf, L"aasa", MB_OK); //弹出无意义消息框
}




void setRandomT()
{
	int rd_start = RandomInt(0, sizeof(state_teris) / sizeof(state_teris[0])); //随机选择一种方块形状
	int rd_next = RandomInt(0, sizeof(state_teris) / sizeof(state_teris[0]));
	//outPutBoxInt(rd_start);
	//outPutBoxInt(rd_next);
	//outPutBoxInt(rd_start);
	if (GAME_STATE == 0) //如果游戏刚开始
	{
		GAME_STATE = GAME_STATE | 0x0001; //赋一
		//outPutBoxInt(GAME_STATE);
		/*void *memcpy(void *dest, const void *src, size_t n)从源src所指的内存地址的起始位置开始拷贝n个字节到目标dest所指的内存地址的起始位置中*/
		memcpy(bCurTeris, state_teris[rd_start], sizeof(state_teris[rd_start])); //随机生成第一个方块
		memcpy(bNextCurTeris, state_teris[rd_next], sizeof(state_teris[rd_next])); //生成下一次掉落的方块
	}
	else //如果游戏已经开始
	{
		memcpy(bCurTeris, bNextCurTeris, sizeof(bNextCurTeris)); //把下一次掉落的方块形状赋给当前方块
		memcpy(bNextCurTeris, state_teris[rd_next], sizeof(state_teris[rd_next])); //随机生成下一次掉落的方块形状
	}

}
void init_game()
{
	GAME_SCORE = 0; //得分归零
	setRandomT(); //随机生成一个方块用作下一次掉落
	curPosX = (NUM_X - 4) >> 1;//默认光标的位置
	curPosY = 0;

	/*
	   void *memset(void *s, int ch, size_t n):
	   解释：将s中当前位置后面的n个字节 （typedef unsigned int size_t ）用 ch 替换并返回 s 。
	*/
	memset(g_hasBlocked, 0, sizeof(g_hasBlocked)); //将堆积在底部的黄色方块清除
	/*设置各个界面的位置（居中）*/
	rc_left.left = 0;
	rc_left.right = SCREEN_LEFT_X;
	rc_left.top = 0;
	rc_left.bottom = SCREEN_Y;

	rc_right.left = rc_left.right + BORDER_X;
	rc_right.right = 180 + rc_right.left;
	rc_right.top = 0;
	rc_right.bottom = SCREEN_Y;

	rc_main.left = 0;
	rc_main.right = SCREEN_X;
	rc_main.top = 0;
	rc_main.bottom = SCREEN_Y;

	rc_right_top.left = rc_right.left;
	rc_right_top.top = rc_right.top;
	rc_right_top.right = rc_right.right;
	rc_right_top.bottom = (rc_right.bottom) / 2;

	rc_right_bottom.left = rc_right.left;
	rc_right_bottom.top = rc_right_top.bottom + BORDER_Y;
	rc_right_bottom.right = rc_right.right;
	rc_right_bottom.bottom = rc_right.bottom;

	/*初始化方块落下的速度*/
	g_speed = t_speed = 1000 - GAME_DIFF * 280;
}

void fillBlock()
{
	int i, j;
	for (i = 0; i<4; i++)
	{
		for (j = 0; j<4; j++)
		{
			if (bCurTeris[i][j])
			{
				g_hasBlocked[curPosY + i][curPosX + j] = 1; //填充底部方块区域
			}
		}
	}
}

void RotateTeris(BOOL bTeris[4][4])
{
	BOOL bNewTeris[4][4];
	int x, y;
	for (x = 0; x<4; x++)
	{
		for (y = 0; y<4; y++)
		{
			bNewTeris[x][y] = bTeris[3 - y][x];
			//逆时针：
			//bNewTeris[x][y] = bTeris[y][3-x];
		}
	}
	if (CheckValide(curPosX, curPosY, bNewTeris) == 1)
	{
		memcpy(bTeris, bNewTeris, sizeof(bNewTeris)); //用新的形状取代原来的形状
	}

}
//获取一个随机int
int RandomInt(int _min, int _max)
{
	srand((rd_seed++) % 65532 + GetTickCount());
	return _min + rand() % (_max - _min);
}
//绘制正在下落的方块
VOID DrawTeris(HDC mdc)
{

	int i, j;
	HPEN hPen = (HPEN)GetStockObject(BLACK_PEN); //黑色画笔
	HBRUSH hBrush = (HBRUSH)GetStockObject(WHITE_BRUSH); //白色画刷
	SelectObject(mdc, hPen); //选择一对象到指定的设备上下文环境中，该新对象替换先前的相同类型的对象
	SelectObject(mdc, hBrush); 
	for (i = 0; i<4; i++)
	{
		for (j = 0; j<4; j++)
		{
			if (bCurTeris[i][j])
			{
				Rectangle(mdc, (j + curPosX)*BLOCK_SIZE + BORDER_X, (i + curPosY)*BLOCK_SIZE + BORDER_Y, (j + 1 + curPosX)*BLOCK_SIZE + BORDER_X, (i + 1 + curPosY)*BLOCK_SIZE + BORDER_Y);
				//给方块涂黑边，涂白色			
			}
		}
	}
	drawBlocked(mdc); //画底部方块
	DeleteObject(hPen);
	DeleteObject(hBrush);
}
//绘制背景
VOID DrawBackGround(HDC hdc)
{

	HBRUSH hBrush = (HBRUSH)GetStockObject(GRAY_BRUSH); //灰色画刷
	HDC mdc = CreateCompatibleDC(hdc); //设备环境句柄
	HBITMAP hBitmap = CreateCompatibleBitmap(hdc, SCREEN_X, SCREEN_Y); //位图句柄

	SelectObject(mdc, hBrush); //在设备环境中替换先前对象
	SelectObject(mdc, hBitmap);

	HBRUSH hBrush2 = (HBRUSH)GetStockObject(WHITE_BRUSH); //白色画刷
	//用指定的画刷填充方块和用户区
	FillRect(mdc, &rc_main, hBrush2); 
	Rectangle(mdc, rc_left.left + BORDER_X, rc_left.top + BORDER_Y, rc_left.right, rc_left.bottom);
	Rectangle(mdc, rc_right.left + BORDER_X, rc_right.top + BORDER_Y, rc_right.right, rc_right.bottom);
	DrawTeris(mdc);
	drawNext(mdc);
	drawScore(mdc);
	::BitBlt(hdc, 0, 0, SCREEN_X, SCREEN_Y, mdc, 0, 0, SRCCOPY);
	DeleteObject(hBrush);
	DeleteDC(mdc);
	DeleteObject(hBitmap);
	DeleteObject(hBrush2);


	//  int x,y;
	//  HPEN hPen = (HPEN)GetStockObject(NULL_PEN);
	//  HBRUSH hBrush = (HBRUSH)GetStockObject(GRAY_BRUSH);
	//  SelectObject(hdc,hPen);
	//  SelectObject(hdc,hBrush);
	//  for (x = 0;x<NUM_X;x++)
	//  {
	//      for(y=0;y<NUM_Y;y++)
	//      {
	//          Rectangle(hdc,BORDER_X+x*BLOCK_SIZE,BORDER_Y+y*BLOCK_SIZE,
	//              BORDER_X+(x+1)*BLOCK_SIZE,
	//              BORDER_Y+(y+1)*BLOCK_SIZE);
	//      }
	//  }
}
//绘制下一个将要掉落的方块
void drawNext(HDC hdc)
{
	int i, j;
	HBRUSH hBrush = (HBRUSH)CreateSolidBrush(RGB(0, 188, 0)); //绿色画刷
	SelectObject(hdc, hBrush);
	for (i = 0; i<4; i++)
	{
		for (j = 0; j<4; j++)
		{
			if (bNextCurTeris[i][j])
			{
				Rectangle(hdc, rc_right_top.left + BLOCK_SIZE*(j + 1), rc_right_top.top + BLOCK_SIZE*(i + 1), rc_right_top.left + BLOCK_SIZE*(j + 2), rc_right_top.top + BLOCK_SIZE*(i + 2));
				//用绿色画下一个方块
			}
		}
	}
	HFONT hFont = CreateFont(30, 0, 0, 0, FW_THIN, 0, 0, 0, UNICODE, 0, 0, 0, 0, L"微软雅黑"); //字体
	SelectObject(hdc, hFont);
	SetBkMode(hdc, TRANSPARENT); //设置指定DC的背景混合模式
	SetBkColor(hdc, RGB(255, 255, 0)); //设置当前的背景色
	RECT rect;
	//设置矩形大小
	rect.left = rc_right_top.left + 40;
	rect.top = rc_right_top.bottom - 150;
	rect.right = rc_right_top.right;
	rect.bottom = rc_right_top.bottom;
	DrawTextW(hdc, TEXT("下一个"), _tcslen(TEXT("下一个")), &rect, 0);
	DeleteObject(hFont);
	DeleteObject(hBrush);
}
//绘制分数
void drawScore(HDC hdc)
{
	HFONT hFont = CreateFont(30, 0, 0, 0, FW_THIN, 0, 0, 0, UNICODE, 0, 0, 0, 0, L"微软雅黑");
	SelectObject(hdc, hFont);
	SetBkMode(hdc, TRANSPARENT);
	SetBkColor(hdc, RGB(255, 255, 0));
	RECT rect;
	rect.left = rc_right_bottom.left;
	rect.top = rc_right_bottom.top;
	rect.right = rc_right_bottom.right;
	rect.bottom = rc_right_bottom.bottom;
	TCHAR szBuf[30];
	LPCTSTR cstr = TEXT("当前难度：%d");
	wsprintf(szBuf, cstr, GAME_DIFF);
	DrawTextW(hdc, szBuf, _tcslen(szBuf), &rect, DT_CENTER | DT_VCENTER);

	RECT rect2;
	rect2.left = rc_right_bottom.left;
	rect2.top = rc_right_bottom.bottom / 2 + 100;
	rect2.right = rc_right_bottom.right;
	rect2.bottom = rc_right_bottom.bottom;
	TCHAR szBuf2[30];
	LPCTSTR cstr2 = TEXT("得分：%d");
	wsprintf(szBuf2, cstr2, GAME_SCORE);
	//outPutBoxInt(sizeof(szBuf));
	DrawTextW(hdc, szBuf2, _tcslen(szBuf2), &rect2, DT_CENTER | DT_VCENTER);

	DeleteObject(hFont);
}
//选择难度
int selectDiffculty(HWND hWnd, int diff)
{
	TCHAR szBuf2[30];
	LPCTSTR cstr2 = TEXT("确认选择难度 %d 吗？");
	wsprintf(szBuf2, cstr2, diff);
	if (MessageBox(hWnd, szBuf2, L"难度选择", MB_YESNO) == IDYES)
	{
		GAME_DIFF = diff;
		InvalidateRect(hWnd, &rc_right_bottom, false);
		GAME_STATE |= 2;
		init_game();
		return GAME_DIFF;
	}
	return -1;
}
//选择布局
int selectLayOut(HWND hWnd, int layout)
{
	NUM_X = 10 * layout;
	NUM_Y = 20 * layout;
	BLOCK_SIZE = 30 / layout;
	GAME_STATE |= 2;
	InvalidateRect(hWnd, &rc_right_bottom, false);
	init_game();
	return layout;
}
//绘制当前已经存在砖块的区域
void drawCompleteParticle(int line)
{
	HWND hWnd = GetActiveWindow(); //获取当前窗口句柄
	HDC hdc = GetDC(hWnd);
	HBRUSH hBrush = (HBRUSH)GetStockObject(GRAY_BRUSH); //灰色画刷
	HPEN hPen = (HPEN)CreatePen(PS_DOT, 1, RGB(255, 255, 0)); //黄色画笔
	SelectObject(hdc, hBrush); //用灰色画刷填充游戏区域
	SelectObject(hdc, hPen); //用黄色画笔画底部方块
	//重新确定游戏区的区域大小
	Rectangle(hdc, BORDER_X,
		BORDER_Y + line*BLOCK_SIZE,
		BORDER_X + NUM_X*BLOCK_SIZE,
		BORDER_Y + BLOCK_SIZE*(1 + line));
	DeleteObject(hBrush); //删除
	DeleteObject(hPen); //删除
}