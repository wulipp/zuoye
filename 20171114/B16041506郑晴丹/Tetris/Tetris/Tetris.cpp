// Tetris.cpp : 定义应用程序的入口点。
//

/*
记录于2016-5-29 星期日    由于闪屏过于显眼，采用双缓冲

*/

#include "stdafx.h"
#include "Tetris.h"
#include "windows.h"
#include <mmsystem.h>/*包含windows中与多媒体有关的大多数接口*/
#pragma comment(lib, "WINMM.LIB")/*导入winmm库：WINMM.LIB是Windows多媒体相关应用程序接口*/
#define MAX_LOADSTRING 100 /*定义字符最大长度*/

// 此代码模块中包含的函数的前向声明:
ATOM                MyRegisterClass(HINSTANCE hInstance);/*窗口注册函数*/
BOOL                InitInstance(HINSTANCE, int);/*初始化应用程序*/
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);/*窗口过程处理函数*/
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

// 全局变量:
HINSTANCE hInst;                                // 当前实例
TCHAR szTitle[MAX_LOADSTRING];                  // 标题栏文本
TCHAR szWindowClass[MAX_LOADSTRING];            // 主窗口类名
HMENU Diff;//难度菜单句柄
HMENU LayOut;//布局菜单句柄

			 //主函数，程序入口
int APIENTRY _tWinMain(HINSTANCE hInstance,/*当前实例句柄*/
	HINSTANCE hPrevInstance,/*其他实例句柄*/
	LPTSTR    lpCmdLine,/*指向程序命令行参数的指针*/
	int       nCmdShow/*应用程序开始执行时窗口显示方式的整数值标识*/)
{
	init_game();/*初始化游戏*/
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);/*避免编译器出现关于未引用参数的警告*/

									  // TODO: 在此放置代码。
	MSG msg;/*定义消息*/
	HACCEL hAccelTable;/*处理加速键表*/

					   // 初始化全局字符串
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_TETRIS, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// 执行应用程序初始化:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_TETRIS));

	// 主消息循环:
	while (1)
	{
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))/*检查线程消息队列*/
		{
			TranslateMessage(&msg);/*将消息的虚拟键转化字符消息*/
			DispatchMessage(&msg); /*将消息传送到指定窗口函数*/
			if (msg.message == WM_QUIT)/*若消息是终止则退出*/
			{
				break;
			}
		}
		else
		{
			if ((GAME_STATE & 2) != 0)
			{
				tCur = GetTickCount(); /*返回从操作系统启动所经过的毫秒数*/
				if (tCur - tPre>g_speed)/*若超时*/
				{

					int flag = CheckValide(curPosX, curPosY + 1, bCurTeris); /*判断下一行的情况，返回给flag*/
					if (flag == 1)/*正常下降一行*/
					{
						curPosY++;/*方块坐标y加1*/
						tPre = tCur;/*重新计算反应时间*/
						HWND hWnd = GetActiveWindow();/*这里两次调用实现了双缓冲的作用，改善了视觉效果*/
						InvalidateRect(hWnd, &rc_left, FALSE);/*向指定窗体的更新区域添加一个矩形，然后窗口客户区的部分将被重新绘制*/
						InvalidateRect(hWnd, &rc_right_top, FALSE);/*系统重绘时不会向窗口发送WM_PAINT消息*/
					}
					else if (flag == -2)/*方块到底时*/
					{
						g_speed = t_speed;/*重置速度*/
						fillBlock();/*将方块填充给矩形*/
						checkComplite();/*检查某行是否填满*/
						setRandomT();/*随机产生新方块*/
						curPosX = (NUM_X - 4) >> 1;/*重新设置新方块的坐标x*/
						curPosY = 0;/*重新设置新方块的坐标y*/
						HWND hWnd = GetActiveWindow(); /*检索获得附加到调用线程的消息队列的活动窗口的窗口句柄*/
						InvalidateRect(hWnd, &rc_main, FALSE);/*重绘矩形区域*/
					}
					else if (flag == -3)/*方块放不下游戏失败时*/
					{
						HWND hWnd = GetActiveWindow();/*检索获得附加到调用线程的消息队列的活动窗口的窗口句柄*/
						if (MessageBox(hWnd, L"胜败乃兵家常事，菜鸡请重新来过", L"这就是命", MB_YESNO) == IDYES)/*弹出消息提示游戏失败，选择yes重新开始*/
						{
							init_game();/*重新开始游戏*/
						}
						else
						{
							break;/*退出循环，游戏结束*/
						}
					}
				}
			}
		}
	}

	return (int)msg.wParam;
}


//
//  函数: MyRegisterClass()
//
//  目的: 注册窗口类。
//
//  注释:
//
//    仅当希望
//    此代码与添加到 Windows 95 中的“RegisterClassEx”
//    函数之前的 Win32 系统兼容时，才需要此函数及其用法。调用此函数十分重要，
//    这样应用程序就可以获得关联的
//    “格式正确的”小图标。
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;/*定义窗口类对象*/

	wcex.cbSize = sizeof(WNDCLASSEX);/*窗口大小*/

	wcex.style = CS_HREDRAW | CS_VREDRAW;/*窗口风格*/
										 /*CS_HREDRAW:当水平长度改变或移动窗口时，重绘整个窗口*/
										 /*CS_VREDEAW:当垂直长度改变或移动窗口时，重绘整个窗口*/
	wcex.lpfnWndProc = WndProc;/*窗口过程处理*/
	wcex.cbClsExtra = 0; /*窗口类无扩展*/
	wcex.cbWndExtra = 0; /*窗口实例无扩展*/
	wcex.hInstance = hInstance;/*当前实例句柄*/
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_TETRIS)); /*窗口的图标为默认图标*/
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);/*设置光标样式*/
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);/*设置背景色*/
	wcex.lpszMenuName = MAKEINTRESOURCE(IDC_TETRIS); /*使用系统自定义的菜单资源*/
	wcex.lpszClassName = szWindowClass;/*设置窗口类名*/
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));/*小图标为默认图标*/

	return RegisterClassEx(&wcex);/*返回注册窗口*/
}

//
//   函数: InitInstance(HINSTANCE, int)
//
//   目的: 保存实例句柄并创建主窗口
//
//   注释:
//
//        在此函数中，我们在全局变量中保存实例句柄并
//        创建和显示主程序窗口。
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND hWnd;

	hInst = hInstance; // 将实例句柄存储在全局变量中

	hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);
	/*WS_OVERLAPPEDWINDOW:创建一带边框、标题栏、系统菜单及最大最小化按钮的窗口*/
	/*CW_USEDEFAULT:系统选择的默认位置窗口的左上角, 而忽略了y参数*/
	/*NULL:空指针类型的文字*/

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);/*显示窗口*/
	UpdateWindow(hWnd);/*刷新窗口，重绘客户区域*/

	return TRUE;
}

//
//  函数: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  目的: 处理主窗口的消息。
//
//  WM_COMMAND  - 处理应用程序菜单
//  WM_PAINT    - 绘制主窗口
//  WM_DESTROY  - 发送退出消息并返回
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps; /*包含应用程序用来绘制他拥有的窗口的客户区域的信息*/
	HDC hdc; /*标识设备环境句柄*/
	int nWinx, nWiny, nClientX, nClientY;
	int posX, posY;
	RECT rect; /*rect用来存储成对出现的参数，如矩形的坐标、高宽*/
	HMENU hSysmenu;/*菜单句柄*/
	switch (message)
	{
	case WM_CREATE:/*当一个应用程序通过CreateWindowEx函数或者CreateWindow函数请求创建窗口时发送此消息*/

		GetWindowRect(hWnd, &rect);/*窗口的位置*/
		nWinx = 530;/*窗口的宽度*/
		nWiny = 680;/*窗口的高度*/
		posX = GetSystemMetrics(SM_CXSCREEN);/*获取屏幕的宽度*/
		posY = GetSystemMetrics(SM_CYSCREEN);/*获取屏幕的高度*/
		posX = (posX - nWinx) >> 1;/*求出居中的x位置*/
		posY = (posY - nWiny) >> 1;/*求出居中的y位置*/
		GetClientRect(hWnd, &rect);/*获取客户区域的大小*/
		nClientX = rect.right - rect.left;/*用户区域的宽度*/
		nClientY = rect.bottom - rect.top;/*用户区域的高度*/

		MoveWindow(hWnd, posX, posY, 530, 680, TRUE); /*改变指定窗口的位置和尺寸*/
		hSysmenu = GetMenu(hWnd);/*获取菜单句柄*/
		AppendMenu(hSysmenu, MF_SEPARATOR, 0, NULL); /*添加一条水平分割线*/
													 //此处的菜单句柄要全局向前初始化

		Diff = CreatePopupMenu();/*创建难度菜单句柄*/
		AppendMenu(hSysmenu, MF_POPUP, (UINT_PTR)Diff, L"难度选择");/*添加菜单Diff*/
		AppendMenu(Diff, MF_STRING, ID_dif1, L"难度1");/*在新添加的菜单下创建菜单难度1*/
		AppendMenu(Diff, MF_STRING, ID_dif2, L"难度2");/*在新添加的菜单下创建菜单难度2*/
		AppendMenu(Diff, MF_STRING, ID_dif3, L"难度3");/*在新添加的菜单下创建菜单难度3*/

		LayOut = CreatePopupMenu();/*创建布局菜单句柄*/
		AppendMenu(hSysmenu, MF_POPUP, (UINT_PTR)LayOut, L"布局选择");/*添加菜单LayOut*/
		AppendMenu(LayOut, MF_STRING, ID_LAYOUT1, L"布局1");/*在新添加的LayOut菜单下创建菜单布局1*/
		AppendMenu(LayOut, MF_STRING, ID_LAYOUT2, L"布局2");/*在新添加的LayOut菜单下创建菜单布局2*/
		SetMenu(hWnd, hSysmenu);/*设置系统自定义菜单*/
		SetMenu(hWnd, Diff);/*设置新添的难度菜单*/
		SetMenu(hWnd, LayOut);/*设置新添的布局菜单*/
		break;
	case WM_COMMAND:
		wmId = LOWORD(wParam);/*从指定的 32 位值，检索低顺序单词*/
		wmEvent = HIWORD(wParam); /*获取高位字节*/
								  // 分析菜单选择:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);/*弹出“关于”消息框*/
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);/*退出程序*/
			break;
		case ID_dif1:
			selectDiffculty(hWnd, 1);/*选择难度1*/
			break;
		case ID_dif2:
			selectDiffculty(hWnd, 2);/*选择难度2*/
			break;
		case ID_dif3:
			selectDiffculty(hWnd, 3);/*选择难度3*/
			break;
		case ID_LAYOUT1:
			selectLayOut(hWnd, 1);/*选择布局1*/
			break;
		case ID_LAYOUT2:
			selectLayOut(hWnd, 2);/*选择布局2*/
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);/*默认处理过程*/
		}
		break;
	case WM_KEYDOWN:/*键盘消息*/
		hdc = GetDC(hWnd);/*获取当前设备上下文句柄*/
		InvalidateRect(hWnd, NULL, false);/*向窗体更新区域添加一个矩形以减少重绘面积*/
		switch (wParam)
		{
		case 'A':
		case VK_LEFT:/*当按下方向左键时*/
			curPosX--;/*方块x坐标减一，左移*/
			if (CheckValide(curPosX, curPosY, bCurTeris) != 1)/*检查位置是否超出范围，若超出*/
			{
				curPosX++;/*位置复原*/
			}
			break;
		case 'D':
		case VK_RIGHT:/*当按下方向右键时*/
			curPosX++;/*方块x坐标加一，右移*/
			if (CheckValide(curPosX, curPosY, bCurTeris) != 1)/*检查位置是否超出范围，若超出*/
			{
				curPosX--;/*位置复原*/
			}
			break;
		case 'W':
		case VK_UP:/*当按下方向上键时*/
			RotateTeris(bCurTeris);/*变换形状*/
			break;
		case 'S':
		case VK_DOWN:/*当按下方向下键时*/
			if (g_speed == t_speed)
				g_speed = 10;
			else
				g_speed = t_speed;
			//outPutBoxInt(g_speed);
			break;
			/*wasd键和上下左右键一样，所以把他移在一起了*/
			/*case 'W':
			RotateTeris(bCurTeris);
			break;
			case 'A':
			curPosX--;
			if (CheckValide(curPosX, curPosY, bCurTeris) != 1)
			{
			curPosX++;
			}
			break;
			case 'D':
			curPosX++;
			if (CheckValide(curPosX, curPosY, bCurTeris) != 1)
			{
			curPosX--;
			}
			break;

			case 'S':
			if (g_speed == t_speed)
			g_speed = 10;
			else
			g_speed = t_speed;
			//outPutBoxInt(g_speed);
			break;*/
		default:
			break;
		}

	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: 在此添加任意绘图代码...
		DrawBackGround(hdc);/*绘制背景*/
		EndPaint(hWnd, &ps);/*停止绘制*/
		break;
	case WM_DESTROY:/*退出程序*/
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// “关于”框的消息处理程序。
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:/*初始化控件*/
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));/*清除一个模态对话框,并使系统中止对对话框的任何处理*/
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}


void drawBlocked(HDC mdc)/*绘制当前已经存在方块的区域*/
{
	int i, j;

	HBRUSH hBrush = (HBRUSH)CreateSolidBrush(RGB(255, 255, 0));/*创建画刷句柄，黄色*/

	SelectObject(mdc, hBrush);/*把该刷子选入设备环境，新对象替换先前的相同类型的对象*/

	for (i = 0; i<NUM_Y; i++)
	{
		for (j = 0; j<NUM_X; j++)
		{
			if (g_hasBlocked[i][j])/*若数组中存在值，则说明有方块*/
			{
				Rectangle(mdc, BORDER_X + j*BLOCK_SIZE, BORDER_Y + i*BLOCK_SIZE,
					BORDER_X + (j + 1)*BLOCK_SIZE, BORDER_Y + (i + 1)*BLOCK_SIZE);
				/*绘制矩形*/
			}
		}
	}
	DeleteObject(hBrush);/*删除对象*/
}

int CheckValide(int startX, int startY, BOOL bTemp[4][4])/*检查位置是否合法*/
{
	int i, j;
	for (i = 3; i >= 0; i--)
	{
		for (j = 3; j >= 0; j--)
		{
			if (bTemp[i][j])/*落下的4*4位置中若存在方块*/
			{
				if (j + startX<0 || j + startX >= NUM_X)/*超过左右界*/
				{
					return -1;
				}
				if (i + startY >= NUM_Y)/*下一行无法下降*/
				{
					return -2;
				}
				if (g_hasBlocked[i + startY][j + startX])
				{
					//outPutBoxInt(j+startY);
					if (curPosY == 0)/*无下一行*/
					{
						return -3;
					}
					return -2;
				}
			}
		}
	}
	//MessageBox(NULL,L"这里",L"as",MB_OK);
	//outPutBoxInt(curPosY);
	return 1;
}

void checkComplite()/*查看一行是否能消去，消去一行后把上面的每行都下移*/
{
	int i, j, k, count = 0;
	for (i = 0; i<NUM_Y; i++)
	{
		bool flag = true;
		for (j = 0; j<NUM_X; j++)
		{
			if (!g_hasBlocked[i][j])/*i行j列缺少方块时，无法消去*/
			{
				flag = false;
			}
		}
		if (flag)/*可消除*/
		{
			count++;/*消去的行数加一*/
			for (j = i; j >= 1; j--)
			{
				for (k = 0; k<NUM_X; k++)
				{
					g_hasBlocked[j][k] = g_hasBlocked[j - 1][k];/*令删除行之上的各行下移*/
				}

			}
			drawCompleteParticle(i);/*在消去的那行画一行空白做出消去的效果*/
			Sleep(300);/*暂停300ms*/

			PlaySound(_T("coin.wav"), NULL, SND_FILENAME | SND_ASYNC);/*消去一行时播放系统自带提示音*/
		}
	}
	GAME_SCORE += int(count*1.5);/*计算得分，一行+1分，两行+3分，四行+6分，以此类推*/
}



VOID outPutBoxInt(int num)/*自定义的弹窗函数，用于调试，已经被注释掉了*/
{
	TCHAR szBuf[1024];
	LPCTSTR str = TEXT("%d");
	wsprintf(szBuf, str, num);
	MessageBox(NULL, szBuf, L"aasa", MB_OK);
}

VOID outPutBoxString(TCHAR str[1024])/*自定义的弹窗函数，用于调试，已经被注释掉了*/
{
	TCHAR szBuf[1024];
	LPCTSTR cstr = TEXT("%s");
	wsprintf(szBuf, cstr, str);
	MessageBox(NULL, szBuf, L"aasa", MB_OK);
}




void setRandomT()/*设置随机方块形状*/
{
	int rd_start = RandomInt(0, sizeof(state_teris) / sizeof(state_teris[0]));/*初始下落方块，[0,7)上随机数*/
	int rd_next = RandomInt(0, sizeof(state_teris) / sizeof(state_teris[0]));/*下一个下落方块*/
																			 //outPutBoxInt(rd_start);
																			 //outPutBoxInt(rd_next);
																			 //outPutBoxInt(rd_start);
	if (GAME_STATE == 0)/*游戏刚开始*/
	{
		GAME_STATE = GAME_STATE | 0x0001;
		//outPutBoxInt(GAME_STATE);
		memcpy(bCurTeris, state_teris[rd_start], sizeof(state_teris[rd_start]));/*令当前下落方块等于初始下落方块*/
		memcpy(bNextCurTeris, state_teris[rd_next], sizeof(state_teris[rd_next]));/*记录下一个下落方块*/
	}
	else/*游戏中已经有下落方块*/
	{
		memcpy(bCurTeris, bNextCurTeris, sizeof(bNextCurTeris));/*令当前下落方块等于下一个下落方块*/
		memcpy(bNextCurTeris, state_teris[rd_next], sizeof(state_teris[rd_next]));/*记录下一个下落方块*/
	}

}
void init_game()/*初始化游戏*/
{
	GAME_SCORE = 0;/*记录得分*/
	setRandomT();/*随机生成方块形状*/
	curPosX = (NUM_X - 4) >> 1;/*从中间下落*/
	curPosY = 0;/*从最上方下落*/


	memset(g_hasBlocked, 0, sizeof(g_hasBlocked));/*g_hasBlocked元素全部清零*/
	rc_left.left = 0;
	rc_left.right = SCREEN_LEFT_X;
	rc_left.top = 0;
	rc_left.bottom = SCREEN_Y;

	rc_right.left = rc_left.right + BORDER_X;
	rc_right.right = 180 + rc_right.left;
	rc_right.top = 0;
	rc_right.bottom = SCREEN_Y;

	rc_main.left = 0;
	rc_main.right = SCREEN_X;
	rc_main.top = 0;
	rc_main.bottom = SCREEN_Y;

	rc_right_top.left = rc_right.left;
	rc_right_top.top = rc_right.top;
	rc_right_top.right = rc_right.right;
	rc_right_top.bottom = (rc_right.bottom) / 2;

	rc_right_bottom.left = rc_right.left;
	rc_right_bottom.top = rc_right_top.bottom + BORDER_Y;
	rc_right_bottom.right = rc_right.right;
	rc_right_bottom.bottom = rc_right.bottom;
	/*设置绘制4*4矩形各个小方格的位置参数*/
	g_speed = t_speed = 1000 - GAME_DIFF * 280;/*初始下落速度*/
}

void fillBlock()/*下落到底部后填充矩阵*/
{
	int i, j;
	for (i = 0; i<4; i++)
	{
		for (j = 0; j<4; j++)
		{
			if (bCurTeris[i][j])/*根据下落方阵形状填充*/
			{
				g_hasBlocked[curPosY + i][curPosX + j] = 1;
			}
		}
	}
}

void RotateTeris(BOOL bTeris[4][4])/*旋转方块*/
{
	BOOL bNewTeris[4][4];
	int x, y;
	for (x = 0; x<4; x++)
	{
		for (y = 0; y<4; y++)
		{
			bNewTeris[x][y] = bTeris[3 - y][x];/*逆时针转换方块*/
			//逆时针：
			//bNewTeris[x][y] = bTeris[y][3-x];
		}
	}
	if (CheckValide(curPosX, curPosY, bNewTeris) == 1)/*检查变换后方块位置的合法性*/
	{
		memcpy(bTeris, bNewTeris, sizeof(bNewTeris));/*成功则更新形状*/
	}

}

int RandomInt(int _min, int _max)/*获取随机数*/
{
	srand((rd_seed++) % 65532 + GetTickCount());
	return _min + rand() % (_max - _min);
}

VOID DrawTeris(HDC mdc)/*绘制正在下落的方块*/
{

	int i, j;
	HPEN hPen = (HPEN)GetStockObject(BLACK_PEN);/*黑色画笔*/
	HBRUSH hBrush = (HBRUSH)GetStockObject(WHITE_BRUSH);/*白色画刷*/
	SelectObject(mdc, hPen);
	SelectObject(mdc, hBrush);
	for (i = 0; i<4; i++)
	{
		for (j = 0; j<4; j++)
		{
			if (bCurTeris[i][j])
			{
				Rectangle(mdc, (j + curPosX)*BLOCK_SIZE + BORDER_X, (i + curPosY)*BLOCK_SIZE + BORDER_Y, (j + 1 + curPosX)*BLOCK_SIZE + BORDER_X, (i + 1 + curPosY)*BLOCK_SIZE + BORDER_Y);/*绘制下落方块外轮廓*/
			}
		}
	}
	drawBlocked(mdc);/*绘制当前已经存在方块的区域*/
	DeleteObject(hPen);/*删除画笔句柄*/
	DeleteObject(hBrush);/*删除画刷句柄*/
}

VOID DrawBackGround(HDC hdc)/*绘制背景*/
{

	HBRUSH hBrush = (HBRUSH)GetStockObject(GRAY_BRUSH);/*灰色画刷*/
	HDC mdc = CreateCompatibleDC(hdc);/*创建一个与指定设备兼容的内存设备上下文环境*/
	HBITMAP hBitmap = CreateCompatibleBitmap(hdc, SCREEN_X, SCREEN_Y);/*创建与指定的设备环境相关的设备兼容的位图*/

	SelectObject(mdc, hBrush);
	SelectObject(mdc, hBitmap);

	HBRUSH hBrush2 = (HBRUSH)GetStockObject(WHITE_BRUSH);/*白色画刷*/
	FillRect(mdc, &rc_main, hBrush2);/*使用指定的刷子填充矩形*/
	Rectangle(mdc, rc_left.left + BORDER_X, rc_left.top + BORDER_Y, rc_left.right, rc_left.bottom);
	Rectangle(mdc, rc_right.left + BORDER_X, rc_right.top + BORDER_Y, rc_right.right, rc_right.bottom);
	DrawTeris(mdc);/*绘制正在下落的方块*/
	drawNext(mdc);/*绘制下一个下落的方块*/
	drawScore(mdc);/*绘制得分*/
	::BitBlt(hdc, 0, 0, SCREEN_X, SCREEN_Y, mdc, 0, 0, SRCCOPY);/*SRCCOPY:将源矩形区域直接拷贝到目标矩形区域*/
	DeleteObject(hBrush);
	DeleteDC(mdc);
	DeleteObject(hBitmap);
	DeleteObject(hBrush2);


	//  int x,y;
	//  HPEN hPen = (HPEN)GetStockObject(NULL_PEN);
	//  HBRUSH hBrush = (HBRUSH)GetStockObject(GRAY_BRUSH);
	//  SelectObject(hdc,hPen);
	//  SelectObject(hdc,hBrush);
	//  for (x = 0;x<NUM_X;x++)
	//  {
	//      for(y=0;y<NUM_Y;y++)
	//      {
	//          Rectangle(hdc,BORDER_X+x*BLOCK_SIZE,BORDER_Y+y*BLOCK_SIZE,
	//              BORDER_X+(x+1)*BLOCK_SIZE,
	//              BORDER_Y+(y+1)*BLOCK_SIZE);
	//      }
	//  }
}

void drawNext(HDC hdc)/*绘制下一个将要掉落的方块*/
{
	int i, j;
	HBRUSH hBrush = (HBRUSH)CreateSolidBrush(RGB(0, 188, 0));/*绿色画刷*/
	SelectObject(hdc, hBrush);
	for (i = 0; i<4; i++)
	{
		for (j = 0; j<4; j++)
		{
			if (bNextCurTeris[i][j])
			{
				Rectangle(hdc, rc_right_top.left + BLOCK_SIZE*(j + 1), rc_right_top.top + BLOCK_SIZE*(i + 1), rc_right_top.left + BLOCK_SIZE*(j + 2), rc_right_top.top + BLOCK_SIZE*(i + 2));
			}
		}
	}
	HFONT hFont = CreateFont(30, 0, 0, 0, FW_THIN, 0, 0, 0, UNICODE, 0, 0, 0, 0, L"微软雅黑");/*创建具有指定特性的逻辑字体*/
	SelectObject(hdc, hFont);
	SetBkMode(hdc, TRANSPARENT);/*设置指定设备上下文的背景混合模式*/
	SetBkColor(hdc, RGB(255, 255, 0));/*设置当前的背景色为白色*/
	RECT rect;/*绘制右部面板框*/
	rect.left = rc_right_top.left + 40;
	rect.top = rc_right_top.bottom - 150;
	rect.right = rc_right_top.right;
	rect.bottom = rc_right_top.bottom;
	DrawTextW(hdc, TEXT("下一个"), _tcslen(TEXT("下一个")), &rect, 0);/*输出文本*/
	DeleteObject(hFont);
	DeleteObject(hBrush);
}

void drawScore(HDC hdc)/*绘制分数和难度*/
{
	HFONT hFont = CreateFont(30, 0, 0, 0, FW_THIN, 0, 0, 0, UNICODE, 0, 0, 0, 0, L"微软雅黑");
	SelectObject(hdc, hFont);
	SetBkMode(hdc, TRANSPARENT);
	SetBkColor(hdc, RGB(255, 255, 0));
	RECT rect;
	rect.left = rc_right_bottom.left;
	rect.top = rc_right_bottom.top;
	rect.right = rc_right_bottom.right;
	rect.bottom = rc_right_bottom.bottom;
	TCHAR szBuf[30];
	LPCTSTR cstr = TEXT("当前难度：%d");
	wsprintf(szBuf, cstr, GAME_DIFF);
	DrawTextW(hdc, szBuf, _tcslen(szBuf), &rect, DT_CENTER | DT_VCENTER);

	RECT rect2;
	rect2.left = rc_right_bottom.left;
	rect2.top = rc_right_bottom.bottom / 2 + 100;
	rect2.right = rc_right_bottom.right;
	rect2.bottom = rc_right_bottom.bottom;
	TCHAR szBuf2[30];
	LPCTSTR cstr2 = TEXT("得分：%d");
	wsprintf(szBuf2, cstr2, GAME_SCORE);
	//outPutBoxInt(sizeof(szBuf));
	DrawTextW(hdc, szBuf2, _tcslen(szBuf2), &rect2, DT_CENTER | DT_VCENTER);
	/*DT_CENTER:在矩形中水平居中文本*/
	/*DT_VCENTER:在矩形中垂直居中文本*/

	DeleteObject(hFont);
}

int selectDiffculty(HWND hWnd, int diff)/*选择难度*/
{
	TCHAR szBuf2[30];
	LPCTSTR cstr2 = TEXT("确认选择难度 %d 吗？");
	wsprintf(szBuf2, cstr2, diff);
	if (MessageBox(hWnd, szBuf2, L"难度选择", MB_YESNO) == IDYES)
	{
		GAME_DIFF = diff;
		InvalidateRect(hWnd, &rc_right_bottom, false);
		GAME_STATE |= 2;
		init_game();
		return GAME_DIFF;
	}
	return -1;
}

int selectLayOut(HWND hWnd, int layout)/*选择布局，一行的格子数*/
{
	NUM_X = 10 * layout;
	NUM_Y = 20 * layout;
	BLOCK_SIZE = 30 / layout;
	GAME_STATE |= 2;
	InvalidateRect(hWnd, &rc_right_bottom, false);
	init_game();
	return layout;
}

void drawCompleteParticle(int line)/*在消去的那行画一行空白做出消去的效果*/
{
	HWND hWnd = GetActiveWindow();/*检索附加到调用线程的消息队列的活动窗口的窗口句柄*/
	HDC hdc = GetDC(hWnd);/*设备上下文环境句柄*/
	HBRUSH hBrush = (HBRUSH)GetStockObject(GRAY_BRUSH);/*灰色画刷*/
	HPEN hPen = (HPEN)CreatePen(PS_DOT, 1, RGB(255, 255, 0));/*画笔句柄*/
	SelectObject(hdc, hBrush);
	SelectObject(hdc, hPen);
	Rectangle(hdc, BORDER_X,
		BORDER_Y + line*BLOCK_SIZE,
		BORDER_X + NUM_X*BLOCK_SIZE,
		BORDER_Y + BLOCK_SIZE*(1 + line));
	DeleteObject(hBrush);
	DeleteObject(hPen);
}