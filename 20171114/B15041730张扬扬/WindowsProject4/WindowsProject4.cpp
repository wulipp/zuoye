//WindowsProject4.cpp: 定义应用程序的入口点。
//

#include "stdafx.h"
#include "WindowsProject4.h"

#define MAX_LOADSTRING 100

#define GAME_WIDTH 10    //游戏区宽度
#define GAME_HEIGHT 20   //游戏区高度
#define INFO_WIDTH 6     //信息区宽度
#define INFO_HEIGHT 20   //信息区高度
#define CELL_SIZE 30     //单元格大小 30 * 30
#define MY_TIMER 1
#define DEFAULT_INTERVAL 500

//方块定义
BOOL BASIC_TETRIS[][4][4] =
{
	{ { 1,1,1,1 },{ 0,0,0,0 },{ 0,0,0,0 },{ 0,0,0,0 } },
	{ { 1,1,0,0 },{ 1,1,0,0 },{ 0,0,0,0 },{ 0,0,0,0 } },
	{ { 1,1,0,0 },{ 0,1,1,0 },{ 0,0,0,0 },{ 0,0,0,0 } },
	{ { 0,1,1,0 },{ 1,1,0,0 },{ 0,0,0,0 },{ 0,0,0,0 } },
	{ { 0,1,0,0 },{ 1,1,1,0 },{ 0,0,0,0 },{ 0,0,0,0 } },
	{ { 1,1,0,0 },{ 0,1,0,0 },{ 0,1,0,0 },{ 0,0,0,0 } },
	{ { 1,1,0,0 },{ 1,0,0,0 },{ 1,0,0,0 },{ 0,0,0,0 } }
};
#define TETRIS_CNT (sizeof(BASIC_TETRIS))/(sizeof(BASIC_TETRIS[0]))    // 7

//记录方块
BOOL NOW_TETRIS[4][4];        //当前初始化的方块
BOOL NEXT_TETRIS[4][4];       //下一个方块
BOOL CUR_TETRIS[GAME_WIDTH][GAME_HEIGHT];   //在整个网格中记录方块落下的位置（下落的最终位置）


//记录初始化信息
UINT TetrisX;                //初始化方块左上角坐标
UINT TetrisY;
UINT GameTime;              //时间间隔
UINT GameScore;             //得分

UINT g_uiMySeed = 0xffff;

// 全局变量: 
HINSTANCE hInst;                                // 当前实例
WCHAR szTitle[MAX_LOADSTRING];                  // 标题栏文本
WCHAR szWindowClass[MAX_LOADSTRING];            // 主窗口类名

// 此代码模块中包含的函数的前向声明: 
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

VOID InitGame();                        //初始化游戏
VOID DrawBackground(HDC hdc);           //绘制游戏背景网格           
VOID DrawInfo(HDC hdc);                 //绘制信息区
VOID DrawTetris(HDC hdc, int nStartX, int nStartY, BOOL bTetris[4][4]);  //画方块
BOOL CheckDownTetris(int tetrisX, int tetrisY, BOOL now_tetirs[4][4], BOOL cur_tetirs[GAME_WIDTH][GAME_HEIGHT]);  //检查方块下落
int GetRandNum(int iMin, int iMax);   //随机数
VOID RefreshTetris(int tetrisX, int tetrisY, BOOL now_tetirs[4][4], BOOL cur_tetirs[GAME_WIDTH][GAME_HEIGHT]);  //刷新
VOID RotateTetris(BOOL bTetris[4][4]);   //旋转

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: 在此放置代码。

    // 初始化全局字符串
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_WINDOWSPROJECT4, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // 执行应用程序初始化: 
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_WINDOWSPROJECT4));

    MSG msg;

    // 主消息循环: 
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  函数: MyRegisterClass()
//
//  目的: 注册窗口类。
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_WINDOWSPROJECT4));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_WINDOWSPROJECT4);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   函数: InitInstance(HINSTANCE, int)
//
//   目的: 保存实例句柄并创建主窗口
//
//   注释: 
//
//        在此函数中，我们在全局变量中保存实例句柄并
//        创建和显示主程序窗口。
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // 将实例句柄存储在全局变量中

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  函数: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  目的:    处理主窗口的消息。
//
//  WM_COMMAND  - 处理应用程序菜单
//  WM_PAINT    - 绘制主窗口
//  WM_DESTROY  - 发送退出消息并返回
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	RECT rect;
	int nWinWidth, nWinHeight,nClientWidth,nClientHeight;
	BOOL newTetris[4][4] = {};   //用来保存旋转后的方块
    switch (message)
    {
	case WM_CREATE:                  //创建窗口时用到此消息
		//获取窗口大小
		GetWindowRect(hWnd,&rect);
		nWinWidth = rect.right - rect.left;
		nWinHeight = rect.bottom - rect.top;
		//获取客户区大小
		GetClientRect(hWnd,&rect);
		nClientWidth = rect.right - rect.left;
		nClientHeight = rect.bottom - rect.top;
		//移动窗口（并设置窗口大小）
		MoveWindow(hWnd,0,0,(GAME_WIDTH + INFO_WIDTH) * CELL_SIZE + (nWinWidth - nClientWidth),
			                 GAME_HEIGHT + INFO_HEIGHT * CELL_SIZE + (nWinHeight - nClientHeight),true);
		InitGame();   //初始化游戏
		SetTimer(hWnd,MY_TIMER,GameTime,NULL);   //设置定时器
		break;
	case WM_TIMER:                 //定时器
		if (CheckDownTetris(TetrisX,TetrisY + 1,NOW_TETRIS,CUR_TETRIS))
		{
			TetrisY++;
		}
		else
		{
			if (TetrisY == 0)   //已经摆满了
			{
				KillTimer(hWnd, MY_TIMER);
			}
			//刷新方块
			RefreshTetris(TetrisX,TetrisY,NOW_TETRIS,CUR_TETRIS);
		}
		InvalidateRect(hWnd,NULL,TRUE);  //重新绘制
		break;
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // 分析菜单选择: 
            switch (wmId)
            {
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:   //窗口显示 
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            //绘制游戏背景
			DrawBackground(hdc);
		    //绘制信息区
			DrawInfo(hdc);
			//绘制方块
			DrawTetris(hdc, TetrisX, TetrisY, NOW_TETRIS);
            EndPaint(hWnd, &ps);
        }
        break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_LEFT:			
			if (CheckDownTetris(TetrisX - 1,TetrisY,NOW_TETRIS,CUR_TETRIS))
			{
				TetrisX--;
				InvalidateRect(hWnd,NULL,TRUE);
			}
			else
			{
				MessageBeep(0);
			}
			break;
		case VK_RIGHT:
			if (CheckDownTetris(TetrisX + 1, TetrisY, NOW_TETRIS, CUR_TETRIS))
			{
				TetrisX ++;
				InvalidateRect(hWnd, NULL, TRUE);
			}
			else
			{
				MessageBeep(0);
			}
			break;
		case VK_UP:
			memcpy(newTetris,NOW_TETRIS,sizeof(newTetris));
			RotateTetris(newTetris);
			if (CheckDownTetris(TetrisX, TetrisY, newTetris, CUR_TETRIS))
			{
				memcpy(NOW_TETRIS, newTetris, sizeof(NOW_TETRIS));
				InvalidateRect(hWnd, NULL, TRUE);
			}
			else
			{
				MessageBeep(0);
			}
			break;
		case VK_DOWN:  
			if (CheckDownTetris(TetrisX, TetrisY + 1, NOW_TETRIS, CUR_TETRIS))
			{
				TetrisY ++;
				InvalidateRect(hWnd, NULL, TRUE);
			}
			else
			{
				MessageBeep(0);
			}
			break;
		default:
			break;
		}
		break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// “关于”框的消息处理程序。
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
//初始化
VOID InitGame()
{
	TetrisX = (GAME_WIDTH - 4) / 2;   //初始化方块左上角坐标
	TetrisY = 0;
	GameScore = 0;
	GameTime = DEFAULT_INTERVAL;
 
	memcpy(NOW_TETRIS,BASIC_TETRIS[GetRandNum(0, TETRIS_CNT)],sizeof(NOW_TETRIS));    //初始化当前方块
	memcpy(NEXT_TETRIS, BASIC_TETRIS[GetRandNum(0, TETRIS_CNT)], sizeof(NEXT_TETRIS));  //初始化下一个方块
	memset(CUR_TETRIS,0,sizeof(CUR_TETRIS));    //清空（清零）保存已经存在的方块的数组
}

/*
* 绘制静态的游戏背景
*
* 普通网格用灰色填充，底部已经下落合并的方块用黑色填充
*/
VOID DrawBackground(HDC hdc) 
{
	HPEN hpen = (HPEN)GetStockObject(NULL_PEN);   //画笔
	HBRUSH hBrush = (HBRUSH)GetStockObject(GRAY_BRUSH);  //方块移动时填充的颜色

	HBRUSH down_Brush = (HBRUSH)GetStockObject(BLACK_BRUSH);  //方块到底合并后填充的颜色
    //画矩形 参数：设备环境句柄、左上角xy坐标、右下角xy坐标
	Rectangle(hdc,0,0,GAME_WIDTH * CELL_SIZE,GAME_HEIGHT * CELL_SIZE);
	
	SelectObject(hdc,hpen);
	int i, j;
	for ( i = 0; i < GAME_WIDTH; i++)
	{
		for ( j = 0; j < GAME_HEIGHT; j++)
		{
			if (CUR_TETRIS[i][j])    //遇到下落后合并的方块的坐标
			{
				SelectObject(hdc,down_Brush);   //填充黑色
			}
			else
			{
				SelectObject(hdc,hBrush);      //填充灰色
			}
			//画网格(单元格)
			Rectangle(hdc,i * CELL_SIZE, j * CELL_SIZE,(i + 1) * CELL_SIZE,(j + 1) * CELL_SIZE);
		}
	}
}

/*
* 绘制信息区
*/
VOID DrawInfo(HDC hdc)
{
	HPEN hpen = (HPEN)GetStockObject(BLACK_PEN);   //画笔
	HBRUSH hBrush = (HBRUSH)GetStockObject(WHITE_BRUSH);     //白色背景
	HPEN tetris_Hpen = (HPEN)GetStockObject(NULL_PEN);
	HBRUSH tetris_Brush = (HBRUSH)GetStockObject(GRAY_BRUSH);//方块填充颜色
	SelectObject(hdc, hpen);
	SelectObject(hdc,hBrush);
	Rectangle(hdc,GAME_WIDTH * CELL_SIZE,0,(GAME_WIDTH + INFO_WIDTH) * CELL_SIZE,INFO_HEIGHT * CELL_SIZE);
	int x, y;
	SelectObject(hdc, tetris_Hpen);
	for (x = 0; x < 4; x++)
	{
		for ( y = 0; y < 4; y++)
		{
			if (NEXT_TETRIS[x][y])
			{
				SelectObject(hdc,tetris_Brush);
			}
			else
			{
				SelectObject(hdc, hBrush);
			}
			Rectangle(hdc,(GAME_WIDTH + y + 1) * CELL_SIZE,(x + 1) * CELL_SIZE,
				          (GAME_WIDTH + y + 2) * CELL_SIZE,(x + 2) * CELL_SIZE);
		}
	}
	//得分部分用矩形画
	RECT rect;
	TCHAR szBuf[100];   //得分的字符串

	rect.left = GAME_WIDTH * CELL_SIZE;
	rect.top = 9 * CELL_SIZE;
	rect.right = (GAME_WIDTH + INFO_WIDTH) * CELL_SIZE;
	rect.bottom = 14 * CELL_SIZE;

	wsprintf(szBuf, L"得分：%d", GameScore);   //函数wsprintf()将一系列的字符和数值输入到缓冲区。
	//在指定的矩形里写入格式化文本
	//DT_CENTER：使正文在矩形中水平居中
	DrawText(hdc, szBuf, wcslen(szBuf), &rect, DT_CENTER);
}

/*
* 画方块
*
* 方块为白色填充，黑色边框
* nStartX、nStartY 为方块左上角坐标
*/ 
VOID DrawTetris(HDC hdc,int nStartX,int nStartY,BOOL bTetris[4][4])
{
	HPEN hPen = (HPEN)GetStockObject(BLACK_PEN);
	HBRUSH hBrush = (HBRUSH)GetStockObject(WHITE_BRUSH);
	SelectObject(hdc,hPen);
	SelectObject(hdc,hBrush);

	int x, y;
	for ( x = 0; x < 4; x++)
	{
		for ( y = 0; y < 4; y++)
		{
			if (bTetris[x][y])   //画方块
			{                    //nStartX,nStary为方块左上角坐标，x, y为偏移             
				Rectangle(hdc,(nStartX + y) * CELL_SIZE,(nStartY + x) * CELL_SIZE,
					      (nStartX + y + 1) * CELL_SIZE,(nStartY + x + 1) * CELL_SIZE);
			}
		}

	}
}

/*
* 检查方块下落是否满足 
* 左上角x,y坐标
* 当前方块、网格中已经存在的方块数组
*/
BOOL CheckDownTetris(int tetrisX,int tetrisY,BOOL now_tetirs[4][4], BOOL cur_tetirs[GAME_WIDTH][GAME_HEIGHT])
{
	if (tetrisX < 0)   //碰到左墙
	{
		return FALSE;
	}
	int x, y;
	for ( x = 0; x < 4; x++)
	{
		for ( y = 0; y < 4; y++)
		{
			if (now_tetirs[x][y])
			{
				//碰到右墙
				if (tetrisX + y  >= GAME_WIDTH )
				{
					return FALSE;
				}
				//碰到底墙
				if (tetrisY + x  >= GAME_HEIGHT )
				{
					return FALSE;
				}
				//碰到底下已有的方块
				if (cur_tetirs[tetrisX + y][tetrisY + x])  //已经存在的方块
				{
					return FALSE;
				}
			}
		}
	}
}

//刷新方块  (当调用此方法时，方块已经下落到底部)
/*
  1、将方块记录在 网格数组中
  2、循环整个网格，从底向上，查找满行
  3、重新排列新网格（去掉满行）
*/
VOID RefreshTetris(int tetrisX, int tetrisY, BOOL now_tetirs[4][4], BOOL cur_tetirs[GAME_WIDTH][GAME_HEIGHT])
{
	int x, y;
	for ( x = 0; x < 4; x++)
	{
		for ( y = 0; y < 4; y++)
		{
			if (now_tetirs[x][y])
			{
				cur_tetirs[tetrisX + y][tetrisY + x] = TRUE;   //把方块记录网格数组中
			}
		}
	}
	//循环整个网格
	
	int fullLine = 0;
	int newY;    //记录新坐标，当满行，用上一行代替这一满行
	int i, j;
	for ( i = GAME_HEIGHT,newY = GAME_HEIGHT; i >= 0; i--)
	{
		BOOL flag = FALSE;
		for ( j = 0; j < GAME_WIDTH; j++)
		{
			cur_tetirs[j][newY] = cur_tetirs[j][i];    //y是递减的，如果有满行，newY不递减，就用上一行代替这满行
			if (!cur_tetirs[j][i])   //不满行
			{
				flag = TRUE;  
			}
		}
		if (flag)   //因为不满行，newY和y一样递减
		{
			newY--;   
		}
		else      //满行，newY不递减，y递减，用上一行代替这满行
		{
			fullLine++;  //满行数 +1
		}
	}
	if (fullLine > 0)
	{
		GameScore += fullLine * 1;  //分数增加
	}
	memcpy(NOW_TETRIS,NEXT_TETRIS,sizeof(NOW_TETRIS));   //把下一个方格拷贝给当前方格
	memcpy(NEXT_TETRIS,BASIC_TETRIS[GetRandNum(0,TETRIS_CNT)],sizeof(NEXT_TETRIS));  //生成下一方格

	TetrisX = (GAME_WIDTH - 4) / 2;  //居中
	TetrisY = 0;
}

//获取随机数
int GetRandNum(int iMin, int iMax)
{
	//取随机数  
	srand(GetTickCount() + g_uiMySeed--);
	return iMin + rand() % (iMax - iMin);
}

//旋转方块，并且靠左上角
VOID RotateTetris(BOOL bTetris[4][4])
{
	BOOL bNewTetris[4][4] = {};   //初始化为空
	int x, y;
	int xPos, yPos;
	BOOL bFlag;       //靠近左上角
					  // 从上往下，从左往右，顺时针旋转
					  //靠上
	for (x = 0, xPos = 0; x < 4; x++)
	{
		bFlag = FALSE;
		for (y = 0; y < 4; y++)
		{
			bNewTetris[xPos][y] = bTetris[3 - y][x];
			if (bNewTetris[xPos][y])
			{
				bFlag = TRUE;  //这一行有数据
			}
		}
		if (bFlag)
		{
			xPos++;
		}
	}
	memset(bTetris, 0, sizeof(bNewTetris));   //内存空间初始化
											  //靠左
	for (y = 0, yPos = 0; y < 4; y++)
	{
		bFlag = FALSE;
		for (x = 0; x < 4; x++)
		{
			bTetris[x][yPos] = bNewTetris[x][y];
			if (bTetris[x][yPos])
			{
				bFlag = TRUE;
			}
		}
		if (bFlag)
		{
			yPos++;
		}
	}
	return;
}