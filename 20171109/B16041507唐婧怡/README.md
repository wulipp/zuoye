## 11.09 作业 注释分配代码

### 运行截图

1. basic

运行截图1

![basicDisplay1](https://gitee.com/uploads/images/2017/1209/114705_5bf6efa8_1578283.jpeg "basic(Display1).jpg")

运行截图2

![basicDisplay2](https://gitee.com/uploads/images/2017/1209/114812_44ab9d54_1578283.jpeg "basic(Display2).jpg")

2. DataType

运行截图1

![DataTypeDisplay1](https://gitee.com/uploads/images/2017/1209/114938_96e89166_1578283.jpeg "DataType(Display1).jpg")

运行截图2

![DataTypeDisplay2](https://gitee.com/uploads/images/2017/1209/115010_821b8d80_1578283.jpeg "DataType(Display2).jpg")

运行截图3

![DataTypeDisplay3](https://gitee.com/uploads/images/2017/1209/115100_b4e48cd7_1578283.jpeg "DataType(Display3).jpg")

运行截图4

![DataTypeDisplay4](https://gitee.com/uploads/images/2017/1209/115135_4dc6eec8_1578283.jpeg "DataType(Display4).jpg")

### 说明
* 被分配到的代码是ch2中的basic和DataType项目
