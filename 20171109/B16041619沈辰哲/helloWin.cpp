#include<Windows.h>
#include<tchar.h> 
//声明窗口函数
LRESULT CALLBACK WindowProc(HWND hwnd,
       UINT uMsg,
       WPARAM wParam,
       LPARAM lparam
       );
int WINAPI WinMain(
     HINSTANCE hInstance,
     HINSTANCE hPrevInatance,
     LPSTR lpCmdLine,
     int nCmdShow
     )
{
 WNDCLASS wndclass;
 wndclass.lpfnWndProc=WindowProc;
 wndclass.cbClsExtra=0;
 wndclass.cbWndExtra=0;
 wndclass.style=CS_HREDRAW|CS_VREDRAW;
 wndclass.lpszClassName=_T("hello world");
 wndclass.hInstance=hInstance;
 wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);
 wndclass.hIcon=0;
 wndclass.hbrBackground=(HBRUSH)(COLOR_WINDOW+1);
 wndclass.lpszMenuName=0;
 //注册窗口类
 if(RegisterClass(&wndclass)==0)
 {
  MessageBox(0,_T("注册窗口类失败"),_T("hello world"),MB_OK);
  return 0;
 }
 //创建窗口实列
 HWND hWnd = CreateWindow(_T("hello world"),_T("hello world"),WS_OVERLAPPEDWINDOW,100,100,500,400,0,0,hInstance,0);
 //显示和更新窗口
 ShowWindow(hWnd,SW_SHOW);
 UpdateWindow(hWnd);
 //消息循环
 MSG msg;
 while(GetMessage(&msg,0,0,0))
 {
  TranslateMessage(&msg);
  DispatchMessage(&msg);
 }
 return 0;
}
//定义窗口函数
LRESULT CALLBACK WindowProc(
       HWND hwnd,
       UINT uMsg,
       WPARAM wParam,
       LPARAM IParam
       )
{
 switch(uMsg)
 {
 case WM_CLOSE:
  PostQuitMessage(0);
  break;
 default:
  return DefWindowProc(hwnd,uMsg,wParam,IParam);
 }
 return 0;
}
