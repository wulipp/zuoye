/* ************************************
*《精通Windows API》
* 示例代码
* setup.c
* 15.3 setup.exe
**************************************/
/* 头文件 */
#include <Windows.h>
#include <Setupapi.h>
#include <shlobj.h>

/* 库 */
#pragma comment (lib, "shell32.lib")
#pragma comment (lib, "Setupapi.lib")
/*静态添加两个lib文件*/

/*************************************
* VOID GetSourceDirectory(LPSTR szPath)
* 功能 获得当前路径
* szPath，返回路径
**************************************/
VOID GetSourceDirectory(LPSTR szPath) //LPSTR为 char * 类型
{
	int i;

	GetModuleFileName(NULL,szPath,MAX_PATH);//调用windows.h的GetModuleFileNameA，获得文件当前路径
	i=strlen(szPath);
	while ((i>0)&&(szPath[i-1]!='\\'))  //while 循环判断
	{
		szPath[--i]=0;
	}
}

/*************************************
* WinMain
* 功能 调用相关Setup API进行安装
**************************************/
INT WinMain(
			HINSTANCE hInstance,        //应用程序当前实例的句柄
			HINSTANCE hPrevInstance,   //应用程序的先前实例的句柄
			LPSTR lpCmdLine,             //应用程序命令行的字符串的指针
			int nCmdShow                //指明窗口如何显示
			)
{
	HINF hInf; 						        // INF文件句柄
	CHAR szSrcPath[MAX_PATH];				// 源路径
	CHAR szDisPath[MAX_PATH];				// 目的路径
	BOOL bResult;  					        //BOOL为int类型
	PVOID pContext;  				        //PVOID为指针类型
	// 与本程序在同一目录下的Setup.inf
	GetSourceDirectory(szSrcPath);
	lstrcat(szSrcPath,"setup.inf");         //调用WinBase.h的IstrcatA函数
	// 打开 inf 文件
	hInf = SetupOpenInfFile(szSrcPath, NULL, INF_STYLE_WIN4, NULL);//调用Windows中SetupOpenInfFileA函数，打开文件

	if (hInf == INVALID_HANDLE_VALUE)       // 判断是否成功
	{
		MessageBox(NULL,
			"Error: Could not open the INF file.",
			"ERROR",
			MB_OK|MB_ICONERROR);				//如果打开失败，弹出窗口显示inf文件打开失败
		return FALSE;
	}
                                            // 获得Program Files的路径
	SHGetSpecialFolderPath(NULL,
		szDisPath, CSIDL_PROGRAM_FILES , FALSE);
                                            // 构造目的路径
	lstrcat(szDisPath,"\\MyInstall");

                                            // 给inf配置文件中的路径ID赋值，使用路径替换路径ID
	bResult = SetupSetDirectoryId(hInf, 32768, szDisPath);
	if (!bResult)
	{
		MessageBox(NULL,
			"Error: Could not associate a directory ID with the destination directory.",
			"ERROR",
			MB_OK|MB_ICONERROR);
		SetupCloseInfFile(hInf);
		return FALSE;
	}

                                        // 设置默认callback函数的参数
	pContext=SetupInitDefaultQueueCallback(NULL);

	// 进行安装
	bResult=SetupInstallFromInfSection(
		NULL,                               // 父窗口句柄，默认为NULL
		hInf,                               // INF文件句柄
		"Install",                          // INF文件中，配置了安装信息的节名
		SPINST_FILES | SPINST_REGISTRY ,    // 安装标志
		NULL,                               // 安装键值
		NULL,                               // 源文件和路径，可以在INF文件中配置
		0,                                  // 复制时的动作
		(PSP_FILE_CALLBACK)SetupDefaultQueueCallback,    // 回调函数
		pContext,                           // 回调函数的参数
		NULL,                               // 设备信息
		NULL                                // 设备信息
		);
	if (!bResult)                           // 安装是否成功
	{
                                            // 失败，输出错误信息
		MessageBox(NULL,
			"SetupInstallFromInfSection",
			"ERROR",
			MB_OK|MB_ICONERROR);			//如果没有安装成功，弹出窗口显示安装失败
                                                //关闭
		SetupTermDefaultQueueCallback(pContext);
		SetupCloseInfFile(hInf);
		return FALSE;
	}

                                            // 关闭
	SetupTermDefaultQueueCallback(pContext);
	SetupCloseInfFile(hInf);

	return TRUE;
}
