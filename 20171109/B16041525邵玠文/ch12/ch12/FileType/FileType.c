#include <Windows.h>
#include <shlobj.h>

int WINAPI WinMain(         //#define WINAPI __stdcall
	HINSTANCE hInstance,//实例句柄 
    HINSTANCE hPrevInstance,
    LPSTR lpCmdLine,    //LPSTR : typedef char CHAR; typedef CHAR *NPSTR,*LPSTR,*PSTR; 
    int nCmdShow
	)
{
	WinExec("regedit win.reg", nCmdShow);//运行指定的程序，返回值大于31表示成功 
	SHChangeNotify(SHCNE_ASSOCCHANGED, SHCNF_IDLIST, NULL, NULL);
	//利用SHChangeNotify函数来刷新系统，达到让注册表新的设置立即生效 
	//SHCHE_ASSOCCHANGED：修改文件关联 
	//SHCNF_IDLIST, NULL, NULL：项目标识符列表的地址都为NULL 
}
