#include <Windows.h>
//#include <shobjidl.h>
#include <shlobj.h>

//#pragma comment (lib , "shell32.lib");
DWORD ListFileInRecycleBin(HWND hwnd);        //父窗口句柄 
int WinMain(
			HINSTANCE hInstance,
			HINSTANCE hPrevInstance,
			LPSTR lpCmdLine,
			int nCmdShow
			)
{

	CHAR szMyDocument[MAX_PATH];	// My Document的路径
	CHAR szDesktop[MAX_PATH];	//DeskTop的路径
	
	LPITEMIDLIST pidl = NULL;            //文件目录为空 
	LPMALLOC pMalloc = NULL;			//用于接收Shell的lMalloc接口指针的地址 
	// 第一种方法获取特殊目录路径
	SHGetSpecialFolderPath(NULL,szMyDocument,CSIDL_PERSONAL,FALSE);       //该api用来获取指定的系统路径 

	SHGetMalloc(&pMalloc);
	// 第二种访求获取特殊目录路径
	SHGetFolderLocation(NULL, CSIDL_DESKTOPDIRECTORY, NULL, 0, &pidl);
	SHGetPathFromIDList(pidl,szDesktop);

	pMalloc->Free(pidl);
	pMalloc->Release();

	ListFileInRecycleBin(NULL);
}

DWORD ListFileInRecycleBin(HWND hwndShowResult)
{
	CHAR pszPath[MAX_PATH];

	IShellFolder *pisf = NULL;     //IshellFolder：用来定位某个文件夹，并对其下的文件和文件夹进行操作 
	IShellFolder *pisfDesktop = NULL;

	SHGetDesktopFolder(&pisfDesktop);		//得到桌面的目录 

	IEnumIDList *peidl = NULL;				//IEnumIDList接口用于遍历IShellFolder接口表示的文件夹下的所有对象 

	LPITEMIDLIST pidlBin = NULL;

	LPITEMIDLIST idlCurrent = NULL;
	BROWSEINFO bi = {0};				//设置浏览文件夹的属性 
	LPMALLOC pMalloc = NULL;

	STRRET strret;
	ULONG uFetched;

	SHGetMalloc(&pMalloc);

	SHGetFolderLocation(NULL, CSIDL_BITBUCKET, NULL, 0, &pidlBin);

	//REFIID riid;

	pisfDesktop->BindToObject(pidlBin,NULL,IID_IShellFolder,(void **) &pisf);

	pisf->EnumObjects(NULL,
		SHCONTF_FOLDERS | SHCONTF_NONFOLDERS |SHCONTF_INCLUDEHIDDEN,
		&peidl);



	while(1)
	{
		if(peidl->Next(1,&idlCurrent,&uFetched) == S_FALSE)
			break;
		SHGetPathFromIDList(idlCurrent,  pszPath);
		pisf->GetDisplayNameOf(idlCurrent,SHGDN_NORMAL,&strret);			//确定文件夹显示名 
	}

	pMalloc->Free(pidlBin);
	pMalloc->Free(strret.pOleStr);
	pMalloc->Release();

	peidl->Release();
	pisf->Release();
	return 0;	
}


DWORD Browse(HWND hwndShowResult) 
{
	CHAR szRecycleBin[MAX_PATH];

	LPITEMIDLIST pidlBin = NULL;

	LPITEMIDLIST pidlSelected = NULL;
	BROWSEINFO bi = {0};
	LPMALLOC pMalloc = NULL;

	SHGetMalloc(&pMalloc);

	SHGetFolderLocation(NULL, CSIDL_PERSONAL, NULL, 0, &pidlBin);
	SHGetPathFromIDList(pidlBin,szRecycleBin);

	bi.hwndOwner = hwndShowResult;											//父窗口句柄 
	bi.pidlRoot = pidlBin;													//要显示的文件夹的根 
	bi.pszDisplayName = "pszDisplayName";									//保存被选取的文件夹路径的缓冲区 
	bi.lpszTitle = "Choose a folder";										//显示对话框的标题 
	bi.ulFlags = 0;															//指定对话框的外观和功能的标志 
	bi.lpfn = NULL;															//处理事件的函数为空 
	bi.lParam = 0;															//应用程序传给函数的参数为0 

	pidlSelected = SHBrowseForFolder(&bi);		

	pMalloc->Free(pidlBin);
	pMalloc->Release();

	//	BOOL SHGetPathFromIDList(          LPCITEMIDLIST pidl,
	//    LPTSTR pszPath
	//);

	return 0;
}
