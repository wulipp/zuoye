/* ************************************
*《精通Windows API》 
* 示例代码
* CopySend.c
* 11.4 通过WM_COPYDATA进程间通信
**************************************/
/* 头文件 */
#include <windows.h> 
#include <commctrl.h>
#include "resource.h"
/* 全局变量 */
HINSTANCE hinst;//“句柄型”数据类型
HWND hwnd, hwndEdit;//窗口句柄
LPSTR lpszCopy =  "you can input any text\n then click \'Send\' Item at Menu"; //字符串数据类型
/* 函数声明 */
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int); 
LRESULT CALLBACK MainWndProc(HWND, UINT, WPARAM, LPARAM); 

/* ************************************
* WinMain 是一个WINDOWS程序的入口。
**************************************/
int WINAPI WinMain(
				   HINSTANCE hinstance,// 当前实例的句柄
				   HINSTANCE hPrevInstance, // 上一个实例的句柄，这个是为了和Win 3.1兼容，在Win32的程序中此参数无用
				   LPSTR lpCmdLine, // 指向命令行字符串的指针
				   int nCmdShow)   // window显示状态
{ 
	MSG msg; 
	WNDCLASSEX wcx; 

	hinst = hinstance;	// 保存应用程序实例

	// 填充WNDCLASSEX
	wcx.cbSize = sizeof(wcx);      
	wcx.style = CS_HREDRAW | CS_VREDRAW;//CS_HREDRAW当窗口水平方向的宽度变化时重绘整个窗口
										//CS_VREDRAW 当窗口垂直方向的宽度变化时重绘整个窗口
	wcx.lpfnWndProc = MainWndProc;		// 消息处理函数
	wcx.cbClsExtra = 0;        
	wcx.cbWndExtra = 0;             
	wcx.hInstance = hinstance;   
	wcx.hIcon = LoadIcon(NULL, 	IDI_APPLICATION);//LoadIcon函数从与hInstance模块相关联的可执行文件中装入lpIconName指定的图标资源，仅当图标资源还没有被装入时该函数才执行装入操作，否则只获取装入的资源句柄  
	wcx.hCursor = LoadCursor(NULL, IDC_ARROW);//从一个与应用事例相关的可执行文件（EXE文件）中载入指定的光标资源。                 
	wcx.hbrBackground = (HBRUSH)GetStockObject( WHITE_BRUSH);               
	wcx.lpszMenuName =  MAKEINTRESOURCE(IDR_MENU_COMMAND);   // 菜单
	wcx.lpszClassName = "copydata";		// 窗口类名
	wcx.hIconSm = NULL; 
	// 注册窗口类
	if(RegisterClassEx(&wcx) == 0)
	{
		return 0;
	}
	// 创建窗口
	hwnd = CreateWindow( //hwnd窗口句柄
		"copydata",        
		"Sample",           
		WS_TILEDWINDOW, 
		CW_USEDEFAULT, CW_USEDEFAULT, 500, 	400,    
		(HWND) NULL,        
		(HMENU) NULL,      
		hinstance,         
		(LPVOID) NULL);    
	if (!hwnd) 
		return 0; 

	// 显示、刷新窗口
	ShowWindow(hwnd, nCmdShow); 
	UpdateWindow(hwnd); 
	// 消息循环

	BOOL fGotMessage;
	while ((fGotMessage = GetMessage(&msg, (HWND) NULL, 0, 0)) != 0 && fGotMessage != -1) 
	{ 
		TranslateMessage(&msg); //将虚拟键消息转换为字符消息
		DispatchMessage(&msg); // 调度一个消息给窗口程序
	} 
	return msg.wParam; 
	UNREFERENCED_PARAMETER(lpCmdLine); 
} 

/* ************************************
* 消息处理函数
**************************************/
LONG APIENTRY MainWndProc(HWND hwnd,      //窗口句柄    
						  UINT message, //uint无符号整型
						  WPARAM wParam,         
						  LPARAM lParam)          
{ 
	switch (message) 
	{ 
	case WM_CREATE:
		{
			// 在主窗口创建时，创建一个EDIT控件，用于编辑需要复制的内容
			RECT rectMain;
			GetClientRect(hwnd,&rectMain);
			hwndEdit = CreateWindow("EDIT",    
				NULL,       
				WS_CHILD//WS_CHILD：创建一个子窗口。这个风格不能与WS_POPUP风格合用。
				| WS_VISIBLE//WS_VISIBLE：创建一个初始状态为可见的窗口。
				| WS_VSCROLL//WS_VSCROLL：创建一个有垂直滚动条的窗口。
				| WS_BORDER //WS_BORDER：创建一个带边框的窗口。 
				| ES_LEFT | ES_MULTILINE | ES_AUTOVSCROLL,//视窗样式 
				0., 0, rectMain.right, rectMain.bottom, 
				hwnd,  	NULL,
				(HINSTANCE) GetWindowLong(hwnd, GWL_HINSTANCE), //获得指定窗口的有关信息
				NULL);     
			if(hwndEdit == NULL)
			{
				MessageBox(hwnd, "Create Window Error","ERROR",MB_OK);
				ExitProcess(0);
			}
			// 设置EDIT控件的内容
			SendMessage(hwndEdit, WM_SETTEXT, 0, (LPARAM) lpszCopy); 
			break;
		}
	case WM_COMMAND:		// 菜单输入
		{
			// 如果通过菜单点击“send”按钮
			if (LOWORD(wParam) == ID_COMMAND_SEND) 
			{
				// 获取EDIT控件中的文本
				DWORD dwTextLenth = GetWindowTextLength(hwndEdit);//获得指定窗口的标题文本的字符长度
				LPSTR szText = (LPSTR)HeapAlloc(GetProcessHeap(),HEAP_NO_SERIALIZE,dwTextLenth);//分配内存
				GetWindowText(hwndEdit,szText,dwTextLenth);////获得指定窗口的标题文本
				// 构造 COPYDATASTRUCT
				COPYDATASTRUCT cds;
				HWND hwnd2 = FindWindow("copydata2","RECV COPY DATA");
				if(hwnd2 == NULL)
				{
					MessageBox(hwnd,"can not find window 2","ERROR",MB_OK);
					break;
				}
				cds.dwData = 0x12345678;			// 自定义的数据
				cds.cbData = dwTextLenth;		// lpData的长度
				cds.lpData = szText;				// 需要复制的数据
				// 发送消息，COPYDATASTRUCT结构的指针做为lParam
				SendMessage(    
					hwnd2,									// 目的窗口
					(UINT) WM_COPYDATA,		// 消息 ID
					(WPARAM) hwnd,		//  wParam，源窗口句柄
					(LPARAM) &cds			//  lParam，COPYDATASTRUCT
					); 
				// 释放内存					
				HeapFree(GetProcessHeap(),HEAP_NO_SERIALIZE,szText);
			} 
			break; 
		}
	case WM_DESTROY:	// 退出
		PostQuitMessage(0); 
		return 0; 

	default:		// 其他消息的处理
		return DefWindowProc(hwnd, message, wParam, lParam); 
	}
return 0; 
}