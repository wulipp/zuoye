# 作业要求
* 将分配到的精通Windows.API-函数
接口、编程实例中的代码添加注解

1. 非c语言自带的类型定义

2. 宏定义

3. windows函数（查询msdn.microsoft.com获得准确解释）

* 对于生成的代码运行通过

1. 熟悉单步执行 
2. 断点、条件断点 
3. 熟悉各调试窗口

# 作业结果截图
![输入图片说明](https://gitee.com/uploads/images/2017/1219/214147_63223db1_1581486.png "StringCode1.png")
![输入图片说明](https://gitee.com/uploads/images/2017/1219/214202_be316960_1581486.png "StringCode2.png")
![输入图片说明](https://gitee.com/uploads/images/2017/1219/214215_a2a1a142_1581486.png "StringCode3.png")
![输入图片说明](https://gitee.com/uploads/images/2017/1219/214232_9485b010_1581486.png "window.png")
