/* ************************************
*《精通Windows API》 
* 示例代码
* Event.c
* 9.2  字符界面程序
**************************************/
/* 头文件 */
#include <windows.h>
/* 函数声明 */
VOID KeyEventProc(KEY_EVENT_RECORD); //显示键盘输入事件
VOID MouseEventProc(MOUSE_EVENT_RECORD); //显示鼠标输入事件
VOID ResizeEventProc(WINDOW_BUFFER_SIZE_RECORD); //重置大小事件

/* ************************************
* DWORD UseEvent(VOID) 
* 功能	使用事件进行控制台操作
**************************************/
DWORD UseEvent(VOID) 
{ 
	CHAR chBuffer[256]; //char类型数组
	DWORD cRead;//unsigned long类型
	HANDLE hStdin; //指向void的指针类型
	DWORD cNumRead, fdwMode, fdwSaveOldMode, i; //unsigned long类型
	INPUT_RECORD irInBuf[128]; //结构体数组

	// 获取标准输入句柄
	hStdin = GetStdHandle(STD_INPUT_HANDLE); //函数：获得输入的缓冲器的HANDLE句柄
	if (hStdin == INVALID_HANDLE_VALUE) //句柄失效
		MyErrorExit("GetStdHandle"); //退出并显示错误信息

	// 保存当前的控制台模式
	if (! GetConsoleMode(hStdin, &fdwSaveOldMode) ) //获取控制台的输入缓冲区
		MyErrorExit("GetConsoleMode"); //退出并显示错误信息

	// 使能窗口鼠标输入事件
	fdwMode = ENABLE_WINDOW_INPUT | ENABLE_MOUSE_INPUT; //宏定义：记录鼠标事件和窗口输入事件
	if (! SetConsoleMode(hStdin, fdwMode) ) //开启鼠标事件
		MyErrorExit("SetConsoleMode"); //退出并显示错误信息

	// 循环读取输入
	while (1) 
	{ 

		// 等待事件
		if (! ReadConsoleInput( //宏定义：函数读取鼠标操作
			hStdin,      // 输入句柄
			irInBuf,     // 保存输入的缓冲区 
			128,         // 缓冲区大小 
			&cNumRead) ) // 实际读取的大小 
			MyErrorExit("ReadConsoleInput"); //退出并显示错误信息

		// 显示事件
		for (i = 0; i < cNumRead; i++) 
		{
			switch(irInBuf[i].EventType) //EventType为unsigned short类型
			{ 
			case KEY_EVENT: // 宏定义：键盘输入
				KeyEventProc(irInBuf[i].Event.KeyEvent); //KeyEvent为宏定义KEY_EVENT_RECORD结构体
				break; 

			case MOUSE_EVENT: // 宏定义：鼠标输入
				MouseEventProc(irInBuf[i].Event.MouseEvent); //MOUSE_EVENT_RECORD结构体
				break; 

			case WINDOW_BUFFER_SIZE_EVENT: // 宏定义：改变窗口控制台大小
				ResizeEventProc( 
					irInBuf[i].Event.WindowBufferSizeEvent); //WINDOW_BUFFER_SIZE_RECORD结构体
				break; 

			case FOCUS_EVENT:  // 控制台得到输入焦点

			case MENU_EVENT:   // 用户使用控制台菜单或工具栏 
				break; 

			default: 
				MyErrorExit("unknown event type"); //退出并显示错误信息
				break; 
			} 
		}
	} 
	return 0; 
}

/* ************************************
* VOID KeyEventProc(KEY_EVENT_RECORD ker)
* 功能	使用对话框显示键盘输入事件
* 参数	KEY_EVENT_RECORD ker  键盘事件
**************************************/
VOID KeyEventProc(KEY_EVENT_RECORD ker)
{
	CHAR szMsg[256];//char类型数组
	wsprintf(szMsg,"KEY_EVENT_RECORD\n Char = %c",
		ker.uChar);//将字符输出到缓冲区
	MessageBox(NULL,szMsg,"EVENT",MB_OK);//弹出一个标准的对话框
}

/* ************************************
* VOID MouseEventProc(MOUSE_EVENT_RECORD mer)
* 功能	使用对话框显示鼠标事件
* 参数	MOUSE_EVENT_RECORD mer 鼠标事件
**************************************/
VOID MouseEventProc(MOUSE_EVENT_RECORD mer)
{
	CHAR szMsg[256];//char类型数组
	wsprintf(szMsg,"MOUSE_EVENT_RECORD\n button state: %d\nmouse position X=%u,Y=%u",
		mer.dwButtonState,mer.dwMousePosition.X,mer.dwMousePosition.Y);//将字符输出到缓冲区
	MessageBox(NULL,szMsg,"EVENT",MB_OK);//弹出一个标准的对话框
	if(IDOK == MessageBox(NULL,"Exit?","EVENT",MB_OKCANCEL))//IDOK按确定按钮，MB_OKCANCEL一个确定一个取消按钮
	{
		ExitProcess(0);
	}
} 

/* ************************************
* VOID ResizeEventProc(WINDOW_BUFFER_SIZE_RECORD wbsr)
* 功能	重置大小事件
* 参数	WINDOW_BUFFER_SIZE_RECORD wbsr) 事件
**************************************/
VOID ResizeEventProc(WINDOW_BUFFER_SIZE_RECORD wbsr)
{
	CHAR szMsg[256];//char类型数组
	wsprintf(szMsg,"WINDOW_BUFFER_SIZE_RECORD\nX=%u,Y=%u",
		wbsr.dwSize.X,wbsr.dwSize.Y);//将字符输出到缓冲区
	MessageBox(NULL,szMsg,"EVENT",MB_OK);//弹出一个标准的对话框
} 