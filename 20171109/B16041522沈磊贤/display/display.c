/* ************************************
*《精通Windows API》 
* 示例代码
* display.c
* 10.1  系统信息
**************************************/
#include <windows.h>
#include <stdio.h>
void main()
{
   BOOL fResult;
   int aMouseInfo[3];
   //获取系统分辨率、窗体显示区域的宽度和高度、滚动条的宽度和高度
   fResult = GetSystemMetrics(SM_MOUSEPRESENT); 
 
   if (fResult == 0) 
      printf("No mouse installed.\n"); 
   else 
   { 
      printf("Mouse installed.\n");

      // Determine whether the buttons are swapped. 
      fResult = GetSystemMetrics(SM_SWAPBUTTON); 
 
      if (fResult == 0) 
         printf("Buttons not swapped.\n"); 
      else printf("Buttons swapped.\n");
 
      // Get the mouse speed and the threshold values. 
 	  //查询或设置系统级参数
      fResult = SystemParametersInfo(
         //#define SPI_GETMOUSE 0x0003
         SPI_GETMOUSE,  // get mouse information 
         0,             // not used 
         &aMouseInfo,   // holds mouse information 
         0);            // not used 

      if( fResult )
      { 
         printf("Speed: %d\n", aMouseInfo[2]); 
         printf("Threshold (x,y): %d,%d\n", 
            aMouseInfo[0], aMouseInfo[1]); 
      }
   } 
}
