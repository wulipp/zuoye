/* ************************************
*《精通Windows API》 
* 示例代码
* start.c
* 1.1.1	第一个示例程序，弹出消息对话框
**************************************/

/* 预处理　*/
/* 头文件　*/
#include <windows.h>
//连接时使用User32.lib
#pragma comment (lib, "User32.lib")

/* ************************************
* WinMain
* 功能	Windows应用程序示例
**************************************/
int WinMain(
			HINSTANCE hInstance,                 //HINSTANCE 是句柄型数据类型，相当于装入了内存的资源的ID。HINSTANCE对应的资源是instance.句柄实际上是一个 无符号长整数。
			HINSTANCE hPrevInstance,
			LPSTR lpCmdLine,  //Win32和VC++所使用的一种字符串数据类型。LPSTR被定义成是一个指向以NULL(‘\0’)结尾的32位ANSI字符数组指针
			int nCmdShow
			)
{
	// 调用API函数 MessageBox
	MessageBox(NULL,             
		TEXT("开始学习Windows编程"),
		TEXT("消息对话框"),
		MB_OK);         //MessageBox函式用于显示短资讯，被认为是一个对话方块，
	return 0;
}