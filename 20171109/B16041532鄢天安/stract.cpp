#include<stdio.h>
#include<stdlib.h> 

char* stract_s(char* str1,char* str2)
{
	if(str1==NULL||str2==NULL)
	{
		printf("input error!\n");
		return NULL;
	}
	int len1=0,len2=0;
	int i=0;
	while(str1[len1]!='\0')
	{
		len1++;
	}
	while(str2[len2]!='\0')
	{
		len2++;
	}

	char* str=(char*)malloc(sizeof(char)*(len1+len2+1));
	for(i=0;i<len1;i++)
	{
		str[i]=str1[i];
	}
	for(i=len1;i<len1+len2;i++)
	{
		str[i]=str2[i-len1];
	}
	str[len1+len2]='\0';
	return str;
}

void gtest()
{
    puts(stract_s("abc","def"));
    puts(stract_s("abc",""));
    puts(stract_s("abc",NULL));
    puts(stract_s(NULL,"abc"));
    puts(stract_s(NULL,NULL));
}

int main()
{
    gtest();
    return 0;
}
