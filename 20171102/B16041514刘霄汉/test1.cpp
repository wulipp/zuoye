#include "stdafx.h"
#include "strcat.h"
#include "gtest/gtest.h"


TEST(strcat, test1) {
	EXPECT_STREQ("abc", strcat("abc", NULL));
}

TEST(strcat, test2) {
	EXPECT_STREQ("abc", strcat(NULL, "abc"));
}

TEST(strcat, test3) {
	EXPECT_STREQ("abc", strcat("abc", ""));
}

TEST(strcat, test4) {
	EXPECT_STREQ("abcdef", strcat("abc", "def"));
}

TEST(strcat, test5) {
	EXPECT_STREQ("", strcat(NULL, NULL));
}

int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
