// strcat.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "gtest\gtest.h"
#include "strctl.h"
#include <assert.h>

TEST(STRCAT, ALL)
{
	EXPECT_STREQ("abcdef", my_strcat("abc", "def"));
	EXPECT_STREQ("abc", my_strcat("abc",""));
	EXPECT_STREQ("abc", my_strcat("abc", NULL));
	EXPECT_STREQ("abc", my_strcat(NULL, "abc"));
	EXPECT_STREQ("", my_strcat(NULL, NULL));
}


int main(int argc, char ** argv)
{
	testing::InitGoogleTest(&argc,argv);
	RUN_ALL_TESTS();

	return 0;
}

