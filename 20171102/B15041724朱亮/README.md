### 1.测试 strcat("abc","def")结果
![img](1.png)

### 2.测试 strcat("abc","")结果
![img](2.png)

### 3.测试 strcat("abc",NULL)结果
![img](./3.png)

### 4.测试 strcat(NULL,"abc")结果
![img](./4.png)

### 5.测试 strcat(NULL,NULL)结果
![img](./5.png)

