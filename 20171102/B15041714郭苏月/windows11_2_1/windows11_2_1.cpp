// windows11_2_1.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include<stdio.h>
#include<malloc.h>
#include<string>
#include<gtest/gtest.h>

unsigned long strlen(char* );
void strcpy(char* , char* );
char* strcat_c(char* , char* );

TEST(widows, strcat_c)
{
	EXPECT_STREQ("abcdef", strcat_c("abc", "def"));
	
}
TEST(widows, strcat_c2)
{
	EXPECT_STREQ("abcdef", strcat_c("ac", "def"));

}


unsigned long strlen(char* str)
{
	unsigned long length = 0;
	while (str[length] != '\0') { length++; }
	return length;
}
void strcpy(char* str1, char* str2)
{
	int i = 0;
	while (str2[i] != '\0')
	{
		str1[i] = str2[i];
		i++;
	}
	str1[i] = '\0';
}
char* strcat_c(char* str1, char* str2)
{
	unsigned long len1, len2;
	len1 = strlen(str1);
	len2 = strlen(str2);
	char* str3 = (char*)malloc(sizeof(char*)*(len1 + len2 + 1));
	strcpy(str3, str1);

	int i = 0;
	while (str3[i] != '\0') { i++; }
	int j = 0;
	while (str2[j] != '\0') { str3[i++] = str2[j++]; }
	str3[i] = '\0';
	return str3;
}
int main(int argc, char *argv[])
{
	strcat_c("abc", "def");

	::testing::InitGoogleTest(&argc, argv);
	RUN_ALL_TESTS();

	 system("pause");
	return 0;
}

