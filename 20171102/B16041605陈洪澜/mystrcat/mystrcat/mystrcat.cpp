// strcat.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <gtest/gtest.h>
#include <iostream>
#include "strcat.h"
using namespace std;


TEST(FooTest, HandleNoneZeroInput)
{
	EXPECT_STREQ("abcdef", mystrcat("abc", "def"));

}

TEST(FooTest, HandleEmptyInput)
{
	EXPECT_STREQ("abc", mystrcat("abc", ""));
}

TEST(FooTest, HandleNULLInput)
{
	EXPECT_STREQ("abc", mystrcat("abc", NULL));
	EXPECT_STREQ("abc", mystrcat(NULL, "abc"));
	EXPECT_STREQ("", mystrcat(NULL, NULL));
}

int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	RUN_ALL_TESTS();
	system("pause");
	return 0;

}

