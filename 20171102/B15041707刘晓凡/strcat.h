#pragma once
#include <iostream>
using namespace std;
char * strcat_s(char *str1, char *str2)
{
	int length1 = 0;
	int length2 = 0;

	if (str1 == NULL && str2==NULL)
	{
		return NULL;
	}

	if (str1== NULL)
	{
		return str2;
	}

	if (str2 == NULL)
	{
		return str1;
	}

	while (str1[length1] != '\0')            //1.计算str1的长度
	{
		length1++;
	}

	while (str2[length2] != '\0')           //计算str2的长度
	{
		length2++;
	}

	int len = length1 + length2;

	char * dest = (char *)malloc(len + 1); //2.申请内存空间

	for (int i = 0; i < length1; i++)     //3.将第一个字符串先复制到申请的内存空间中
	{
		dest[i] = str1[i];
	}

	for (int i = 0; i < length2; i++)    //再将第二个字符串复制到申请的内存空间中
	{
		dest[length1 + i] = str2[i];
	}

	dest[len] = '\0';                   //4.字符串结束标志

	return dest;
}
