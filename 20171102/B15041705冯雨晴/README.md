本次实验运用了GTest测试框架，测试之后才发现需要考虑字符串为NULL和空的情况，就重写了一个字符串长度计算函数，当参数为NULL，字符串长度为0，代码如下：

```
int myStrlen(char * str) {
	if (str == NULL || str == "") {
		return 0;
	}
	int i = 0;
	while (str[i] != '\0') {
		i++;
	}
	return i;
}
```
刚开始测试时运用了EXPECT_EQ断言，发现比较的是字符串的地址，就去查了一下资料，发现了EXPECT_STREQ这个断言，适用于字符串比较；

![输入图片说明](https://gitee.com/uploads/images/2017/1108/231657_fa9a1f07_1074275.png "3.png")

ASSERT类断言与EXPECT类断言的区别在assert是运行如果出错会停止运行，而expect则是出错依然运行；
最终测试结果如下图：

![输入图片说明](https://gitee.com/uploads/images/2017/1108/231635_12410504_1074275.png "1.png")
