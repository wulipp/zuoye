// stdafx.cpp : 只包括标准包含文件的源文件
// ConsoleApplication1.pch 将作为预编译标头
// stdafx.obj 将包含预编译类型信息

#include "stdafx.h"
#include<stdlib.h>
#include "malloc.h"
// TODO: 在 STDAFX.H 中引用任何所需的附加头文件，
//而不是在此文件中引用
#include <gtest/gtest.h>

using namespace std;
/*int main() {
char *str1 = "abc";
char *str2 = "def";
char *str3 = "abc";
char *str4 = "";
char *str5 = "abc";
char *str6 = NULL;
char *str7 = NULL;
char *str8 = "abc";
char *str9 =NULL;
char *str10 =NULL;
strcat(str1 , str2);
cout << '\n';
strcat(str3, str4);
cout << '\n';

strcat(str5, str6);
cout << '\n';
strcat(str7, str8);
cout << '\n';
strcat(str9, str10);
cout << '\n';
system("pause");
return 0;


}*/
TEST(testCase, test0)
{
	EXPECT_STREQ("abcdef", strcat1("abc", "def"));
}

TEST(testCase, test1)
{
	EXPECT_STREQ("abc", strcat1("abc", ""));
}

TEST(testCase, test2)
{
	EXPECT_STREQ("abc", strcat1("abc", NULL));
}

TEST(testCase, test3)
{
	EXPECT_STREQ("abc", strcat1(NULL, "abc"));
}

TEST(testCase, test4)
{
	EXPECT_STREQ(NULL, strcat1(NULL, NULL));
}


int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

