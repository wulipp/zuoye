// ConsoleApplication1.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "strcatString.h"
#include "gtest/gtest.h"

TEST(strcatString, strcatString1)
{
	EXPECT_STREQ("abcdef", strcatString("abc", "def"));
	//EXPECT_STREQ("abc", strcatString("abc", ""));
	//EXPECT_STREQ("abc", strcatString("abc", NULL));
	//EXPECT_STREQ("abc", strcatString(NULL, "abc"));
	//EXPECT_STREQ(NULL, strcatString(NULL, NULL));
}
TEST(strcatString, strcatString2)
{
	//EXPECT_STREQ("abcdef", strcatString("abc", "def"));
	EXPECT_STREQ("abc", strcatString("abc", ""));
	//EXPECT_STREQ("abc", strcatString("abc", NULL));
	//EXPECT_STREQ("abc", strcatString(NULL, "abc"));
	//EXPECT_STREQ(NULL, strcatString(NULL, NULL));
}


int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}



