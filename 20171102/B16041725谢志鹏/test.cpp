#include "gtest/gtest.h"  
#include  "strafx.h"
#include <tchar.h>  

TEST(FootTest,HandleNoneZeroInput)
{
	EXPECT_STREQ("abcdef",stract("abc","def"));
	EXPECT_STREQ("abc", stract("abc", ""));
	EXPECT_STREQ("abc", stract("abc", NULL));
	EXPECT_STREQ("abc", stract(NULL, "abc"));
	EXPECT_STREQ(NULL, stract(NULL,NULL));
}

int _tmain(int argc, _TCHAR* argv[])
{	

	testing::InitGoogleTest(&argc, argv);
	RUN_ALL_TESTS();
	system("PAUSE");
	return 0;
}