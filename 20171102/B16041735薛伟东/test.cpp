#include "pch.h"


char *mystrcat(char *str1, char *str2)
{
	int lstr1 = 0, lstr2 = 0;
	if (str1 != NULL)
	{
		while (str1[lstr1] != '\0')
		{
			lstr1++;
		}
	}
	if (str2 != NULL)
	{
		while (str2[lstr2] != '\0')
		{
			lstr2++;
		}
	}
	if (lstr1 == 0)
	{
		char *p = new char[lstr2 + 1];
		for (int i = 0; i<lstr2; ++i)
		{
			p[i] = str2[i];
		}
		p[lstr2] = '\0';
		return p;
	}
	else if (lstr2 == 0)
	{
		char *p = new char[lstr1 + 1];
		for (int i = 0; i<lstr1; ++i)
		{
			p[i] = str1[i];
		}
		p[lstr1] = '\0';
		return p;
	}
	else if (lstr1 == 0 && lstr2 == 0)
	{
		char *p = new char;
		*p = '\0';
		return p;
	}
	else
	{
		char *p = new char[lstr1 + lstr2 + 1];
		for (int i = 0; i<lstr1; ++i)
		{
			p[i] = str1[i];
		}
		for (int i = 0; i<lstr2; ++i)
		{
			p[lstr1 + i] = str2[i];
		}
		p[lstr1 + lstr2] = '\0';
		return p;
	}
}

TEST(mystrcat, s1) {
	EXPECT_STREQ(mystrcat("abc", "def"), "abcdef");
}

TEST(mystrcat, s2) {
	EXPECT_STREQ(mystrcat("abc", ""), "abc");
}

TEST(mystrcat, s3) {
	EXPECT_STREQ(mystrcat("abc",NULL), "abc");
}

TEST(mystrcat, s4) {
	EXPECT_STREQ(mystrcat(NULL, "abc"), "abc");
}
TEST(mystrcat, s5) {
	EXPECT_STREQ(mystrcat(NULL, NULL), "");
}


int main(int argc, char* argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}