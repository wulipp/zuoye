#include "stdafx.h"
#include<gtest/gtest.h>

char* Strcat(char* str1, char* str2)
{
	if (str1 == NULL&&str2 == NULL)
	{
		return NULL;
	}
	if (str1 == NULL)
		return str2;
	if (str2 == NULL)
		return str1;
	char* ch;
	int i;
	int n1 = 0;
	int n2 = 0;
	while (str1[n1] != '\0')
	{
		n1++;
	}

	while (str2[n2] != '\0')
	{
		n2++;
	}
	ch = (char*)malloc(sizeof(char)*(n1 + n2+1));
	for (i = 0; i < n1; i++)
	{
		ch[i] = str1[i];
	}
	for (i = n1; i < n1 + n2; i++)
	{
		ch[i] = str2[i - n1];
	}
	ch[n1 + n2] = '\0';
	return ch;
}
TEST(FootTest, HandleNoneZeroInput)
{
	EXPECT_STREQ("abcdef", Strcat("abc", "def"));
	EXPECT_STREQ("abc", Strcat("abc", ""));
	EXPECT_STREQ("abc", Strcat("abc", NULL));
	

}


int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	 RUN_ALL_TESTS();
	 int n;
	 cin >> n;
	 return 0;

}
