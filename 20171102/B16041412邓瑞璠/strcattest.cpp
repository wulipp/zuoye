// strcattest.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <gtest\gtest.h>


TEST(strcat, Test)
{
	EXPECT_STREQ("abcdef", mystrcat("abc", "def"));
	EXPECT_STREQ("abc", mystrcat("abc", ""));
	EXPECT_STREQ("abc", mystrcat("abc", NULL));
	EXPECT_STREQ("abc", mystrcat(NULL, "abc"));
	EXPECT_STREQ("", mystrcat(NULL, NULL));
}

int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	RUN_ALL_TESTS();
	system("PAUSE");
	return 0;
}

