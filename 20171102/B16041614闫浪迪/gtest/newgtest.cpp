// newgtest.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "strcat1.h"
#include "gtest/gtest.h"

#ifdef _DEBUG  
#pragma comment(lib, "G:\\googletest-master\\googletest\\msvc\\gtest\\Win32-Debug\\gtestd.lib")  
#pragma comment(lib, "G:\\googletest-master\\googletest\\msvc\\gtest\\Win32-Debug\\gtest_maind.lib")  
#else  
#pragma comment(lib, "G:\\googletest-master\\googletest\\msvc\\gtest\\Win32-Release\\gtest.lib")  
#pragma comment(lib, "G:\\googletest-master\\googletest\\msvc\\gtest\\Win32-Release\\gtest_main.lib")   
#endif


TEST(strcat1, case1)
{
	EXPECT_STREQ("abcdef", strcat1("abc", "def"));
	EXPECT_STREQ("abc", strcat1("abc", ""));
	EXPECT_STREQ("abc", strcat1("abc", NULL));
	EXPECT_STREQ("abc", strcat1(NULL, "abc"));
	EXPECT_STREQ("", strcat1(NULL, NULL));
	//strcat1(NULL, NULL)
}
int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

