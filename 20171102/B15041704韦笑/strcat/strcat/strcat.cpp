// strcat.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"
char *strcat1(char *str1, char *str2)
{
	int len1 = 0;
	int len2 = 0;
	int i;
	int j;
	if (str1 == NULL || str2 == NULL)
	{
		if (str1 == NULL &&str2 == NULL)
		{
			return NULL;
		}
		else if (str1 == NULL)
		{
			return str2;
		}
		else
		{
			return str1;
		}
	}
	while (str1[len1] != '\0')
	{
		len1++;
	}
	while (str2[len2] != '\0')
	{
		len2++;
	}
	char *str3 = (char*)malloc(sizeof(char)*(len1 + len2 + 1));

	for (i = 0; i < len1; i++)
	{
		str3[i] = str1[i];
	}
	for (j = 0; j < len2; j++)
	{
		str3[j + len1] = str2[j];
	}
	str3[len1 + len2] = '\0';
	return str3;
}

TEST(FooTest, HandleNoneZeroInput)
{
	EXPECT_STREQ("abcdef", strcat1("abc", "def"));
	EXPECT_STREQ("abc", strcat1("abc", ""));
	EXPECT_STREQ("abc", strcat1("abc", NULL));
	EXPECT_STREQ("abc", strcat1(NULL, "abc"));
	EXPECT_STREQ(NULL, strcat1(NULL, NULL));
}
int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}


