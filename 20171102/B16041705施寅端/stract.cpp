#include "stdafx.h"
#include <iostream>
using namespace std;

char* strcats(char* a, char* b) {
	int lenA = 0;
	if (a != NULL) {
		while (a[lenA] != '\0') {
			lenA += 1;
		}
	}
	int lenB = 0;
	if (b != NULL) {
		while (b[lenB] != '\0') {
			lenB += 1;
		}
	}
	//cout << lenA << " " << lenB << endl;
	char* res = new char[lenA + lenB + 1];
	for (int i = 0; i < lenA; ++i) {
		res[i] = a[i];
	}
	for (int i = lenA, j = 0; j < lenB; ++i, ++j) {
		res[i] = b[j];
	}
	res[lenA + lenB] = '\0';
	if (lenA + lenB > 0) {
		return res;
	}
	else {
		return NULL;
	}
}