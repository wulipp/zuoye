#pragma once
// gtest_demo.cpp : 定义控制台应用程序的入口点。
//
#pragma comment (lib, "gtestd.lib")

#include "stdafx.h"
#include <gtest/gtest.h>
#include "stracth.h"


TEST(strcatTest, HandleNoneZeroInput)
{
	EXPECT_STREQ("abcdef", strcat_s("abc", "def"));
	EXPECT_STREQ("abc", strcat_s("abc", ""));
	EXPECT_STREQ("abc", strcat_s("abc", NULL));
	EXPECT_STREQ("abc", strcat_s(NULL, "abc"));
	EXPECT_STREQ(NULL, strcat_s(NULL, NULL));
}

int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

