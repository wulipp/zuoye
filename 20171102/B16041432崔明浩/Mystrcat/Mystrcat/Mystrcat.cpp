// Mystrcat.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"

char * mystrcat(char * str1, char * str2)
{
	int len1 = 0;
	int len2 = 0;
	int i;
	int cur=0;

	if (str1 == NULL)
		len1 = 0;
	else
	{
		for (i = 0; str1[i] != '\0'; i++);
		len1 = i;
	}

	if (str2 == NULL)
		len2 = 0;
	else
	{
		for (i = 0; str2[i] != '\0'; i++);
		len2 = i;
	}

	char * pstr = (char *)malloc(sizeof(char) * (len1+len2) + 1);

	if(str1 != NULL)
		for (i = 0; i[str1] != '\0'; i++)
			pstr[cur++] = str1[i];
	if(str2 != NULL)
		for (i = 0; i[str2] != '\0'; i++)
			pstr[cur++] = str2[i];
	pstr[cur] = '\0';

	return pstr;
}

TEST(FooTest, HandleNoneZeroInput)
{
	EXPECT_STREQ("abcdef", mystrcat("abc", "def"));
	EXPECT_STREQ("abc", mystrcat("abc", ""));
	EXPECT_STREQ("abc", mystrcat("abc", NULL));
	EXPECT_STREQ("abc", mystrcat(NULL, "abc"));
	EXPECT_STREQ("", mystrcat(NULL, NULL));
}

int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	RUN_ALL_TESTS();
	getchar();
	return 0;
}
