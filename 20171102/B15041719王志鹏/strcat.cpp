// strcat.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <gtest/gtest.h>

char* strcat_(char* str1, char* str2) {
	if (str1 == NULL && str2 == NULL) {
		return NULL;
	}
	else if (str1 == NULL) {
		return str2;
	}
	else if (str2 == NULL) {
		return str1;
	}

	int length1 = 0;
	int length2 = 0;
	while (str1[length1] != '\0')
	{
		length1++;
	}

	while (str2[length2] != '\0')
	{
		length2++;
	}

	char* result = (char *)malloc(length1 + length2 + 1);
	for (int i = 0; i < length1; i++)
	{
		result[i] = str1[i];
	}
	for (int i = 0; i < length2; i++)
	{
		result[length1 + i] = str2[i];
	}

	result[length1 + length2] = '\0';

	return result;

}

TEST(testCase, test0)
{
	EXPECT_STREQ("abcdef", strcat_("abc", "def"));
}

TEST(testCase, test1)
{
	EXPECT_STREQ("abc", strcat_("abc", ""));
}

TEST(testCase, test2)
{
	EXPECT_STREQ("abc", strcat_("abc", ""));
}
TEST(testCase, test3)
{
	EXPECT_STREQ("abc", strcat_(NULL, "abc"));
}
TEST(testCase, test4)
{
	EXPECT_STREQ("abc", strcat_("abc", NULL));
}

int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

