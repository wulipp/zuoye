#include<stdio.h>
#include<stdlib.h>
char *strcat(char *str1,char *str2)
{
    int m=0,n=0,i;
    if (str1 == NULL&&str2 == NULL)
		return NULL;
	if (str1 == NULL)
		return str2;
	if (str2 == NULL)
		return str1;
    while(str1[m]!='\0')
    {
        m++;
    }
    while(str2[n]!='\0')
    {
        n++;
    }
    char *str3;
    str3=(char*)malloc(sizeof(char)*(m+n+1));
    for(i=0;i<m;i++)
        str3[i]=str1[i];
    for(i=m;i<m+n;i++)
        str3[i]=str2[i-m];
    str3[m+n]='\0';
    return str3;
}
void gtest()
{
    puts(strcat("abc","def"));
    puts(strcat("abc",""));
    puts(strcat("abc",NULL));
    puts(strcat(NULL,"abc"));
    puts(strcat(NULL,NULL));
}
int main()
{
    gtest();
    return 0;
}
