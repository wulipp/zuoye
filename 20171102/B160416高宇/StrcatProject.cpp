// StrctProject.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include<string.h>


char* myStrcat(char* str1, char* str2) {
	int m, n , i, j;
	int len1 = 0, len2 = 0;
	if (str1 != NULL) {
		for (m = 0; str1[m] != '\0'; m++) {
			len1++;
		}
	}
	if (str2 != NULL) {
		for (n = 0; str2[n] != '\0'; n++) {
			len2++;
		}
	}
	char *p = (char*)malloc((len1 + len2 + 1) * sizeof(char));
	for (i = 0; i < len1; i++) {
		p[i] = str1[i];
	}
	for (j = 0; j < len2; j++) {
		p[i+j] = str2[j];
	}
	p[i + j] = '\0';
	return p;
}

void gtest() {
	printf("%s\n", myStrcat("abc", "def"));
	printf("%s\n", myStrcat("abc", ""));
	printf("%s\n", myStrcat("abc", NULL));
	printf("%s\n", myStrcat(NULL, "abc"));
	printf("%s\n", myStrcat(NULL, NULL));

}

int main()
{
	gtest();
	system("pause");
    return 0;
}

