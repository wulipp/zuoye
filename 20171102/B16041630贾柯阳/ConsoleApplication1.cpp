// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <gtest/gtest.h>

int getStrlen(char *str) 
{
	int index = 0;
	if (str == NULL)
		return 0;
	while (str[index] != '\0')
		index++;
	return index;
}

char *strcat(char *str1,char *str2)
{
	int m, i, j;
	char *p;
	if (str1 == NULL && str2 == NULL)
		return NULL;
	i = getStrlen(str1);
	j = getStrlen(str2);
	p = (char*)malloc(sizeof(char)*(i+j+1));

	for (m = 0; m < i; m++)
		p[m] = str1[m];
	for (m = 0; m < j; m++)
		p[i + m] = str2[m];
	p[i + j + 1] = '\0';
	return p;
}

TEST(FooTest, HandleNoneZeroInput)
{
	char *p = "abc",*q = "def",*s = "";
	EXPECT_STREQ(p, strcat(p, q));
	EXPECT_STREQ(p, strcat(p, s));

	s = NULL;
	EXPECT_STREQ(p, strcat(p, s));
	EXPECT_STREQ(q, strcat(s, q));
	EXPECT_STREQ(s, strcat(s, s));
}


int main(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

