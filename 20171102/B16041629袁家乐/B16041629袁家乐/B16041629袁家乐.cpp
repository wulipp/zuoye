// B16041629袁家乐.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gtest/gtest.h>
int my_strlen(char* s)
{
	int lenth = 0;
	if (!s)
		return lenth;
	while (*s != '\0')
	{
		lenth++;
		*s++;
	}
	return lenth;
}
char* my_strcat(char* str1, char* str2)
{
	char* str, *p;
	int lenth1 = 0, lenth2 = 0;
	if (!str2&&str1)
		return str1;
	else if (!str1&&str2)
	{
		str1 = str2;
		return str1;
	}
	else if (!str1 && !str2)
		return str1;
	lenth1 = my_strlen(str1);
	lenth2 = my_strlen(str2);
	str = (char*)malloc(sizeof(char)*(lenth1 + lenth2 + 1));
	p = str;
	while (*str1 != '\0')
	{
		*p++ = *str1++;
	}
	while (*str2 != '\0')
	{
		*p++ = *str2++;
	}
	*p = '\0';
	str1 = str;
	return str1;
}
TEST(FooTest, HandleNoneZeroInput)
{
	char *str1 = NULL;
	char *str2 = NULL;
	char *str = NULL;
	str1 = "abc";
	str2 = "def";
	str = "abcdef";
	str1 = my_strcat(str1, str2);
	EXPECT_STRCASEEQ(str, str1);
	str1 = "abc";
	str2 = "";
	str = "abc";
	str1 = my_strcat(str1, str2);
	EXPECT_STRCASEEQ(str, str1);
	str1 = "abc";
	str2 = NULL;
	str = "abc";
	str1 = my_strcat(str1, str2);
	EXPECT_STRCASEEQ(str, str1);
	str1 = NULL;
	str2 = "abc";
	str = "abc";
	str1 = my_strcat(str1, str2);
	EXPECT_STRCASEEQ(str, str1);
	str = str1 = str2 = NULL;
	str1 = my_strcat(str1, str2);
	EXPECT_STRCASEEQ(str, str1);
}
int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

