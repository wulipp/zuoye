// ConsoleApplication1.cpp: 定义控制台应用程序的入口点。
//
#include"stdafx.h"
#include <gtest/gtest.h>
using namespace std;

char* strcat1(char* str1, char* str2)
{
	int i, j, x, n;
	char *s1 = str1, *s2 = str2;
	char *tmp;
	if (s1 != NULL)
		i = strlen(s1);
	else
		i = 0;
	if(s2!=NULL)
		j = strlen(s2);
	else
		j = 0;
	n = i + j;
	tmp = (char*)malloc(sizeof(char)*n);
	for (x = 0; x<n; x++)
	{
		if (x<i)
			tmp[x] = s1[x];
		else
			tmp[x] = s2[x - i];
	}
	tmp[x] = '\0';
	return tmp;
}
TEST(FooTest, HandleNoneZeroInput)
{
	ASSERT_STRCASEEQ("abcdef", strcat1("abc", "def"));
	ASSERT_STRCASEEQ("abc", strcat1("abc", ""));
	ASSERT_STRCASEEQ("abc", strcat1("abc", NULL));
	ASSERT_STRCASEEQ("abc", strcat1(NULL, "abc"));
	ASSERT_STRCASEEQ("", strcat1(NULL, NULL));
}
int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	system("pause");
	return RUN_ALL_TESTS();
}

