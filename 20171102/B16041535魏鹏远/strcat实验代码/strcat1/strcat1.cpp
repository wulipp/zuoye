// strcat1.cpp: 定义控制台应用程序的入口点。

//分别计算str1和str2的长度
//申请内存空间
//逐个字符复制到申请的内存空间中
//字符串结束标记写入
#include<iostream>
#include<stdlib.h>
using namespace std;
char* strcat1(char* str1, char* str2);
int main()
{
	char s1[20] = "abc";
	char s2[20] = "def";
	char *tmp1;
	tmp1 = strcat1(s1, s2);
	cout << "字符串1为："<<s1<<endl;
	cout << "字符串2为：" << s2 << endl;
	cout << "将字符串1和字符串2连接后为："<<tmp1 << endl;
	system("pause");
	return 0;
}
char* strcat1(char* str1, char* str2)
{
	int i, j, x, n;
	char *s1 = str1, *s2 = str2;
	char *tmp;
	i = strlen(s1);
	j = strlen(s2);
	n = i + j;
	tmp = (char*)malloc(sizeof(char)*n);
	for (x = 0; x<n; x++)
	{
		if (x<i)
			tmp[x] = s1[x];
		else
			tmp[x] = s2[x - i];
	}
	tmp[x] = '\0';
	return tmp;
}


