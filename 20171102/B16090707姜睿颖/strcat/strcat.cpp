// strcat.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "stract.h"

TEST(strcat, mystrcat)
{
	EXPECT_STREQ("abcdef", mystrcat("abc", "def"));
	EXPECT_STREQ("abc", mystrcat("abc", ""));
	EXPECT_STREQ("abc", mystrcat("abc", NULL));
	EXPECT_STREQ("abc", mystrcat(NULL, "abc"));
}

int main(int argc, char* argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}