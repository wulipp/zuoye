    学会了gtest测试的使用方法，gtest的设置比较繁琐但是用gtest进行测试比较方便快捷，也直观。
    因为是比较字符串类型，所以要用到EXPECT_STREQ断言。
    在定义字符串拼接函数的时候不能定义成strcat这个函数名，因为c语言自带库的函数与它重名会出现重载的情况，需要把这个函数名改掉。
    第一次测试的时候没有对str1和str2等于NULL的情况进行讨论，所以测试结果有失败的情况。
    加上if (str1 == NULL || str2 == NULL)
	{
		if (str1 == NULL&&str2 == NULL)
			return NULL;
		else if (str1 == NULL)
			return str2;
		else
			return str1;
	}这段代码之后，测试可以完全通过。
