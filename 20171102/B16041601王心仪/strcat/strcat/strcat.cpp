// strcat.cpp: 定义控制台应用程序的入口点。
//

#pragma once
#include"stdafx.h"

char *mystrcat(char *str1, char *str2)
{
	int length1 = 0, length2 = 0;
	while (str1&&str1[length1] != '\0')
	{
		length1++;
	}
	while (str2&&str2[length2] != '\0')
	{
		length2++;
	}
	char*a = (char*)malloc(sizeof(char)*(length1+length2+1));

	for (int i = 0; i<length1 - 1; i++)
	{
		if (i < length1)
		{
			a[i] = str1[i];
		}
		else
			a[i] = str2[i - length1];
	}
	a[length1+length2] = '\0';
	return a;

}
int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

TEST(FooTest, HandleNoneZeroInput)
{
	EXPECT_EQ("abcdef", mystrcat("abc", "def"));
	EXPECT_EQ("abc", mystrcat("abc", ""));
	EXPECT_EQ("abc", mystrcat("abc", NULL));
	EXPECT_STREQ("abc", mystrcat(NULL, "abc"));
	EXPECT_STREQ(NULL, mystrcat(NULL, NULL));
}
