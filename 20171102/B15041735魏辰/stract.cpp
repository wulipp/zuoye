﻿// stract.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include<gtest\gtest.h>

//char* myStrcat(char* str1, char* str2);

/*int main()
{
	char str1[] = "abc";
	char str2[] = "def";
	char* text = myStrcat(str1,str2);
	printf(text);
	std::cout << "\n";
	system("pause");
    return 0;
}*/

char* myStrcat(char* str1, char* str2) {
	if (str1 == NULL) {
		if (str2 == NULL)
		{
			return "NULL";
		}
		else
		{
			return str2;
		}
	}
	else
	{
		if (str2 == NULL)
		{
			return str1;
		}
	}

	int L1 = 0, L2 = 0;

	int size = 0;
	while (str1[L1] != '\0')
	{
		L1++;
	}

	while (str2[L2] != '\0')
	{
		L2++;
	}

	size = L1 + L2;

	char* temp = (char*)malloc(size+1);

	for (  int i = 0 ; i < L1; i++)
	{
		temp[i] = str1[i];
	}

	for (int j = 0 ; j < L2 ; j ++ ) {
		temp[j + L1] = str2[j];
	}

	temp[size] = '\0';

	return temp;
}

TEST(testCase, test0)
{
	EXPECT_STREQ("abcdef", myStrcat("abc", "def"));
}

TEST(testCase, test1)
{
	EXPECT_STREQ("abc", myStrcat("abc", ""));
}

TEST(testCase, test2)
{
	EXPECT_STREQ("abc", myStrcat("abc", NULL));
}

TEST(testCase, test3)
{
	EXPECT_STREQ("abc", myStrcat(NULL, "abc"));
}

/*TEST(testCase, test4)
{
	EXPECT_STREQ(NULL, myStrcat(NULL, NULL));
}*/


int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}


