#include "stdafx.h"
#include <iostream>
#include <string.h> 
#include<gtest/gtest.h>
using namespace std;
char* stract(char* str1, char* str2)
{
	int len1 = strlen(str1);
	int len2 = strlen(str2);
	char *p;
	if(len1==0||len2==0)
	{
		if(len1==0&&len2==0)
		return null;
		else
		if(len1==0&&len2!=0)
		return str1;
		else
		if(len1!=0&&len2==0)
		return str2;
	 } 
	p = new char[len1 + len2];
	for (int i = 0; i<len1; i++)
	{
		p[i] = str1[i];
	}
	for (int i = 0; i<len2; i++)
	{
		p[i + len1] = str2[i];
	}
	return p;
}
TEST(FooTest, HandleNoneZeroInput)
{
	EXPECT_STREQ("abcdef", stract("abc", "def"));
	EXPECT_STREQ("abc", stract("abc", ""));
	EXPECT_STREQ("abc", stract("abc", NULL));
	EXPECT_STREQ("abc", stract(NULL, "abc"));
	EXPECT_STREQ(NULL, stract(NULL, NULL));
}
int _tmain(int argc, _TCHAR * argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

