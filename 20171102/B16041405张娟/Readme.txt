#include "stdafx.h"
#include<stdio.h>
#include<stdlib.h>
#include<gtest/gtest.h>
char * stract(char *str1, char *str2)
{
	int m = 0, n = 0, i, j;
	char *str;
	if (str1 == NULL || str2 == NULL)
	{
		if (str1 == NULL&&str2 == NULL)
			return NULL;
		else if (str1 == NULL)
			return str2;
		else
			return str1;
	}
	while (str1[m] != NULL)
		m++;
	while (str2[n] != NULL)
		n++;
	str = (char *)malloc(sizeof(char)*(m + n + 1));
	for (i = 0; i<m; i++)
		str[i] = str1[i];
	for (j = 0; j<n; j++)
		str[j + m] = str2[j];
	str[m + n] = '\0';
	return str;
}
TEST(FooTest, HandleNoneZeroInput)
{
	EXPECT_STREQ("abcdef", stract("abc", "def"));
	EXPECT_STREQ("abc", stract("abc", ""));
	EXPECT_STREQ("abc", stract("abc", NULL));
	EXPECT_STREQ("abc", stract(NULL, "abc"));
	EXPECT_STREQ(NULL, stract(NULL, NULL));
}
int _tmain(int argc, _TCHAR * argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
