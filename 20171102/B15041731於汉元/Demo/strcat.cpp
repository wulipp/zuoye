// Demo.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "malloc.h"
#include <iostream>
#include "gtest\gtest.h"
using namespace std;
char *strcatmm(char *str1, char *str2) {
	if (str1 == NULL&&str2 != NULL) {
		return str2;
	}
	if (str1 != NULL&&str2 == NULL) {
		return str1;
	}
	if (str1 == NULL&&str2 == NULL) {
		return NULL;
	}
	int str1len = 0;
	int str2len = 0;
	while (str1[str1len] != '\0') {
		str1len++;
	}
	while (str2[str2len] != '\0') {
		str2len++;
	}
	char * str = (char *) malloc (str1len + str2len + 1);
	for (int i = 0; i < str1len;i++) {
		str[i] = str1[i];
	}
	for (int i = 0; i < str2len; i++) {
		str[i+str1len] = str2[i];
	}
	str[str1len + str2len] = '\0';
	return str;

}

TEST(testCase, test0)
{
	EXPECT_STREQ("abcdef", strcatmm("abc", "def"));
}
TEST(testCase, test1)
{
	EXPECT_STREQ("abc", strcatmm("abc", ""));
}

TEST(testCase, test2)
{
	EXPECT_STREQ("abc", strcatmm("abc", NULL));
}

TEST(testCase, test3)
{
	EXPECT_STREQ("abc", strcatmm(NULL, "abc"));
}

TEST(testCase, test4)
{
	EXPECT_STREQ(NULL, strcatmm(NULL, NULL));
}

int _tmain(int argc, _TCHAR* argv[]) {
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

