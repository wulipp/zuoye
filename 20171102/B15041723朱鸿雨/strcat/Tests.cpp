#include"str_lib.h"
#include"gtest\gtest.h"
TEST(TEST_STRCAT, strcat)
{
	char * str1 = "abc";
	char * str2 = "def";
	char * str3 = "abcdef";
	char * str4 = "";
	char * str_null = NULL;

	EXPECT_EQ(*str3, *strcat(str1, str2));			// "abc" + "def" = "abcdef"
	EXPECT_EQ(*str1, *strcat(str1,str4));			// "abc" + "" = "abc"
	EXPECT_EQ(*str1, *strcat(str1,str_null));       // "abc" + NULL = "abc"
	EXPECT_EQ(*str1, *strcat(str_null,str1));       // NULL + "abc" = "abc"
	EXPECT_EQ(str_null, strcat(str_null,str_null)); // NULL + NULL = NULL
}

int main(int argc, char* argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	int result = RUN_ALL_TESTS();
	system("pause");
	return result;
}