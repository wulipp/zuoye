#include "myStrcat.h"

#include <memory>

char* strcat(char* str1, char* str2)
{
	if (str1 == nullptr && str2 == nullptr)
	{
		return nullptr;
	}

	int len1 = 0; 
	if (str1 != nullptr)
	{
		while (str1[len1] != '\0')
		{
			++len1;
		}
	}

	int len2 = 0;
	if (str2 != nullptr) 
	{
		while (str2[len2] != '\0')
		{
			++len2;
		}
	}

	char* ans = (char *)malloc(len1 + len2 + 1);
	for (int i = 0; i < len1; ++i)
	{
		ans[i] = str1[i];
	}

	for (int i = 0; i < len2; ++i)
	{
		ans[len1 + i] = str2[i];
	}

	ans[len1 + len2] = '\0';

	return ans;
}