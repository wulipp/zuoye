// ConsoleApplication1.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "myStrcat.h"
#include <gtest/gtest.h>

TEST(testCase, test1)
{
	EXPECT_STREQ("abcdef", strcat("abc", "def"));
}

TEST(testCase, test2)
{
	EXPECT_STREQ("abc", strcat("abc", ""));
}

TEST(testCase, test3)
{
	EXPECT_STREQ("abc", strcat("abc", NULL));
}

TEST(testCase, test4)
{
	EXPECT_STREQ("abc", strcat(NULL, "abc"));
}

TEST(testCase, test5)
{
	EXPECT_STREQ(NULL, strcat(NULL, NULL));
}

int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

