#include <malloc.h>

char* strcat1(char* str1, char* str2)
{
	char* str3;
	int i, cout1 = 0, cout2 = 0;
	if (str1 == NULL)
		cout1 = 0;
	else
		for (i = 0; str1[i] != NULL; i++)
			cout1++;
	if (str2 == NULL)
		cout2 = 0;
	else
		for (i = 0; str2[i] != NULL; i++)
			cout2++;
	int cout = cout1 + cout2;
	if (cout == 0)
		return NULL;
	str3 = (char *)malloc(sizeof(char)*(cout1 + cout2 + 1));
	char *temp = str3;
	for (i = 0; i < cout1; i++)
		*temp++ = str1[i];
	for (i = 0; i < cout2; i++)
		*temp++ = str2[i];
	*temp = '\0';
	return str3;
}