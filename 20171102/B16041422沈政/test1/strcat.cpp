// stdafx.cpp : 只包括标准包含文件的源文件
// test1.pch 将作为预编译标头
// stdafx.obj 将包含预编译类型信息


#include "stdafx.h"

char* strcat_s(char* str1, char* str2)
{
	if (str1 == NULL || str2 == NULL) {
		printf("the input string is error!\n");
		return NULL;
	}
	int j = 0, i = 0;
	while (str1[i] != '\0') i++;
	while (str2[j] != '\0') j++;
	i += j;
	char *str = (char*)malloc(sizeof(char)*(i + 1));
	while (*str1 != '\0') *str++ = *str1++;
	while (*str2 != '\0') *str++ = *str2++;
	*str = '\0';
	for (; i>0; i--)
		*str--;
	return str;
}


// TODO: 在 STDAFX.H 中引用任何所需的附加头文件，
//而不是在此文件中引用
