
#ifndef STRCAT_H_
#define STRCAT_H_


char* my_strcat(char* str1, char* str2)
{
	int m = 0, n = 0, i = 0;
	while (str1 && str1[m] != '\0')
	{
		m++;
	}
	while (str2 && str2[n] != '\0')
	{
		n++;
	}
	char* newstr = (char*)malloc(sizeof(char)*(m + n + 1));
	for (i = 0; i < m; i++)
	{
		newstr[i] = str1[i];
	}
	for (i = 0; i < n; i++)
	{
		newstr[m + i] = str2[i];
	}
	newstr[m + n] = '\0';
	return newstr;
}


#endif /* STRCAT_H_ */
