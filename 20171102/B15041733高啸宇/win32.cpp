﻿// win32.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "resource.h"

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;	//current instance		//HINSTANCE 是“句柄型”数据类型。相当于装入到了内存的资源的ID。HINSTANCE对应的资源是instance	
TCHAR szTitle[MAX_LOADSTRING];								// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];								// The title bar text

// Foward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance); //MyRegisterClass定义窗口变量并利用RegisterClass函数注册
//InitInstance函数的返回值为BOOL类型，所谓的BOOL（布尔）类型，在C语言中就是int，它实际就是一个宏定义 #define int BOOL
//如果初始化成功，则返回非零值；否则返回0。
BOOL				InitInstance(HINSTANCE, int);//InitInstance调用CreatWindow创建窗口，创建成功，则显示和更新窗口，若InitInstance返回true，启动消息循环，否则退出
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);//窗口函数
LRESULT CALLBACK	About(HWND, UINT, WPARAM, LPARAM);//About对话框窗口函数

//WinMain函数有三个部分组成：函数说明，初始化（窗口类的定义，注册窗口类RegisterClass，创建窗口CreatWindow,显示窗口ShowWindow），消息循环
int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
 	// TODO: Place code here.
	MSG msg; //声明消息结构对象;
	HACCEL hAccelTable;

	// 加载全局字符串。 Initialize global strings
	//调用函数LoadString从资源里获取显示的字符串,所有可变的字符串，都可以使用函数LoadString从资源里加载字符串显示
	//将hInstance当前实例中的资源文件中的IDS_APP_TITLE定义的字符放在szTitle中
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_WIN32, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);//注册窗口

	// Perform application initialization://创建窗口,初始化
	if (!InitInstance (hInstance, nCmdShow)) 
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDC_WIN32);

	// Main message loop://消息循环
	while (GetMessage(&msg, NULL, 0, 0)) 
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) 
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage is only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX); 

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= (WNDPROC)WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, (LPCTSTR)IDI_WIN32);
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= (LPCSTR)IDC_WIN32;
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, (LPCTSTR)IDI_SMALL);

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HANDLE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, unsigned, WORD, LONG)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//窗口函数,处理窗口
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	TCHAR szHello[MAX_LOADSTRING];
	LoadString(hInst, IDS_HELLO, szHello, MAX_LOADSTRING);

	switch (message) 
	{
		case WM_COMMAND://表明为命令消息，即为单击菜单，工具栏，或者加速键发出的消息，需要继续辨别消息的附加参数wParam和IParam
			//wParam的高16位和IParam为0，wParam的低16位为命令的ID 号
			wmId    = LOWORD(wParam); 
			wmEvent = HIWORD(wParam); 
			// Parse the menu selections:
			switch (wmId)
			{
				//wParam的低16位为IDM_ABOUT,表明选中了菜单的“About”项
				case IDM_ABOUT:
					//调用DialogBox函数创建和显示About对话框
				   DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);
				   break;
				case IDM_EXIT://退出
				   DestroyWindow(hWnd);//终止对话框显示
				   break;
				default:
					//系统的默认消息处理函数,该函数确保每一个消息得到处理
					//hWnd：指向接收消息的窗口过程的句柄。Msg：指定消息类型。wParam：指定其余的、消息特定的信息。该参数的内容与Msg参数值有关。
					//IParam：指定其余的、消息特定的信息。该参数的内容与Msg参数值有关。返回值：返回值就是消息处理结果，它与发送的消息有关。
				   return DefWindowProc(hWnd, message, wParam, lParam);
			}
			break;
		case WM_PAINT:
			hdc = BeginPaint(hWnd, &ps);
			// TODO: Add any drawing code here...
			RECT rt;
			GetClientRect(hWnd, &rt);
			DrawText(hdc, szHello, strlen(szHello), &rt, DT_CENTER);
			EndPaint(hWnd, &ps);
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
   }
   return 0;
}

// Mesage handler for about box.
LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_INITDIALOG://WM_INITDIALOG：消息类别 表示对话框已经创建，开始初始化对话框控件
				return TRUE;

		case WM_COMMAND://需要判别wParam的低16位以处理对话框控件发出的命令，单击OK时，wParam的低16位为IDOK，单击cancle时，wParam的低16位为IDCANCEL
			if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) 
			{
				EndDialog(hDlg, LOWORD(wParam));//用于关闭对话框
				return TRUE;
			}
			break;
	}
    return FALSE;
}
