#include "pch.h"

char* my_strcat(char *str1, char *str2)
{
	char *str;
	int length = 0;
	int length_str1 = 0;
	int length_str2 = 0;
	if (str1 == NULL && str2 == NULL)
		return NULL;
	else if (str1 == NULL && str2 != NULL)
		return str2;
	else if (str1 != NULL && str2 == NULL)
		return str1;
	else {
		/*求总长度*/
		while (*str1 != '\0')
		{
			str1++;
			length_str1++;
		}
		while (*str2 != '\0')
		{
			str2++;
			length_str2++;
		}
		length = length_str1 + length_str2 + 1;

		str1 -= length_str1;
		str2 -= length_str2;
		/*创建临时空间*/
		str = (char*)malloc(length * sizeof(char));
		/*复制*/
		while (*str1 != '\0')
		{
			*str = *str1;
			str++;
			str1++;
		}
		while (*str2 != '\0')
		{
			*str = *str2;
			str++;
			str2++;
		}
		*str = '\0';
		//回退
		str -= (length - 1);
		return str;
	}
}

TEST(TestCaseName, TestName) {
	EXPECT_STREQ("abcdef", my_strcat("abc", "def"));
	EXPECT_STREQ("abc", my_strcat("abc", ""));
	EXPECT_STREQ("abc", my_strcat("abc", NULL));
	EXPECT_STREQ("abc", my_strcat(NULL, "abc"));
	EXPECT_STREQ(NULL, my_strcat(NULL, NULL));
}

int main(int argc, char* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}