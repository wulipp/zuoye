#include "stdafx.h"
#include "str.h"
#include "gtest/gtest.h"
//#include<iostream>

/*
三、用 gtest 来进行单元测试
测试 strcat("abc", "def");
测试 strcat("abc", "");
测试 strcat("abc", NULL);
测试 strcat(NULL, "abc");
测试 strcat(NULL, NULL);
*/

TEST(str, str_s) {
	EXPECT_STREQ("abcdef", str_s("abc", "def"));
	EXPECT_STREQ("abc", str_s("abc", ""));
	EXPECT_STREQ("abc", str_s("abc", NULL));
	EXPECT_STREQ("abc", str_s(NULL, "abc"));
	EXPECT_STREQ("", str_s(NULL, NULL));
}

int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
#pragma once
// ConsoleApplication1.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <iostream>
#include "stdio.h"

using namespace std;

char* str_s(char *str1, char *str2) {

	int length1 = 0, length2 = 0;

	if (str1 != NULL) {
		while (str1[length1] != '\0')
		{
			length1++;
		}
	}
	else {
		return "error";
	}

	if (str2 != NULL) {
		while (str2[length2] != '\0')
		{
			length2++;
		}
	}
	else {
		return "error";
	}

	char *newstr = (char*)malloc(length1 + length2 + 1);

	int s1 = 0, s2 = 0, i = 0;
	while (str1[s1] != '\0')
	{
		newstr[i++] = str1[s1];
		s1++;
	}

	while (str2[s2] != '\0')
	{
		newstr[i++] = str2[s2];
		s2++;
	}

	newstr[length1 + length2] = '\0';

	return newstr;

}
