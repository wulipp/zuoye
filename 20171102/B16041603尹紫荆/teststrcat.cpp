//链接器-输入-附加依赖项 添加gtestd.lib的地址！
//否则出现无法解析的外部命令错误

#include "stdafx.h"
#include "fun.h"

TEST(fun, myStrcat)
{
	EXPECT_EQ("abcdef", myStrcat("abc","def"));
	EXPECT_EQ("abc", myStrcat("abc", ""));
	EXPECT_EQ("abc", myStrcat("abc", NULL));
	EXPECT_EQ("abc", myStrcat(NULL, "abc"));
	EXPECT_EQ("", myStrcat(NULL, NULL));
}

int  main(int argc, char* argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

//ctrl+F5运行不闪退