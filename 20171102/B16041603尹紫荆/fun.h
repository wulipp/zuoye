﻿#ifndef _FOO_H_
#define _FOO_H_
#include<stdlib.h>

char* myStrcat(char* str1, char* str2)
{
	int cnt1 = 0, cnt2 = 0;
	char *temp1 = str1;
	while (str1 && *temp1++ != '\0')//判断str是否为空
	{
		cnt1++;
	}
	char *temp2 = str2;
	while (str2 && *temp2++ != '\0')
	{
		cnt2++;
	}
	char *t1 = (char*)malloc(sizeof(char)*(cnt1 + 1));
	char *t2 = (char*)malloc(sizeof(char)*(cnt2 + 1));
	char *t = (char*)malloc(sizeof(char)*((cnt1 + cnt2) + 1));
	char *n = t;
	while (str1 && *str1 != '\0')
	{
		*t++ = *str1++;
	}
	while (str2 && *str2 != '\0')
	{
		*t++ = *str2++;
	}
	*t = '\0';
	return n;
}

#endif//_FOO_H_

