// Mystrcat.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <gtest/gtest.h>


char * mystrcat(char * str1, char * str2)  //传入两个
{
	char * a; //定义两个字符指针
	char * b;
	int m = 0, n = 0;
	if (str1 == NULL || str2 == NULL)
	{
		if (str1 == NULL&&str2 == NULL)
			return NULL;
		else if (str1 == NULL)
			return str2;
		else
			return str1;
	}
	m = strlen(str1);
	n = strlen(str2);
	a = (char*)malloc(sizeof(char*) * (m + n));
	b = a;
	while (*str1 != '\0')
	{
		*a++ = *str1++;
	}

	while (*str2)
	{
		*a = *str2;
		a++;
		str2++;
	}
	*a = '\0';
	return b;
}

TEST(FooTest, HandleNoneZeroInput)
{
	EXPECT_STREQ("abcdef", mystrcat("abc", "def"));
	EXPECT_STREQ("abc", mystrcat("abc", ""));
	EXPECT_STREQ("abc", mystrcat("abc", NULL));
	EXPECT_STREQ("abc", mystrcat(NULL, "abc"));
	EXPECT_STREQ(NULL, mystrcat(NULL, NULL));
}


int _tmain(int argc, _TCHAR * argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

