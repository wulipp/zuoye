// strcat.cpp : 定义控制台应用程序的入口点。
//

//#include "stdafx.h"
#include<malloc.h>
#include<stdio.h>
#include<string>
unsigned long strlen(char* str)
{
	unsigned long length=0;
	while(str[length]!='\0'){length++;}
	return length;
}
void strcpy(char* str1,char* str2)
{
	int i=0;
	while(str2[i]!='\0')
	{
		str1[i]=str2[i];
		i++;
	}
	str1[i]='\0';
}
char* strcat(char* str1,char* str2)
{
	unsigned long len1,len2;
	len1=strlen(str1);
	len2=strlen(str2);
	char* str3=(char*)malloc(sizeof(char*)*(len1+len2+1));
	strcpy(str3,str1);
	
	int i =0;
	while(str3[i]!='\0'){i++;}
	int j = 0;
	while(str2[j]!='\0'){str3[i++]=str2[j++];}
	str3[i]='\0';
	return str3;
}
int main(/*int argc, _TCHAR* argv[]*/)
{
	printf("%s",strcat("abc","def"));
	return 0;
}

