#include "stdafx.h"
#include "gtest/gtest.h"
#include "myStrcat.h"

TEST(TestCaseName, test0) {
	EXPECT_STREQ("abcdef", myStrcat("abc", "def"));
}
TEST(TestCaseName, test1) {
	EXPECT_STREQ("abc", myStrcat("abc", ""));
}
TEST(TestCaseName, test2) {
	EXPECT_STREQ("abc", myStrcat("abc", NULL));
}
TEST(TestCaseName, test3) {
	EXPECT_STREQ("abc", myStrcat(NULL, "abc"));
}
TEST(TestCaseName, test4) {
	EXPECT_STREQ(NULL, myStrcat(NULL, NULL));
}
int _tmain(int argc, _TCHAR* argv[])
{
	//多个测试用例时使用，如果不写，运行RUN_ALL_TESTS()时会全部测试，加上则只返回对应的测试结果  
	//testing::GTEST_FLAG(filter) = "test_case_name.test_name";
	//测试初始化
	testing::InitGoogleTest(&argc, argv);
	//return RUN_ALL_TESTS();
	RUN_ALL_TESTS();
	//暂停，方便观看结果,结果窗口将会一闪而过  
	system("PAUSE");
	return 0;
}