// 实验作业1.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <string.h>
#include <malloc.h>
#include< gtest\gtest.h >

char* Strcat(char* str1, char* str2)
{
	if (str1 == NULL&&str2 == NULL)
	{
		return 0;
	}
	if (str1 == NULL)
		return str2;
	if (str2 == NULL)
		return str1;
	char* str3;
	int i;
	int a = 0;
	int b = 0;
	while (str1[a] != '\0')
	{
		a++;
	}

	while (str2[b] != '\0')
	{
		b++;
	}
	str3 = (char*)malloc(sizeof(char)*(a +b));
	for (i = 0; i <a; i++)
	{
		str3[i] = str1[i];
	}
	for (i = a; i < a + b; i++)
	{
		str3[i] = str2[i -a];
	}
	str3[a + b] = '\0';
	return str3;
}
EST(FootTest, HandleNoneZeroInput)
{
	EXPECT_STREQ("abcdef", stract("abc", "def"));
	EXPECT_STREQ("abc", stract("abc", ""));
	EXPECT_STREQ("abc", stract("abc", NULL));
	EXPECT_STREQ("abc", stract(NULL, "abc"));
	EXPECT_STREQ(NULL, stract(NULL, NULL));
}

int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return  RUN_ALL_TESTS();
}