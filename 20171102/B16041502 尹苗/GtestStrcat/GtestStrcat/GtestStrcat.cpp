// GtestStrcat.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "mystrcat.h"

TEST(mystrcat, myStrcat)
{
	EXPECT_STREQ("abcdef", myStrcat("abc", "def"));
	EXPECT_STREQ("abc", myStrcat("abc", ""));
	EXPECT_STREQ("abc", myStrcat("abc", NULL));
	EXPECT_STREQ("abc", myStrcat(NULL, "abc"));
	EXPECT_STREQ("", myStrcat(NULL, NULL));
}
int main(int argc,char* argv[])

{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

