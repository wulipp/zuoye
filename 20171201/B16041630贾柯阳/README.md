![输入图片说明](https://gitee.com/uploads/images/2017/1224/205030_5f03408a_1577189.png "俄罗斯方块函数和变量.png")

1.init_game()初始化游戏，获得方块，计算相关矩形大小，初始化速度。

2.消息循环
	
	(1)PeekMessage(&msg, 0, 0, 0, PM_REMOVE)检查是否有消息，如果有就发送到指定窗口。
	(2)GetTickCount();获取系统当前时间，判断是否要下降方块。
	(3)CheckValide(int curPosX, int curPosY, BOOL bCurTeris[4][4])根据此函数的结果判断要进行的操作。
3.WndProc()消息处理函数
	(1)WM_CREATE:创建窗口，设置窗口位置。
	(2)WM_COMMAND:接受菜单消息，根据相信的消息作出反应。
	(3)WM_KEYDOWN:根据按键以及CheckValide的结果移动存放方块的矩形。
	(4)WM_PAINT:重新绘制背景。
	(5)WM_DESTROY:退出程序。
