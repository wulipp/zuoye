###俄罗斯方块
![](http://m.qpic.cn/psb?/V12CtaGk3eix72/Wlar.cF6bbp4Yqs4ZBmr6flRNzQ3irDXNTBCFPANm08!/b/dDwBAAAAAAAA&bo=KQINAwAAAAARBxU!&rf=viewer_4)


### 图形界面元素与代码之间的关系

1、 消息循环 

(1)GetTickCount 

(2)CheckValide

2、 init_game() 计算各矩形，设定窗口位置和大小，初始化速度

3、 wndproc中

(1)WM_CREATE 创建菜单

(2)WM_COMMAND 处理材料， setdifferent

(3)WM_KEYDOWN 处理键盘消息，处理俄罗斯方块矩阵旋转

(4)WM_PAINT 

* drawteris画当前的方块和背景中已经填充的方块；

* drawnext画右上角的部分；

* drawscore画右下角部分

### 主要数据结构：

bool g_hasBlocked[50][50];

BOOL bCurTeris[4][4];

BOOL bNextCurTeris[4][4];

int curPosX, curPosY;

int state_teris[][4][4]