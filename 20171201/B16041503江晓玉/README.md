# Tetris
![](https://gitee.com/AYuKKNi/images/raw/master/Tetris.png)
# 初始化游戏
![](https://gitee.com/AYuKKNi/images/raw/master/%E5%88%9D%E5%A7%8B%E5%8C%96%E6%B8%B8%E6%88%8F.png)
# 注册窗口类
![](https://gitee.com/AYuKKNi/images/raw/master/%E6%B3%A8%E5%86%8C%E7%AA%97%E5%8F%A3%E7%B1%BB.png)
# 应用程序初始化
![](https://gitee.com/AYuKKNi/images/raw/master/%20%E5%BA%94%E7%94%A8%E7%A8%8B%E5%BA%8F%E5%88%9D%E5%A7%8B%E5%8C%96.png)
# 消息循环
![](https://gitee.com/AYuKKNi/images/raw/7f1723f3e883a0a747e210768866f36673e8488a/%E6%B6%88%E6%81%AF%E5%BE%AA%E7%8E%AF.png)
# 创建窗口
![](https://gitee.com/AYuKKNi/images/raw/7f1723f3e883a0a747e210768866f36673e8488a/%E5%88%9B%E5%BB%BA%E7%AA%97%E5%8F%A3.png)
# 命令项目
![](https://gitee.com/AYuKKNi/images/raw/7f1723f3e883a0a747e210768866f36673e8488a/%E5%91%BD%E4%BB%A4%E9%A1%B9%E7%9B%AE.png)
# 键盘消息
![](https://gitee.com/AYuKKNi/images/raw/7f1723f3e883a0a747e210768866f36673e8488a/%E9%94%AE%E7%9B%98%E6%B6%88%E6%81%AF.png)
# 更新画面
![](https://gitee.com/AYuKKNi/images/raw/7f1723f3e883a0a747e210768866f36673e8488a/%E6%9B%B4%E6%96%B0%E7%94%BB%E9%9D%A2.png)

