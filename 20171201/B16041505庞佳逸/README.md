# 函数、变量作用
![输入图片说明](https://gitee.com/uploads/images/2017/1220/153115_ab76cd95_1581486.png "Tetris.png")

# 图形界面元素与代码之间的关系

## 消息循环
(1) GetTickCount 取得从操作系统启动所经过的毫秒数，从而控制方块下落时间

(2) CheckValide 判断对方块的操作是否合法

## init_game()
(1)计算各矩形大小

(2)设定窗口位置和大小

(3)初始化速度

## wndproc
(1)WM_CREATE 

* 创建菜单

(2)WM_COMMAND 

* 处理材料
* 设置难度
* 设置布局

(3)WM_KEYDOWN

* 处理方块左右平移的键盘消息
* 处理方块向下加速运动的键盘消息
* 处理方块矩阵旋转的键盘消息

(4)WM_PAINT

* drawteris 画当前的方块和背景中已经填充的方块
* drawnext 画信息区上方的下一个方块
* drawscore 画信息区下方的难度和得分
