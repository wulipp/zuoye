##俄罗斯方块流程图
![](http://ww1.sinaimg.cn/large/a91fc5bcgy1fmlz03ebvdj20ty0s9tcz.jpg)

### 函数
checkComplite()//查看一行能否消去
selectDiffculty(HWND hWnd, int diff)//选择难度
selectLayOut(HWND hWnd, int layout)//选择布局
CheckValide(int startX, int startY, BOOL bTemp4//查看矩阵是否合法
fillBlock()//填充矩阵
etRandomT()//产生方块
init_game()//初始化游戏
RotateTeris(BOOL bTeris4)//改变形状
drawBlocked(HDC hdc)//绘制当前砖块
DrawBackGround(HDC hdc)//绘制背景
outPutBoxInt(int num)//弹窗函数
setRandomT()//随机生成下一次掉落方块

###变量

####全局变量
加速后的速度g_speed
正常的速度t_speed
当前小方块bCurTeris[4][4]
小方块在游戏区的x坐标curPosX
小方块在游戏区的y坐标curPosY
随机种子rd_speed
运行的毫秒数:tPre
从曹组系统经过的毫秒数tCur
游戏状态GAME_START
游戏得分GAME_SCORE
游戏难度GAME_DIFF
游戏区x坐标长度NUM_X
游戏区y坐标长度NUM_Y
方块大小BLOCK_SIZE

####常量
x方向的边距const int BORDER_X
y方向的边距const int BORDER_Y
左边游戏区的宽const int SCREEN_LEFT_X
右边游戏区的宽const int SCREEN_RIGHT_X
用户区的高const int SCREEN_Y
用户区的宽const int SCREEN_X
存储方块const BOOL state_teris[][4][4]