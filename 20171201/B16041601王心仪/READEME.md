## 函数</br>
选择难度int selectDiffculty(HWND hWnd, int diff)</br>
选择布局int selectLayOut(HWND hWnd, int layout)</br>
给定一个矩阵，查看是否合法int CheckValide(int startX, int startY, BOOL bTemp[4][4])</br>
将方块填充给矩形void fillBlock()</br>
查看一行是否能消去void checkComplite()</br>
产生新的方块void setRandomT()</br>
初始化游戏void init_game()</br>
改变形状void RotateTeris(BOOL bTeris[4][4])</br>
绘制当前砖块void drawBlocked(HDC hdc)</br>
绘制背景void DrawBackGround(HDC hdc)</br>
自定义弹窗函数void outPutBoxInt(int num)</br>
随机生成下一次掉落方块void setRandomT()</br>
绘制正在掉落的方块void DrawTeris(HDC mdc)</br>
绘制下一个方块void drawNext(HDC hdc)</br>
绘制分数void drawScore(HDC hdc)</br>
在消去那一行画出空白表示消去void drawCompleteParticle(int line)</br>
获取一个随机intint RandomInt(int _min, int _max)</br>
给定一个矩阵查看是否合法int CheckValide(int curPosX, int curPosY, BOOL bCurTeris[4][4])</br>
## 变量
x方向的边距int BORDER_X</br>
y方向的边距int BORDER_Y</br>
左边游戏区的宽int SCREEN_LEFT_X</br>
右边游戏区的宽int SCREEN_RIGHT_X</br>
用户区的高int SCREEN_Y</br>
用户区的宽int SCREEN_X</br>
存储方块BOOL state_teris[][4][4]</br>
加速后的速度g_speed</br>
正常的速度t_speed</br>
当前小方块bCurTeris[4][4]</br>
小方块在游戏区的x坐标：curPosX</br>
小方块在游戏区的y坐标：
curPosY</br>
随机种子rd_speed</br>
运行的毫秒数:tPre</br>
从曹组系统经过的毫秒数tCur</br>
游戏状态GAME_START</br>
游戏得分GAME_SCORE</br>
游戏难度GAME_DIFF</br>
游戏区x坐标长度NUM_X</br>
游戏区y坐标长度NUM_Y</br>
方块大小BLOCK_SIZE
## 代码与图形元素界面关系
#### init_game:
随机生成方块、计算界面大小、初始化速度</br>
#### WndProc:
WM_CREATE设置菜单</br>
WM_COMMAND关于消息框、设置难度、设置布局</br>
WM_KEYDOWN键盘消息</br>
WM_PAINT绘制背景


