变量：
GAME_SCORE:                                                     //游戏分数
GAME_STATE:                                                     //游戏状态
GAME_DIFF:                                                      //游戏难度
curPosX:                                                        //当前X坐标
curPosY:                                                        //当前Y坐标
BOOL bCurTeris[4][4]:                                           //当前俄罗斯方块
BOOL bNextCurTeris[4][4]:                                       //下一个俄罗斯方块
int BLOCK_SIZE:                                                 //一个方块大小
BOOL g_hasBlocked[50][50]:                                      //界面数组
int NUM_X = 10;                                                 //界面横向一共10格，纵向20格
int NUM_Y = 20; 
const BOOL state_teris[][4][4] =                                //俄罗斯方块的样式
{
	{ { 1,1,1,1 },{ 0,0,0,0 },{ 0,0,0,0 },{ 0,0,0,0 } },
	{ { 0,1,1,0 },{ 0,1,1,0 },{ 0,0,0,0 },{ 0,0,0,0 } },
	{ { 0,1,1,1 },{ 0,0,0,1 },{ 0,0,0,0 },{ 0,0,0,0 } },
	{ { 0,1,1,0 },{ 0,0,1,1 },{ 0,0,0,0 },{ 0,0,0,0 } },
	{ { 0,1,0,0 },{ 1,1,1,0 },{ 0,0,0,0 },{ 0,0,0,0 } },
	{ { 0,1,1,1 },{ 0,1,0,0 },{ 0,0,0,0 },{ 0,0,0,0 } },
	{ { 0,1,1,0 },{ 1,1,0,0 },{ 0,0,0,0 },{ 0,0,0,0 } }
};

函数：
void checkComplite();                                            //查看一行是否能消去  
void drawBlocked(HDC hdc);                                       //绘制当前已经存在砖块的区域
void DrawBackGround(HDC hdc);                                    //绘制背景
void outPutBoxInt(int num);                                      //自定义的弹窗函数  用于调试
void setRandomT();                                               //随机生成一个方块用作下一次掉落
void init_game();                                                //初始化游戏
void fillBlock();                                                //到达底部后填充矩阵
void RotateTeris(BOOL bTeris[4][4]);                             //旋转矩阵
void DrawTeris(HDC mdc);                                         //绘制正在下落的方块
void drawNext(HDC hdc);                                          //绘制下一个将要掉落的方块
void drawScore(HDC hdc);                                         //绘制分数
void drawCompleteParticle(int line);                             //显示消除的一行
int RandomInt(int _min, int _max);                               //获取一个随机int
int CheckValide(int curPosX, int curPosY, BOOL bCurTeris[4][4]); //给定一个矩阵，查看是否合法
int selectDiffculty(HWND hWnd, int dif);                         //选择难度
int selectLayOut(HWND hWnd, int layout);                         //选择布局
ATOM                MyRegisterClass(HINSTANCE hInstance);        //窗口注册函数
BOOL                InitInstance(HINSTANCE, int);                //在启动时初始化应用程序
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);         //窗 口过程处理函数
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);           //处理关于对话框

