## Tetris变量和函数思维导图
### ![Tetris变量和函数](http://m.qpic.cn/psb?/V141RwUG3Hgo3P/1asAfRrtlpxgnrdEVlfetkvONlqg8o.JWxQepV.XMhY!/b/dPIAAAAAAAAA&bo=0QSAAgAAAAADJ1U!&rf=viewer_4)
## 界面元素与代码间的关系
### 1.主要数据结构
#### 1)BOOL state_teris[][4][4]
#### 2)bool g_hasBlocked[50][50]
#### 3)BOOL bCurTeris[4][4]
#### 4)BOOL bNextCurTeris[4][4]

### 2.init_game()
#### 1)设置初始坐标；
#### 2)计算各矩形；
#### 3)设定窗口位置和大小；
#### 4)初始化速度

### 3.WinMain()
#### 主消息循环
##### 1)GetTickCount()
##### 2)CheckValide()

### 4.WndProc()
#### 1)WM_CREATE添加菜单（难度、布局）
#### 2)WM_COMMAND处理菜单项选择
#### 3)WM_KEYDOWN处理键盘消息（方块上、下、左、右移动；旋转、下落）
#### 4)WM_PAINT绘制窗口(绘制游戏界面)
##### DrawBackGround(hdc)
##### a) 绘制左右区域Rectangle
##### b) DrawTeris画当前的方块和背景中已经填充的方块;
##### c) DrawNext画右上角的部分（绘制下一个将要掉落的方块和标识）;
##### d) DrawScore画右下角部分（绘制当前难度和得分）
