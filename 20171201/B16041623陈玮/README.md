图形界面元素与代码之间的关系
1. 消息循环
（1）GetTickCount 
（2）CheckValide
2. init_game() 计算各矩形，设定窗口位置和大小，初始化速度
3. wndproc中
（1）WM_CREATE 创建菜单
（2）WM_COMMAND 处理材料， setdifferent
（3）WM_KEYDOWN 处理键盘消息，处理俄罗斯方块矩阵旋转
（4）WM_PAINT中 drawteris画当前的方块和背景中已经填充的方块；drawnext画右上角的部分；drawscore画右下角部分

主要数据结构：
bool g_hasBlocked[50][50];
BOOL bCurTeris[4][4];
BOOL bNextCurTeris[4][4];
int curPosX, curPosY;
int state_teris[][4][4]

思维导图如下：
![输入图片说明](https://gitee.com/uploads/images/2017/1219/163830_83c2b8d5_1342734.png "俄罗斯方块.png")