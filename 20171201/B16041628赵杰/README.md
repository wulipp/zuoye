# 主要数据结构： 
![[变量思维导图]](https://gitee.com/uploads/images/2017/1210/154901_151f25ef_1575090.png "俄罗斯方块中的主要变量、常量.png")
# 图形界面元素与代码之间的关系
## 1、wWinMain(...)中

![[wWinMain思维导图]](https://gitee.com/uploads/images/2017/1210/154927_a1f226d3_1575090.png "wWinMain(...).png")
## 2、init_game() 中
### 分数清零、随机生成下一个方块、计算各矩形，设定窗口位置和大小、初始化速度 ###
![[init思维导图]](https://gitee.com/uploads/images/2017/1210/154947_9c713e25_1575090.png "init_game().png")
## 3、WndProc中
### （1）WM_CREATE 
创建窗口时，添加菜单（难度选择、布局选择）
### （2）WM_COMMAND 
处理菜单项选择， setdifferent
### （3）WM_KEYDOWN 
处理键盘消息（俄罗斯方块矩阵旋转、左右移动、快速下落）
### （4）WM_PAINT 
hdc = BeginPaint(hWnd, &ps);
### （5）WM_DESTROY
DrawBackGround(hdc)绘制游戏界面

![[WndProc思维导图]](https://gitee.com/uploads/images/2017/1210/155017_d625c718_1575090.png "WndProc(...).png")
## 4、DrawBackGround中
（1）绘制左右区域

Rectangle(mdc, rc_left.left + BORDER_X, rc_left.top + BORDER_Y, rc_left.right, rc_left.bottom);

Rectangle(mdc, rc_right.left + BORDER_X, rc_right.top + BORDER_Y, rc_right.right, rc_right.bottom);

（2）DrawTeris画当前的方块和背景中已经填充的方块；

![[DrawTeris思维导图]](https://gitee.com/uploads/images/2017/1210/155032_6d549dc1_1575090.png "DrawTeris(HDC mdc).png")

（3）drawNext画右上角的部分（绘制下一个将要掉落的方块和“下一个”标识）；
![drawNext思维导图](https://gitee.com/uploads/images/2017/1210/155955_2bfebfac_1575090.png "drawNext(mdc);.png")
（4）drawScore画右下角部分（“当前难度”与“得分”）;

![[drawScore思维导图]](https://gitee.com/uploads/images/2017/1210/155046_9cc38ecc_1575090.png "drawScore(HDC hdc).png")

![[DrawBackGround思维导图]](https://gitee.com/uploads/images/2017/1210/155058_c580933a_1575090.png "DrawBackGround(HDC hdc).png")
## 5、RotateTeris中

![[RotateTeris思维导图]](https://gitee.com/uploads/images/2017/1210/155109_44f33fc4_1575090.png "RotateTeris(BOOL bTeris[4][4]).png")

![[总图]](https://gitee.com/uploads/images/2017/1210/155128_f39ee282_1575090.png "tetris函数流程图.png")