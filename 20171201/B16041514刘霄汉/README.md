## 12.01 作业

### 变量和函数

![输入图片说明](https://gitee.com/uploads/images/2017/1220/115041_db2f279c_1578283.png "TetrisExplain.png")

### 图形界面元素与代码之间的关系

#### 1.消息循环 

(1)GetTickCount 

(2)CheckValide

#### 2.init_game() 

(1)计算各矩形

(2)设定窗口位置和大小

(3)初始化速度

#### 3.wndproc()

(1)WM_CREATE

* 创建菜单

(2)WM_COMMAND

* 处理材料

* 设置难度

* 设置布局

(3)WM_KEYDOWN

* 处理键盘消息

* 处理俄罗斯方块矩阵旋转

(4)WM_PAINT 

* drawteris画当前的方块和背景中已经填充的方块；

* drawnext画右上角的部分；

* drawscore画右下角部分

### 主要数据结构：

* bool g_hasBlocked[50][50];

* BOOL bCurTeris[4][4];

* BOOL bNextCurTeris[4][4];

* int curPosX, curPosY;

* int state_teris[][4][4]