## 12.01 作业

### 变量和函数

![输入图片说明](https://gitee.com/uploads/images/2017/1221/202530_220d5845_1575162.png "Tetris.png")

### 图形界面元素与代码之间的关系

1、 WinMain()主消息循环 

(1)GetTickCount 

(2)CheckValide

2、 init_game() 初始化游戏

(1)设置初始坐标

(2)计算各矩形

(3)设定窗口位置和大小

(4)初始化速度

3、 wndproc()

(1)WM_CREATE 创建菜单

(2)WM_COMMAND 处理菜单项选择

(3)WM_KEYDOWN 处理键盘消息，处理俄罗斯方块矩阵旋转

(4)WM_PAINT 绘制窗口

   DrawBackGround(hdc)：

   * drawteris画当前的方块和背景中已经填充的方块；

   * drawnext绘制下一个将要掉落的方块和标识；

   * drawscore绘制当前难度和得分；

### 主要数据结构：

bool g_hasBlocked[50][50];

BOOL bCurTeris[4][4];

BOOL bNextCurTeris[4][4];

int curPosX, curPosY;

int state_teris[][4][4]