/* ************************************
*《精通Windows API》 
* 示例代码
* global.c
* 5.3.3  使用全局和局部函数分配和释放内存
**************************************/

/* 头文件　*/
#include <windows.h>
#include <stdio.h>

/*************************************
* int main(void)
* 功能	演示Global*函数的使用
*
* 参数	无
*
**************************************/
int main(void)
{	
	LPVOID lpMem;	//内存地址  LPVOID  指向任何类型的指针
	HGLOBAL hMem;	//内存句柄  HGLOBAL 全局内存块的句柄
	SIZE_T sizeMem;	//内存大小  SIZE_T  指针可以指向的最大字节数。用于必须跨越指针的全范围的计数
	UINT uFlags;	//属性  UINT 无符号INT。范围为0到4294967295十进制

	//分配内存
	//GlobalAlloc函数 从堆中分配指定的字节数 
	//第一个参数 GPTR:分配固定内存,将内存初始化为零 
	//第二个参数 要分配的字节数 
	lpMem = (LPVOID)GlobalAlloc(GPTR,1000); 
	
	//将数据复制到内存中
	//lstrcpy 将字符串复制到缓冲区 
	//第一个参数:一个缓冲区，用于接收第二个参数指向的字符串的内容 
	//第二个参数:要复制的以null结尾的字符串 
	lstrcpy(lpMem,"this is a string");	//lstrcpy函数 将字符串复制到缓冲区 
	
	//获得内存属性，打印
	//GlobalFlags函数 检索有关指定的全局内存对象的信息
	//参数:全局内存对象的句柄 
	uFlags = GlobalFlags(lpMem); 
	
	//打印 
	printf("内存中的内容：\"%s\"，内存地址：0x%.8x，内存属性：%u\n",
		lpMem,lpMem,uFlags);
		
	//释放
	// GlobalFree 释放指定的全局内存对象并使其句柄无效 
	//参数:全局内存对象的句柄 
	GlobalFree(lpMem); 
	
	//分配内存，获取信息
	//GlobalAlloc函数 从堆中分配指定的字节数 
	//第一个参数 GMEM_MOVEABLE:分配活动记忆 内存块从不在物理内存中移动，但可以在默认堆中移动
	//第二个参数 要分配的字节数 
	hMem = GlobalAlloc(GMEM_MOVEABLE,1000); 
	
	// GlobalSize 检索指定的全局内存对象的当前大小（以字节为单位）
	//参数:全局内存对象的句柄 
	sizeMem = GlobalSize(hMem); 
	
	//GlobalFlags函数 检索有关指定的全局内存对象的信息
	//参数:全局内存对象的句柄 
	uFlags = GlobalFlags(hMem); 
	
	//打印信息
	printf("内存大小：%d，内存句柄：0x%.8x，内存属性：%u\n",
		sizeMem,hMem,uFlags);
		
	//锁定
	// GlobalLock 锁定全局内存对象，并返回指向对象内存块第一个字节的指针 
	//参数:全局内存对象的句柄 
	lpMem = GlobalLock(hMem); 
	
	//释放
	// GlobalFree 释放指定的全局内存对象并使其句柄无效 
	//参数:全局内存对象的句柄 
	GlobalFree(hMem); 
	return 0;
}
