/* ************************************
*《精通Windows API》
* 示例代码
* Regions.c
* 13.8 区域的填充和反转
**************************************/
/* 头文件 */
#include <windows.h>
/* 全局变量 */
LPSTR szAppName = "RGN";//标题名
LPSTR szTitle = "RGN Sample";//类名
/* 函数声明 */
LRESULT CALLBACK WndProc( HWND ,  UINT ,  WPARAM ,  LPARAM );//处理主窗口的消息
//LRESULT:_W64 long   
//CALLBACK:__stdcall被这个关键字修饰的函数，其参数都是从右向左通过堆栈传递的(__fastcall的前面部分由ecx,edx传)，函数调用在返回前要由被调用者//清理堆栈。
//HDWN:窗口句柄   UINT:unsigned int   WPARAM;包含有关消息的附加信息,不同消息其值有所不同   LPARAM:用于提供消息的附加信息
/*************************************
* ElliRgns
* 功能	创建椭圆区域，并进行填充和反转
**************************************/
HRGN ElliRgns(HWND hwnd, POINT point)//HRGN:区域句柄，用于设置窗体区域的一个值   POINT：定义了屏幕上或窗口中的一个点的X和Y坐标
{
	
	RECT rect, rectClient;
	HRGN hrgn;
	HBRUSH hBrushOld, hBrush;
	// DC
	HDC hdc = GetDC(hwnd);
	GetClientRect(hwnd,&rectClient);
	// 点的周围一块区域
	rect.left = point.x - 40;
	rect.right = point.x + 40;
	rect.top = point.y - 30;
	rect.bottom = point.y + 30;
	// 通过RECT 创建椭圆区域
	hrgn = CreateEllipticRgnIndirect(&rect);//CreateEllipticRgnIndirect():创建一内切于特定矩形的椭圆区域
	// 创建画刷
	hBrush = CreateSolidBrush(RGB(0,255,0));//创建一个具有指定颜色的逻辑刷子,初始化一个指定颜色的画刷,画笔可以随后被选为任何设备上下文的                                                //当前刷子
	// 为DC 选择画刷
	hBrushOld = (HBRUSH)SelectObject(hdc,hBrush);//SelectObject():选择一对象到指定的设备上下文环境中，新对象替换先前的相同类型的对象
	// 使用当前画刷绘制区域
	PaintRgn(hdc,hrgn);
	// 等待一断时间后，将刚才绘制的区域进行反转
	Sleep(3000);
	InvertRgn(hdc,hrgn);
	// 释放资源
	hBrush = (HBRUSH)SelectObject(hdc,hBrushOld);
	DeleteObject(hBrush);
	DeleteObject(hrgn);
	DeleteDC( hdc );
	return 0;
}

int WINAPI WinMain(
				   HINSTANCE hInstance,
				   HINSTANCE hPrevInstance,
				   LPSTR lpCmdLine,
				   int nCmdShow)
{
	MSG msg;
消息号的值有关，
在Win中消息用
结构体MSG表示
	HWND hWnd;
	WNDCLASS wc;

	wc.style = CS_OWNDC; 
	wc.lpfnWndProc = (WNDPROC)WndProc; 
	wc.cbClsExtra = 0; 
	wc.cbWndExtra = 0; 
	wc.hInstance = hInstance;
	wc.hIcon = NULL; 
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = szAppName; 

	if (!RegisterClass(&wc))
		return (FALSE);
	}

	hWnd = CreateWindow(
		szAppName, 
		szTitle, 
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, 
		NULL, 	NULL,	hInstance, NULL 
		);
	if (!hWnd)
	{
		return (FALSE);
	}

	ShowWindow(hWnd, nCmdShow); 
	UpdateWindow(hWnd); 

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return (int)(msg.wParam);

	UNREFERENCED_PARAMETER(lpCmdLine); 
}


LRESULT CALLBACK WndProc(
						 HWND hWnd, 
						 UINT message, 
						 WPARAM uParam, 
						 LPARAM lParam)
{

	switch (message)
	{
	case WM_CREATE:
		break;

	case WM_PAINT:
		break;

	case WM_LBUTTONDOWN:
		{
			
			POINT point;
			point.x = LOWORD(lParam);
			point.y = HIWORD(lParam);
			ElliRgns(hWnd, point);
		}
		break;

	case WM_DESTROY: 
		PostQuitMessage(0);
		break;

	default: 
		return (DefWindowProc(hWnd, message, uParam, lParam));
	}
	return (0);
}