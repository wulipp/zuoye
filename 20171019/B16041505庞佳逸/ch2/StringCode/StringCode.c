/* ************************************
*《精通Windows API》 
* 示例代码
* StringCode.c
* 2.4	Unicode和多字节
**************************************/

/* 预处理　*/
/* 头文件　*/
#include <windows.h>

/* ************************************
* 功能	Unicode与多字节编码演示
**************************************/
int WINAPI WinMain(
	// HINSTANEC	-	实例句柄
	// LPSTR		-	char* 窄字符串
	// LPWSTR		-	LPWSTR类型的宽字符串
	// LPTSTR		-	自适就字符串

			HINSTANCE hInstance,
			HINSTANCE hPrevInstance,
			LPSTR lpCmdLine,
			int nCmdShow
			)
{
	LPWSTR szUnicode = L"This is a Unicode String;";
    LPSTR szMutliByte = "This is not a Unicode String;";
    LPTSTR szString = TEXT("This string is Unicode or not depends on the option.");

	/*  
	参数列表：
	1、A handle to the owner window,these message boxes have no owner window
	2、The message to be displayed.
	3、The dialog box title.
	4、The contents and behavior of the dialog box*/
    //使用W版本的API函数，以宽字符串为参数。
    MessageBoxW(NULL,szUnicode,L"<字符编码1>",MB_OK);
    //使用A版本的API函数，以窄字符串为参数。
    MessageBoxA(NULL,szMutliByte,"<字符编码2>",MB_OK);
    //使用自适用的API函数，采用相适应的字符串类型为参数。
    MessageBox(NULL,szString,TEXT("<字符编码3>"),MB_OK);

	return 0;
}