﻿#include <Windows.h>
#include <shlobj.h>

int WINAPI WinMain(
	HINSTANCE hInstance,//句柄
    HINSTANCE hPrevInstance,//句柄
    LPSTR lpCmdLine,//指向32位ANSI字符指针数组
    int nCmdShow//指定窗口如何显示
	)
{
	WinExec("regedit win.reg", nCmdShow);//运行指定的程序
	SHChangeNotify(SHCNE_ASSOCCHANGED, SHCNF_IDLIST, NULL, NULL);//通知系统 应用程序已经完成某个事件。如果应用程序执行的动作会影响到外壳就应该使用这个函数。
}