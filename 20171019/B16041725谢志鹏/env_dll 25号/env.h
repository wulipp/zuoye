#include <windows.h>

#define VARIABLES_APPEND	1/*宏定义*/
#define VARIABLES_RESET		2
#define VARIABLES_NULL		0

DWORD WINAPI EnumEnvironmentVariables();/*DWORD 代表 unsigned long,定义一个DWORD型的函数*/
DWORD WINAPI ChangeEnviromentVariables(
									   LPSTR szName, /*szName是需要改变的环境*/
													 /*LPSTR ,指针，指向一个字串   */
									   LPSTR szNewValue,/*szNewValue新的变量值 */
									   DWORD dwFlag);/*附加、重置还是清零*/