// ConsoleApplication1.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"


/* ************************************
*《精通Windows API》
* 示例代码
* Mutex.c
* 7.2.2  演示使用Mutex同步线程
**************************************/

/* 头文件　*/
#include <windows.h>     //WINDEF.H 基本型态定义。 
                         //WINNT.H 支援Unicode的型态定义。
						 //WINBASE.H Kernel函式。
                         //WINUSER.H 使用者介面函式。
                         //WINGDI.H 图形装置介面函式。
#include <stdio.h>

/* 常量定义　*/
#define NUM_THREADS	4 

/* 全局变量　*/
DWORD dwCounter = 0;        //unsigned long DWORD
HANDLE hMutex;              // handle to hMutex  句柄

/* 函数声明　*/
void UseMutex(void);
DWORD WINAPI MutexThread(LPVOID lpParam);//返回值为DWORD型，调用方式为stdcall，LPVOID为空指针类型

/*************************************
* int main(void)
* 功能	演示
*
* 参数	未使用
**************************************/
int main()
{
	UseMutex();
}
/*************************************
* void UseMutex(void)
* 功能	演示 Mutex 的使用方法
*
* 参数	未使用
**************************************/
void UseMutex(void)
{
	INT i;
	HANDLE hThread;

#ifdef MUTEX
	// 创建 Mutex
	hMutex = CreateMutex(           //找出当前系统是否已经存在指定进程的实例。如果没有则创建一个互斥体。
		NULL,                       // 默认安全属性，安全控制，一般直接传入NULL
		FALSE,                      // 初始化为未被拥有，（确定互斥量的初始拥有者：如果传入TRUE表示互斥量对象内部会记录
	//创建它的线程的线程ID号并将递归计数设置为1，由于该线程ID非零，所以互斥量处于未触发状态，表示互斥量为创建线程拥有 
	//如果传入FALSE，那么互斥量对象内部的线程ID号将设置为NULL，递归计数设置为0，这意味互斥量不为任何线程占用，处于触发状态

		NULL);                      // 未命名 设置互斥量的名称，在多个进程中的线程就是通过名称来确保它们访问的是同一个互斥量
	if (hMutex == NULL)
	{
		printf("CreateMutex error: %d\n", GetLastError());
	}
#endif
	// 创建多个线程
	for (i = 0; i < NUM_THREADS; i++)
	{
		hThread = CreateThread(//返回一个在内核对象中分配的线程标识/句柄，可供管理
			NULL,//NULL为默认安全性，不可以被子线程继承，指向SECURITY_ATTRIBUTES型态的结构的指针
			0,//设置初始栈的大小，以字节为单位，如果为0，那么默认将使用与调用该函数的线程相同的栈空间大小
			MutexThread,//指向线程函数的指针
			NULL,//向线程函数传递的参数，是一个指向结构的指针，不需传递参数时，为NULL
			0, //线程标志，0表示创建后立即激活
			NULL);//保存新线程的id，函数成功，返回线程句柄；函数失败返回false。若不想返回线程ID,设置值为NULL
		if (hThread == NULL)
		{
			printf("CreateThread failed (%d)\n", GetLastError());
			return;
		}
	}
	Sleep(1000);
}

/*************************************
* DWORD WINAPI MutexThread(LPVOID lpParam)
* 功能	线程函数，读共享内存
*
* 参数	未使用
**************************************/
DWORD WINAPI MutexThread(LPVOID lpParam)
{

#ifdef MUTEX 
	DWORD dwWaitResult;
	dwWaitResult = WaitForSingleObject(
		hMutex,			// 句柄
		INFINITE);		// 无限等待

/*WaitForSingleObject
当指定的对象处于有信号状态或者等待时间结束的状态时，此函数返回。
DWORD WaitForSingleObject(
  HANDLE hHandle,
  DWORD dwMilliseconds
);
参数：
hHandle：指定对象或事件的句柄；
dwMilliseconds: 等待时间，以毫妙为单位，当超过等待时间时，此函数将返回。如果该参数设置为0，则该函数立即返回，如果设置为INFINITE，则该函数直到有信号才返回。
返回值：
如果此函数成功，该函数的返回之标识了引起该函数返回的事件。返回值如下：
  WAIT_ABANDONED（0x00000080L）
  指定的对象是一个互斥对象，该对象没有被拥有该对象的线程在线程结束前释放。互斥对象的所有权被同意授予调用该函数的线程。互斥对象被设置成为无信号状态。
  WAIT_OBJECT_0 （0x00000000L）
  指定的对象出有有信号状态。
  WAIT_TIMEOUT （0x00000102L）
  超过等待时间，指定的对象处于无信号状态
如果失败，返回 WAIT_FAILED；
备注：
此函数检查指定的对象或事件的状态，如果该对象处于无信号状态，则调用线程处于等待状态，此时该线程不消耗CPU时间，*/

	switch (dwWaitResult)
	{
	case WAIT_OBJECT_0:
#endif
		// 等待随机长的时间，各个线程等待的时间将不一致
		Sleep(rand() % 100);
		// 得到互斥对象后 访问共享数据
		printf("counter: %d\n", dwCounter);
		// 互斥对象保证同一时间只有一个线程在访问dwCounter
		dwCounter++;

#ifdef MUTEX
		// 释放 Mutex
		if (!ReleaseMutex(hMutex))
		{
			printf("Release Mutex error: %d\n", GetLastError());
		}
		break;
	default:
		printf("Wait error: %d\n", GetLastError());
		ExitThread(0);
	}
#endif
	return 1;
}


