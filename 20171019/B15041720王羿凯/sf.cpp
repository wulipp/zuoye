/* ************************************
*《精通Windows API》 
* 示例代码
* sf.c
* 12.1  Shell目录管理接口
**************************************/
/* 头文件　*/
#include <Windows.h>
#include <shlobj.h>
#include <stdio.h>

/* 函数申明　*/
DWORD ListFileInRecycleBin();//返回类型为32位无符号数
VOID GetSpecialFolder();//定义函数名为GetSpecialfolder,返回类型为void

/*************************************
* int main()
* 功能	调用相关函数
*
* 参数	未使用
**************************************/
int main()
{
	GetSpecialFolder();//调用GetSpecialFolder
	ListFileInRecycleBin();//调用ListFileInRecycleBin
}
/*************************************
* VOID GetSpecialFolder()
* 功能	获取并显示特殊目录
*
* 参数	未使用
**************************************/
VOID GetSpecialFolder()
{
	// 获取我的文档的路径
	CHAR szMyDocument[MAX_PATH];// My Document的路径
	// 使用SHGetSpecialFolderPath获取特殊目录路径
	SHGetSpecialFolderPath(NULL,szMyDocument,CSIDL_PERSONAL,FALSE);
	// 获取桌面的路径
	CHAR szDesktop[MAX_PATH];	//DeskTop的路径
	LPITEMIDLIST pidl = NULL;//定义类型为LPITEMIDLIST名为pidl，初始化为NULL
	LPMALLOC pMalloc = NULL;//定义LPMALLOC名为pMalloc，初始化为BULL
	// 分配
	SHGetMalloc(&pMalloc);//调用SHGetMalloc，返回一个指向shell的IMalloc接口的指针
	// 使用SHGetFolderLocation、SHGetPathFromIDList可以获取任意目录的路径
	SHGetFolderLocation(NULL, CSIDL_DESKTOPDIRECTORY, NULL, 0, &pidl);//获得特定的目录路径（父窗口保留，系统具体目录，访问令牌，保留，获得目录位置）
	SHGetPathFromIDList(pidl,szDesktop);//制定文档中的一个目录地点，接收文档系统的缓冲地址，从而把项目标识符列表转化为文档系统路径
	// 释放
	pMalloc->Free(pidl);//释放pidl
	pMalloc->Release();//释放内存空间
	// 显示结果
	printf("My Document:\t %s\n",szMyDocument);//打印My Document路劲
	printf("DeskTop:\t %s\n",szDesktop);//打印桌面路劲
}
/*************************************
* VOID GetSpecialFolder()
* 功能	遍历并显示回收站中的文件
*
* 参数	未使用
**************************************/
DWORD ListFileInRecycleBin()
{
	CHAR pszPath[MAX_PATH];		// 保存路径
	// IShellFolder接口
	IShellFolder *pisf = NULL;
	IShellFolder *pisfRecBin = NULL;
	// 获取“根”目录，桌面
	SHGetDesktopFolder(&pisfRecBin);
	
	IEnumIDList *peidl = NULL;	// 对象遍历接口
	LPITEMIDLIST pidlBin = NULL;
	LPITEMIDLIST idlCurrent = NULL;

	LPMALLOC pMalloc = NULL;
	// 分配
	SHGetMalloc(&pMalloc);//调用SHGetMalloc，返回一个指向shell的IMalloc接口的指针
	// 回收站位置
	SHGetFolderLocation(NULL, CSIDL_BITBUCKET, NULL, 0, &pidlBin);//获得特定的目录路径（父窗口保留，系统具体目录，访问令牌，保留，获得目录位置）
	// 绑定回收站对象
	pisfRecBin->BindToObject(pidlBin,NULL,IID_IShellFolder,(void **) &pisf);
	// 列举回收站中的对象,得到IEnumIDList接口，包括SHCONTF_FOLDERS、
	// SHCONTF_NONFOLDERS、SHCONTF_INCLUDEHIDDEN类型的对象
	pisf->EnumObjects(NULL,
		SHCONTF_FOLDERS | SHCONTF_NONFOLDERS |SHCONTF_INCLUDEHIDDEN,
		&peidl);

	STRRET strret;
	ULONG uFetched;

	HANDLE hOutPut = GetStdHandle(STD_OUTPUT_HANDLE);//它用于从一个特定的标准设备（标准输入、标准输出或标准错误）中取得一个句柄（用来标识不同设备的数值）
	printf("\nFiles In Recycle Bin:\n");

	while(1)
	{
		// 遍历IEnumIDList对象，idlCurrent为当前对象
		if(peidl->Next(1,&idlCurrent,&uFetched) == S_FALSE)
			break;
		// 获取回收站当前对象当前的路径，这里没有输出结果，读者可自行修改
		SHGetPathFromIDList(idlCurrent,  pszPath);
		// DisplayName，删除前的路径
		pisf->GetDisplayNameOf(idlCurrent,SHGDN_NORMAL,&strret);//获取全路径
		// 显示，printf可能会造成字符编码不正确。
		WriteConsoleW(hOutPut,L"\t",1,NULL,NULL);//从当前光标位置写入一个字符串到屏幕缓冲区
		WriteConsoleW(hOutPut,strret.pOleStr,lstrlenW(strret.pOleStr),NULL,NULL);//从当前光标位置写入一个字符串到屏幕缓冲区
		WriteConsoleW(hOutPut,L"\n",1,NULL,NULL);//从当前光标位置写入一个字符串到屏幕缓冲区
	}
	// 释放资源
	pMalloc->Free(pidlBin);//释放pidBin
	pMalloc->Free(strret.pOleStr);//释放srret.pOleStr
	pMalloc->Release();
	peidl->Release();
	pisf->Release();
	system("pause");//停顿
	return 0;	
}
