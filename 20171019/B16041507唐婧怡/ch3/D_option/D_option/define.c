/* ************************************
*《精通Windows API》 
* 示例代码
* define.c
* 3.1.6  示例：使用/D选项进行条件编译
**************************************/

/* 预处理　*/
/* 头文件　*/
#include <stdio.h>

/* 判断申明 */
#ifdef DEBUG_PRINT//条件编译命令，当标识符已经被定义过时，则对接下来程序段进行编译，否则编译else后程序段
#define MyPrint printf("hello\n")
#else
#define MyPrint
#endif

// main函数
int main()
{
	MyPrint;
	return 0;
}