#--- 用户自定义宏 ---
OUT_DIR = subdir
WINDIR = OK

all:showpath $(OUT_DIR)\showmacro.exe recurssion

#---- 演示引用环境变量 ----
showpath:
	echo	shwo environment variable macro//echo可以基于TCP协议，服务器就在TCP端口7检测有无消息，如果使用UDP协议，基本过程和TCP一样，检测的端口也是7。 是路由也是网络中最常用的数据包，可以通过发送echo包知道当前的连接节点有那些路径，并且通过往返时间能得出路径长度。 
	echo	WINDIR=$(WINDIR)
	echo	---

#--- 演示文件名宏 ---
$(OUT_DIR)\showmacro.exe:
	echo	show file name macro
	echo	$(@D)
	echo 	$(@F)
	
recurssion:
	echo ---
	$(MAKE) /N /$(MAKEFLAGS) /F s.mak showpath