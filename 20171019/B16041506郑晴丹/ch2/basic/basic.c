/* ************************************
*《精通Windows API》
* 示例代码
* basic.c
* 2.2  Windows API的功能分类
**************************************/

/* 头文件　*/
#include <windows.h>

/* ************************************
* 功能	获取系统目录信息，并存储到文件中
**************************************/
int main(int argc, TCHAR argv[])
{
	//文件句柄
	HANDLE hFile;
	/* HANDLE 类型定义 void空指针类型，指向任何对象的指针都可以转换为void*类型指针 */

	DWORD dwWritten;
	/* DWORD 类型定义 unsigned long类型 */
	//字符数组，用于存储系统目录
	TCHAR szSystemDir[MAX_PATH];
	/* TCHAR 类型定义 char类型
	   MAX_PATH 宏定义 260 */
	//获取系统目录
	GetSystemDirectory(szSystemDir, MAX_PATH);
	/* GetSystemDirectory(用来接收路径的指向缓冲区的指针,缓冲区的最大大小) 宏定义 GetSystemDirectoryA()/GetSystemDirectoryW() Windows函数 检索系统目录的路径 */

	//创建文件systemroot.txt
	hFile = CreateFile("systemroot.txt",
		GENERIC_WRITE,
		0, NULL, CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL,
		NULL);
	/* GENERIC_WRITE 宏定义 (0x40000000L)
	   CREATE_ALWAYS 宏定义 2
	   FILE_ATTRIBUTE_NORMAL 宏定义 0x00000080
	   CreateFile(lpFileName 文件名,dwDesiredAccess 访问模式,dwShareMode 共享模式,lpSecurityAttributes ,dwCreationDisposition 如何创建,dwFlagsAndAttributes 文件属性,hTemplateFile 用于复制文件句柄) 宏定义 CreateFileA()/CreateFileW() Windows函数 创建或打开文件或I/O设备 */
	//判断文件是否创建成功
	if (hFile != INVALID_HANDLE_VALUE)
	/* INVALID_HANDLE_VALUE 宏定义 ((HANDLE)(LONG_PTR)-1) */
	{
		//将系统目录系统信息写入文件
		if (!WriteFile(hFile, szSystemDir, lstrlen(szSystemDir), &dwWritten, NULL))
			/* WriteFile(hFile 文件或I/O设备的句柄,lpBuffer 指向缓冲区的指针,lpNumberOfBytesWritten 要写入文件或设备的字节数,lpOverlapped 对于overlapped结构的不同处理) Windows函数 将数据写入指定的文件
			   lstrlen(要检查的以null结尾的字符串) 宏定义 lstrlenA()/lstrlenW() Windows函数 确定指定字符串的长度（不包括终止空字符）*/
		{
			return GetLastError();
			/* GetLastError() Windows函数 检索调用线程的最后一个错误代码值 */
		}
	}
	//关闭文件，返回。
	CloseHandle(hFile);
	/* CloseHandle(打开的对象句柄) Windows函数 关闭一个打开的对象句柄 */
	return 0;
}
